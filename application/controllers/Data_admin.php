<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_admin extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->securityForAdmin();
    }

    protected function index($id_semester=false)
    {
        $transfer['list'] = $this->dbsystem->getData('*','el_user',array('akses' => 'admin'))->result();
        $this->myload->display('page/admin/list',$transfer);
    }

    protected function hapus_admin($id_semester=false,$id_user=false)
    {
        $this->db->delete('el_user',array('id_user' => $id_user));
        $this->session->set_flashdata('berhasil', 'Data Sudah dihapus !');
        redirect(base_url().'index.php/'.$this->mycrypt->enkripsi('data_admin','index',array('id_semester' => $id_semester)));
    }

    protected function tambah_admin($id_semester=false)
    {
        $data = $_POST;
        $data['akses'] = "admin";
        $this->db->insert('el_user',$data);
        $this->session->set_flashdata('berhasil', 'Data Sudah berhasil ditambahkan !');
        redirect(base_url().'index.php/'.$this->mycrypt->enkripsi('data_admin','index',array('id_semester' => $id_semester)));
    }

    protected function edit_admin($id_semester=false,$id_user=false)
    {
        $data = $_POST;
        $where = array('id_user' => $id_user);
        $this->db->update('el_user',$data,$where);
        $this->session->set_flashdata('berhasil', 'Data Sudah berhasil diperbarui !');
        redirect(base_url().'index.php/'.$this->mycrypt->enkripsi('data_admin','index',array('id_semester' => $id_semester)));
    }

}

/* End of file Data_admin.php */
/* Location: ./application/controllers/Data_admin.php */