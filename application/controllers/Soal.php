<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal extends MY_Controller {
	function __construct(){
    	parent::__construct();
    	$this->securityForAdminGuru();

    	$this->load->model('dbsoal','dbmine');
    	$this->load->model('mylibrary/myupload');
  	}

	protected function index($id_semester=false,$kategori=false)
	{

		if($_SESSION['akses']=="admin")
		{
				$list = $this->dbmine->dataSoal(false,array('el_ujian.id_semester' => $id_semester, 'kategori_ujian' => $kategori),'el_mapel.nama_mapel asc')->result_array();
				for($x=0;$x<count($list);$x++)
				{
					if($list[$x]['tipe_ujian'] == "pilihan")
					{
						$list[$x]['soal'] = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $list[$x]['id_ujian']))->num_rows();
					}
				}

			$transfer['list'] = $list;
			$transfer['kategori'] = $kategori;

			$this->myload->display('page/soal/list',$transfer);
		}
		else if($_SESSION['akses'] == "guru")
		{
				$list = $this->dbmine->dataSoal(false,array('el_ujian.id_semester' => $id_semester, 'kategori_ujian' => $kategori,'id_guru' => $_SESSION['id_guru']),'el_mapel.nama_mapel asc')->result_array();
				for($x=0;$x<count($list);$x++)
				{
					if($list[$x]['tipe_ujian'] == "pilihan")
					{
						$list[$x]['soal'] = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $list[$x]['id_ujian']))->num_rows();
					}
				}

			$transfer['list'] = $list;
			$transfer['kategori'] = $kategori;
			$transfer['mapel'] = $this->dbmine->MapelGuru(false,array('id_guru' => $_SESSION['id_guru']))->result();
 			$this->myload->display('page/soal/list',$transfer,"guru");
		}

	}

	protected function tambahAksesGuru($id_semester=false,$kategori=false)
	{
		$data = $_POST;
		$data['id_guru'] = $_SESSION['id_guru'];
		$data['id_semester'] = $id_semester;
		$data['kategori_ujian'] = $kategori;
		$this->db->insert('el_ujian',$data);

			$this->session->set_flashdata('berhasil', 'Data Soal Berhasil ditambahkan');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $id_semester,'id2' => $kategori));
			redirect($redirek);
	}

	protected function editAksesGuru($id_semester=false,$kategori=false,$id_ujian=false)
	{
		$cek = $this->dbsystem->getData('*','el_ujian_launch',array('id_ujian' => $id_ujian))->num_rows();
		if($cek == 0)
		{
			$this->db->update('el_ujian',$_POST,array('id_ujian' => $id_ujian));
			$this->session->set_flashdata('berhasil', 'Data Soal berhasil di perbarui.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $id_semester,'id2' => $kategori));
			redirect($redirek);
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Maaf ujian ini sudah di berikan kepada siswa sehingga tidak bisa di edit');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $id_semester,'id2' => $kategori));
			redirect($redirek);
		}
	}

	protected function hapus($id_semester=false,$kategori=false,$id_ujian=false)
	{
		$cek = $this->dbsystem->getData('*','el_ujian_launch',array('id_ujian' => $id_ujian))->num_rows();
		if($cek == 0)
		{
			$this->db->delete('el_ujian',array('id_ujian' => $id_ujian));
			$this->session->set_flashdata('berhasil', 'Data Soal berhasil di hapus.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $id_semester,'id2' => $kategori));
			redirect($redirek);
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Maaf ujian ini sudah di berikan kepada siswa sehingga tidak bisa di hapus');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $id_semester,'id2' => $kategori));
			redirect($redirek);
		}
	}


	protected function pertanyaan($id_semester=false,$kategori=false,$id_ujian=false)
	{
		// echo $id_ujian;exit;
		$where =array('id_ujian' => $id_ujian);
		$transfer['kategori'] = $kategori;
		$transfer['id_ujian'] = $id_ujian;
		$transfer['ujian'] = $this->dbmine->dataSoal(false,$where)->row();

		if($transfer['ujian']->tipe_ujian == "pilihan")
		{
			$transfer['soal'] = $this->dbsystem->getData('*','el_pilihan_soal',$where)->result();
		}


			if($_SESSION['akses'] == "guru")
			{
				$this->myload->display('page/soal/pertanyaan',$transfer,'guru');
			}
			else if($_SESSION['akses'] == "admin")
			{
				$this->myload->display('page/soal/pertanyaan',$transfer);
			}
	}

	protected function pilihan_pertanyaan_tambah($id_semester=false,$kategori=false,$id_ujian=false)
	{
		$cek = $this->dbsystem->getData('*','el_ujian_launch',array('id_ujian' => $id_ujian))->num_rows();
		if($cek == 0)
		{
		//atas cek

		$file = $_FILES;
		$file_0 = "";
		$file_1 = "";
		$file_2 = "";
		$file_3 = "";
		$file_4 = "";
		$file_5 = "";
		
		
		if($file['pertanyaan_gambar']['name'] != "")
		{
			$pertanyaan_gambar = $this->myupload->gambar_ResizeW('pertanyaan_gambar','./file/soal/',"800");
			if($pertanyaan_gambar['status'] == "berhasil")
			{
				$file_0 = $pertanyaan_gambar['file_name'];
			}
		}

		if($file['pg1_gambar']['name'] != "")
		{
			$pg1_gambar = $this->myupload->gambar_ResizeW('pg1_gambar','./file/soal/',"800");
			if($pg1_gambar['status'] == "berhasil")
			{
				$file_1 = $pg1_gambar['file_name'];
			}
		}

		if($file['pg2_gambar']['name'] != "")
		{
			$pg2_gambar = $this->myupload->gambar_ResizeW('pg2_gambar','./file/soal/',"800");
			if($pg2_gambar['status'] == "berhasil")
			{
				$file_2 = $pg2_gambar['file_name'];
			}
		}

		if($file['pg3_gambar']['name'] != "")
		{
			$pg3_gambar = $this->myupload->gambar_ResizeW('pg3_gambar','./file/soal/',"800");
			if($pg3_gambar['status'] == "berhasil")
			{
				$file_3 = $pg3_gambar['file_name'];
			}
		}

		if($file['pg4_gambar']['name'] != "")
		{
			$pg4_gambar = $this->myupload->gambar_ResizeW('pg4_gambar','./file/soal/',"800");
			if($pg4_gambar['status'] == "berhasil")
			{
				$file_4 = $pg4_gambar['file_name'];
			}
		}

		if($file['pg5_gambar']['name'] != "")
		{
			$pg5_gambar = $this->myupload->gambar_ResizeW('pg5_gambar','./file/soal/',"800");
			if($pg5_gambar['status'] == "berhasil")
			{
				$file_5 = $pg5_gambar['file_name'];
			}
		}

		$insert = $_POST;
		$insert['pertanyaan_gambar'] = $file_0;
		$insert['pg1_gambar'] = $file_1;
		$insert['pg2_gambar'] = $file_2;
		$insert['pg3_gambar'] = $file_3;
		$insert['pg4_gambar'] = $file_4;
		$insert['pg5_gambar'] = $file_5;
		$insert['id_ujian'] = $id_ujian;
		
		$tambah=$this->db->insert('el_pilihan_soal',$insert);
		if($tambah)
		{
			$this->session->set_flashdata('berhasil', 'Pertanyaan berhasil ditambahkan.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Penambahan Pertanyaan Gagal.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}

		//ceknya
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Penambahan gagal karena soal sudah dipublish.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}

	}

	protected function pilihan_pertanyaan_edit($id_semester=false,$kategori=false,$id_ujian=false,$id_pertanyaan=false)
	{
		$cek = $this->dbsystem->getData('*','el_ujian_launch',array('id_ujian' => $id_ujian))->num_rows();
		if($cek == 0)
		{
			$transfer['pertanyaan'] = $this->dbsystem->getData('*','el_pilihan_soal',array('id_pilihan_soal' => $id_pertanyaan))->row();
			$transfer['id_ujian'] = $id_ujian;
			$transfer['kategori'] = $kategori;
			$this->myload->display('page/soal/pertanyaan_pilihan_edit',$transfer,'guru');
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Maaf ujian ini sudah di berikan kepada siswa sehingga tidak bisa di edit');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);	
		}
	}

	protected function pilihan_pertanyaan_edit_action($id_semester=false,$kategori=false,$id_ujian=false,$id_pertanyaan=false)
	{
		$pertanyaan = $this->dbsystem->getData('*','el_pilihan_soal',array('id_pilihan_soal' => $id_pertanyaan))->row();

		$data = $_POST;
		$file = $_FILES;
		if($file['pertanyaan_gambar']['name'] != "")
		{
			$pertanyaan_gambar = $this->myupload->gambar_ResizeW('pertanyaan_gambar','./file/soal/',"800");
			if($pertanyaan_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pertanyaan_gambar)){
					unlink('./file/soal/'.$pertanyaan->pertanyaan_gambar);
				}
				$data['pertanyaan_gambar'] = $pertanyaan_gambar['file_name'];
			}
		}

		if($file['pg1_gambar']['name'] != "")
		{
			$pg1_gambar = $this->myupload->gambar_ResizeW('pg1_gambar','./file/soal/',"800");
			if($pg1_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pg1_gambar)){
					unlink('./file/soal/'.$pertanyaan->pg1_gambar);
				}
				$data['pg1_gambar'] = $pg1_gambar['file_name'];
			}
		}

		if($file['pg2_gambar']['name'] != "")
		{
			$pg2_gambar = $this->myupload->gambar_ResizeW('pg2_gambar','./file/soal/',"800");
			if($pg2_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pg2_gambar)){
					unlink('./file/soal/'.$pertanyaan->pg2_gambar);
				}
				$data['pg2_gambar'] = $pg2_gambar['file_name'];
			}
		}

		if($file['pg3_gambar']['name'] != "")
		{
			$pg3_gambar = $this->myupload->gambar_ResizeW('pg3_gambar','./file/soal/',"800");
			if($pg3_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pg3_gambar)){
					unlink('./file/soal/'.$pertanyaan->pg3_gambar);
				}
				$data['pg3_gambar'] = $pg3_gambar['file_name'];
			}
		}

		if($file['pg4_gambar']['name'] != "")
		{
			$pg4_gambar = $this->myupload->gambar_ResizeW('pg4_gambar','./file/soal/',"800");
			if($pg4_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pg4_gambar)){
					unlink('./file/soal/'.$pertanyaan->pg4_gambar);
				}
				$data['pg4_gambar'] = $pg4_gambar['file_name'];
			}
		}

		if($file['pg5_gambar']['name'] != "")
		{
			$pg5_gambar = $this->myupload->gambar_ResizeW('pg5_gambar','./file/soal/',"800");
			if($pg5_gambar['status'] == "berhasil")
			{
				if(is_file('./file/soal/'.$pertanyaan->pg5_gambar)){
					unlink('./file/soal/'.$pertanyaan->pg5_gambar);
				}
				$data['pg5_gambar'] = $pg5_gambar['file_name'];
			}
		}

		$update = $this->db->update('el_pilihan_soal',$data,array('id_pilihan_soal' => $id_pertanyaan));
		
		if($update)
		{
			$this->session->set_flashdata('berhasil', 'Pertanyaan berhasil di perbarui.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}
		

	}

	protected function pilihan_pertanyaan_hapus($id_semester=false,$kategori=false,$id_ujian=false,$id_pertanyaan=false)
	{
		$cek = $this->dbsystem->getData('*','el_ujian_launch',array('id_ujian' => $id_ujian))->num_rows();
		if($cek == 0)
		{
			$pertanyaan = $this->dbsystem->getData('*','el_pilihan_soal',array('id_pilihan_soal' => $id_pertanyaan))->row();
			if(is_file('./file/soal/'.$pertanyaan->pertanyaan_gambar)){
				unlink('./file/soal/'.$pertanyaan->pertanyaan_gambar);
			}
			if(is_file('./file/soal/'.$pertanyaan->pg1_gambar)){
				unlink('./file/soal/'.$pertanyaan->pg1_gambar);
			}
			if(is_file('./file/soal/'.$pertanyaan->pg2_gambar)){
				unlink('./file/soal/'.$pertanyaan->pg2_gambar);
			}
			if(is_file('./file/soal/'.$pertanyaan->pg3_gambar)){
				unlink('./file/soal/'.$pertanyaan->pg3_gambar);
			}
			if(is_file('./file/soal/'.$pertanyaan->pg4_gambar)){
				unlink('./file/soal/'.$pertanyaan->pg4_gambar);
			}
			if(is_file('./file/soal/'.$pertanyaan->pg5_gambar)){
				unlink('./file/soal/'.$pertanyaan->pg5_gambar);
			}

			$this->db->delete('el_pilihan_soal',array('id_pilihan_soal' => $id_pertanyaan));

			$this->session->set_flashdata('berhasil', 'Pertanyaan berhasil di hapus.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Tidak bisa dihapus karena soal sudah dipublish.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian));
			redirect($redirek);
		}
	}
}

/* End of file Soal.php */
/* Location: ./application/controllers/Soal.php */