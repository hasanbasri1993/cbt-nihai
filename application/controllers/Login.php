<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->securityForAdminGuru();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dbujian', 'dbmine');
    }


    public function index($id=false)
    {
        if (isset($_SESSION['username']) or isset($_SESSION['akses']) or isset($_SESSION['grand'])) {
            if ($_SESSION['grand'] == "qq_elearning") {
                if ($_SESSION['akses'] == "admin") {
                    $id_semester = $this->dbsystem->getData('*', 'master_ajaran_semester', array('status' => 'Y'))->row()->id;
                    $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => 'nihai'));
                    redirect($redirek);
                }
                if ($_SESSION['akses'] == "siswa") {
                    $id_semester = $this->dbsystem->getData('*', 'master_ajaran_semester', array('status' => 'Y'))->row()->id;
                    
                    $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('dashboard', 'siswa', array('id' => $id_semester));
                    redirect($redirek);
                }
            }
        }
        $this->myload->view('page/login1');
    }

    public function cekruang()
    {
        if(isset($_POST['ruang'])){
            $id_semester = $this->dbsystem->getData('*', 'master_ajaran_semester', array('status' => 'Y'))->row()->id;
            $this->db->where('id', '1');
            $this->db->update('el_ruang', array('ruang' => $_POST['ruang']));
            echo "sukses";
        }
     }
    

    protected function action()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        $cek = explode("_", $username);
        if ($cek[0] == "siswa") {
            $user = $this->dbsystem->getData('*', 'master_santri', array('induk' => $cek[1],'password' => $password))->row();
            if (count($user) == 1) {
                $session = array(
                    'id_siswa' => $user->id,
                    'nis' => $user->induk,
                    'username' => $user->induk,
                    'grand' => "qq_elearning",
                    'akses' => "siswa"
                    );
                $this->session->set_userdata($session);
               
                
                $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('dashboard', 'siswa', array('id' => $id_semester));
                redirect($redirek);
            //add else reidirect login klo siswa salah
            //09102018 11:07
            } else {
                $this->session->set_flashdata('gagal', 'Username atau Password anda salah !');
                redirect('login');
            }
        } else {
            $where = array(
                'akses' => "admin",
                'username' => $username,
                'password' => $password
                );
            $user = $this->dbsystem->getData('*', 'el_user', $where)->row();

            if (count($user) == "1") {
                $session = array(
                    'username' => $user->username,
                    'akses' => $user->akses,
                    'grand' => 'qq_elearning'
                    );
                $this->session->set_userdata($session);
                $id_semester 	= $this->dbsystem->getData('*', 'master_ajaran_semester', array('status' => 'Y'))->row()->id;
                $ruang 			= $this->dbsystem->getData('*', 'el_ruang', array('id'=>'1'))->row()->ruang;
               // print $ruang;
                if ($ruang == 0) {
                    $this->session->set_flashdata('ruang_kosong', '1');
                    $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => 'nihai'));
                    redirect($redirek);
                    $this->session->set_flashdata('ruang_kosong', '1');
                } else {
                    $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => 'nihai'));
                    redirect($redirek);
                    print "FALSE";
                }
            } else {
                $this->session->set_flashdata('gagal', 'Username atau Password anda salah !');
                redirect('login');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
