<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_mapel extends MY_Controller {

	function __construct(){
    	parent::__construct();
    	$this->securityForAdmin();
  	}

	protected function index($id_semester=false)
	{
		$transfer['list'] = $this->dbsystem->getData('*','el_mapel',false,"tulisan asc")->result();
		$this->myload->display('page/mapel/list',$transfer);
	}

	protected function tambah($id_semester=false)
	{
		if(isset($_POST['menulis']) or isset($_POST['membaca']) or isset($_POST['praktek']) )
		{
			$data = array(
				'nama_mapel' => $_POST['nama_mapel'],
				'bobot' => $_POST['bobot'],
				'tulisan' => $_POST['tulisan'],
				'paragraph' => $_POST['paragraph'],
				'rapot' => $_POST['rapot']
				);
			$mapel = $this->dbsystem->insertData('el_mapel',$data);
			if($mapel)
			{
				if(isset($_POST['menulis']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $mapel, 'tipe' => 'menulis'));
				if(isset($_POST['membaca']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $mapel, 'tipe' => 'membaca'));
				if(isset($_POST['praktek']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $mapel, 'tipe' => 'praktek'));

				$this->session->set_flashdata('berhasil', 'Data Mata Pelajaran berhasil ditambahkan.');
				$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
				redirect($redirek);
			}
			else
			{
				$this->session->set_flashdata('gagal', 'Penambahan Gagal.');
				$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
				redirect($redirek);
			}
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Tipe Penilaian harus dipilih salah satu.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
			redirect($redirek);
		}


	}

	protected function edit($id_semester=false,$id_mapel=false)
	{
		$transfer['list'] = $this->dbsystem->getData('*','el_mapel',array('id_mapel' => $id_mapel))->row();
		$transfer['tipe'] = $this->dbsystem->getData('*','el_mapel_tipe',array('id_mapel' => $id_mapel))->result();
		$this->myload->display('page/mapel/edit',$transfer);
	}


	protected function edit_action($id_semester=false,$id_mapel=false)
	{
		if(isset($_POST['menulis']) or isset($_POST['membaca']) or isset($_POST['praktek']) )
		{
			$data = array(
				'nama_mapel' => $_POST['nama_mapel'],
				'bobot' => $_POST['bobot'],
				'tulisan' => $_POST['tulisan'],
				'paragraph' => $_POST['paragraph'],
				'rapot' => $_POST['rapot']
				);
			$mapel = $this->dbsystem->updateData('el_mapel',$data,array('id_mapel' => $id_mapel));
			if($mapel)
			{
				$this->db->delete('el_mapel_tipe',array('id_mapel' => $id_mapel));

				if(isset($_POST['menulis']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $id_mapel, 'tipe' => 'menulis'));
				if(isset($_POST['membaca']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $id_mapel, 'tipe' => 'membaca'));
				if(isset($_POST['praktek']))$this->db->insert('el_mapel_tipe',array('id_mapel' => $id_mapel, 'tipe' => 'praktek'));

				$this->session->set_flashdata('berhasil', 'Data Mata Pelajaran telah diperbarui.');
				$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
				redirect($redirek);
			}
			else
			{
				$this->session->set_flashdata('gagal', 'Edit Gagal.');
				$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','edit',array('id' => $id_semester,'id2' => $id_mapel));
				redirect($redirek);
			}
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Tipe Penilaian harus dipilih salah satu.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','edit',array('id' => $id_semester,'id2' => $id_mapel));
			redirect($redirek);
		}
	}

	protected function hapus($id_semester=false,$id_mapel=false)
	{
		$cek1 = $this->dbsystem->getData('*','el_mapel_guru',array('id_mapel' => $id_mapel))->num_rows();
		$cek2 = $this->dbsystem->getData('*','el_ujian',array('id_mapel' => $id_mapel))->num_rows();
		if($cek1 == 0 and $cek2 == 0)
		{
			$this->db->delete('el_mapel_tipe',array('id_mapel' => $id_mapel));
			$this->db->delete('el_mapel',array('id_mapel' => $id_mapel));
					
			$this->session->set_flashdata('berhasil', 'Data Mata Pelajaran telah di hapus.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
			redirect($redirek);
		}
		else
		{
			$this->session->set_flashdata('gagal', 'Mata Pelajaran tidak bisa dihapus karena sudah terkait dengan guru atau ujian.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_mapel','index',array('id' => $id_semester));
			redirect($redirek);	
		}
	}
}

/* End of file Data_mapel.php */
/* Location: ./application/controllers/Data_mapel.php */
