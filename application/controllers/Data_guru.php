<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_guru extends MY_Controller {

	function __construct(){
    	parent::__construct();
    	$this->load->model('dbguru','dbmine');
    	$this->securityForAdmin();
  	}
  	
	function index($id_semester=false)
	{
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
		$transfer['list'] = $this->dbmine->dataGuru("count(master_pegawai.id) as jumlah",array('el_mapel_guru.id_ajaran' => $tahun->id),false,'master_pegawai.id')->result();
		
		$this->myload->display('page/guru/list',$transfer);
	}

	function tambah($id_semester=false)
	{
		$transfer['guru'] = $this->dbsystem->getData('*','master_pegawai','status = "aktif"',"nama asc")->result();
		$transfer['mapel'] = $this->dbsystem->getData('*','el_mapel')->result();
			
			$master_ajaran = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
			$sekolah = $this->dbsystem->getData('*','master_sekolah')->result_array();
			for($x=0;$x<count($sekolah);$x++) {
				$sekolah[$x]['kelasnya'] = $this->dbsystem->getData('*','master_kelas',array('id_sekolah' => $sekolah[$x]['id']))->result_array();
				for($i=0;$i<count($sekolah[$x]['kelasnya']);$i++) {
					$sekolah[$x]['kelasnya'][$i]['rombelnya'] = $this->dbsystem->getData('*','master_rombel',array('id_kelas' => $sekolah[$x]['kelasnya'][$i]['id'], 'tahun_ajaran' => $master_ajaran->id))->result_array();
				}
			}

		$transfer['sekolah'] = $sekolah;
		$this->myload->display('page/guru/tambah',$transfer); 
	}

	function tambah_action($id_semester=false)
	{
		//Proses Checking
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();

		$cek1 = $this->dbsystem->getData('*','el_mapel_guru',array('id_guru' => $_POST['id_guru'],'id_ajaran' => $tahun->id))->num_rows();
		$cek2 = $this->dbsystem->getData('*','el_rombel_guru',array('id_guru' => $_POST['id_guru'],'id_ajaran' => $tahun->id))->num_rows();

		if($cek1 > 0 or $cek2 > 0)
		{
			$this->session->set_flashdata('gagal', 'Maaf pegawai yang anda pilih sudah terdaftar.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','tambah',array('id' => $id_semester));
			redirect($redirek);
		}
		
			$mapel = $this->dbsystem->getData('*','el_mapel')->result();
			
			$rom_xs = $this->dbsystem->getData('*','master_rombel',array('tahun_ajaran' => $tahun->id),"id asc")->row_array()['id'];
			$rom_lg = $this->dbsystem->getData('*','master_rombel',array('tahun_ajaran' => $tahun->id),"id desc")->row_array()['id'];

				$kelas = 0;
				for ($x=$rom_xs; $x<=$rom_lg; $x++) { 
					if(isset($_POST[$x])){
						$kelas++;
					}
				}

				$j_mapel = 0;
				foreach ($mapel as $m) {
					$var = "mapel".$m->id_mapel;
					if(isset($_POST[$var]))
					{
						$j_mapel++;
					}
				}


		if($j_mapel == 0 or $kelas == 0)
		{
			$this->session->set_flashdata('gagal', 'Pemilihan Kelas dan Mata Pelajaran tidak boleh kosong.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','tambah',array('id' => $id_semester));
			redirect($redirek);
		}
		else
		{
			// echo "berhasil";exit;
			//Proses Insert Guru
			for ($x=$rom_xs; $x<=$rom_lg; $x++) { 
				if(isset($_POST[$x])){
					$this->db->insert('el_rombel_guru',array('id_guru' => $_POST['id_guru'], 'id_rombel' => $_POST[$x], 'id_ajaran' => $tahun->id));
				}
			}

			foreach ($mapel as $m) 
			{
				$var = "mapel".$m->id_mapel;
				if(isset($_POST[$var]))
				{
					$this->db->insert('el_mapel_guru',array('id_guru' => $_POST['id_guru'], 'id_mapel' => $_POST[$var], 'id_ajaran' => $tahun->id));
				}
			}

			$this->session->set_flashdata('berhasil', 'Data Guru telah ditambahkan.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','index',array('id' => $id_semester));
			redirect($redirek);
		}


	}



	function edit($id_semester=false,$id_guru=false)
	{
		$transfer['guru'] = $this->dbsystem->getData('*','master_pegawai',"id = $id_guru","nama asc")->row();
		$transfer['mapel'] = $this->dbsystem->getData('*','el_mapel')->result();
			
			$master_ajaran = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
			$sekolah = $this->dbsystem->getData('*','master_sekolah')->result_array();
			for($x=0;$x<count($sekolah);$x++) {
				$sekolah[$x]['kelasnya'] = $this->dbsystem->getData('*','master_kelas',array('id_sekolah' => $sekolah[$x]['id']))->result_array();
				for($i=0;$i<count($sekolah[$x]['kelasnya']);$i++) {
					$sekolah[$x]['kelasnya'][$i]['rombelnya'] = $this->dbsystem->getData('*','master_rombel',array('id_kelas' => $sekolah[$x]['kelasnya'][$i]['id'], 'tahun_ajaran' => $master_ajaran->id))->result_array();
				}
			}

		$transfer['sekolah'] = $sekolah;


		$transfer['el_mapel'] = $this->dbsystem->getData('*','el_mapel_guru',"id_guru = $id_guru")->result();
		$transfer['el_rombel'] = $this->dbsystem->getData('*','el_rombel_guru',"id_guru = $id_guru")->result();
		$this->myload->display('page/guru/edit',$transfer); 
	}

	function edit_action($id_semester=false,$id_guru=false)
	{
			$mapel = $this->dbsystem->getData('*','el_mapel')->result();
			$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
			$rom_xs = $this->dbsystem->getData('*','master_rombel',array('tahun_ajaran' => $tahun->id),"id asc")->row_array()['id'];
			$rom_lg = $this->dbsystem->getData('*','master_rombel',array('tahun_ajaran' => $tahun->id),"id desc")->row_array()['id'];

				$kelas = 0;
				for ($x=$rom_xs; $x<=$rom_lg; $x++) { 
					if(isset($_POST[$x])){
						$kelas++;
					}
				}

				$j_mapel = 0;
				foreach ($mapel as $m) {
					$var = "mapel".$m->id_mapel;
					if(isset($_POST[$var]))
					{
						$j_mapel++;
					}
				}

		if($j_mapel == 0 or $kelas == 0)
		{
			$this->session->set_flashdata('gagal', 'Pemilihan Kelas dan Mata Pelajaran tidak boleh kosong.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','edit',array('id' => $id_semester,'id2' => $id_guru));
			redirect($redirek);
		}
		else
		{
			//Proses Insert Guru
			// echo "masuk";exit;
			$this->db->delete('el_mapel_guru',array('id_guru' => $id_guru, 'id_ajaran' => $tahun->id));
			$this->db->delete('el_rombel_guru',array('id_guru' => $id_guru, 'id_ajaran' => $tahun->id));
			
			for ($x=$rom_xs; $x<=$rom_lg; $x++) { 
				if(isset($_POST[$x])){
					$this->db->insert('el_rombel_guru',array('id_guru' => $id_guru, 'id_rombel' => $_POST[$x], 'id_ajaran' => $tahun->id));
				}
			}

			foreach ($mapel as $m) 
			{
				$var = "mapel".$m->id_mapel;
				if(isset($_POST[$var]))
				{
					$this->db->insert('el_mapel_guru',array('id_guru' => $id_guru, 'id_mapel' => $_POST[$var], 'id_ajaran' => $tahun->id));
				}
			}

			$this->session->set_flashdata('berhasil', 'Data Guru telah diperbarui.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','index',array('id' => $id_semester));
			redirect($redirek);
			// echo "berhasil";
		}

	}


	function hapus($id_semester=false,$id_guru=false)
	{
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
		$this->db->delete('el_mapel_guru',array('id_guru' => $id_guru, 'id_ajaran' => $tahun->id));
		$this->db->delete('el_rombel_guru',array('id_guru' => $id_guru, 'id_ajaran' => $tahun->id));

		$this->session->set_flashdata('berhasil', 'Data Guru telah dihapus.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','index',array('id' => $id_semester));
			redirect($redirek);
	}
}

/* End of file data_guru.php */
/* Location: ./application/controllers/data_guru.php */