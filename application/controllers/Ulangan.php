<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ulangan extends MY_Controller {
	function __construct()
	{
    	parent::__construct();
    	date_default_timezone_set('Asia/Jakarta');
	    	$this->securityForAll();
	    	$this->load->model('dbsantri','dbmine');
  	}

	protected function index($id_semester=false,$kategori=false)
	{
		$where = array(
			'el_ujian_siswa.id_siswa' => $_SESSION['id_siswa'],
			'el_ujian_siswa.status_ujian !=' => "sudah",
			'el_ujian_launch.id_semester' => $id_semester,
			'el_ujian_launch.waktu_dari <=' => date('Y-m-d H:i:s'),
			'el_ujian_launch.waktu_sampai >=' => date('Y-m-d H:i:s')
			);
			//print_r($where);
		$transfer['list'] = $this->dbmine->listUjian($where)->result();
		$transfer['kategori'] = $kategori;
		$this->myload->display("page/ulangan/list",$transfer,"siswa");
	}


	protected function mulai($id_semester=false,$kategori=false,$id_ujian_siswa=false)
	{
		$data = $this->dbmine->listUjian(array('el_ujian_siswa.id_ujian_siswa' => $id_ujian_siswa))->row();

		//checking
		// if($data->pengulangan == "7"){
		// 	$this->db->update('el_ujian_siswa',array("status_ujian" => "sudah"),array('id_ujian_siswa' => $id_ujian_siswa));

		// 	$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','curang',array('id' => $id_semester,'id2' => $kategori,'id3' => $id_ujian_siswa));
		// 	redirect($redirek);
		// }

		//proses
		if($data->tipe_ujian == "pilihan")
		{
			//pengambilan soal
			if($data->status_random == "Y"){
				$d_random = array(
						"id_pilihan_soal asc",
						"id_pilihan_soal desc",
						"pertanyaan asc",
						"pertanyaan desc",
						"pertanyaan_gambar asc",
						"pertanyaan_gambar desc",
						"pg1 asc",
						"pg2 desc",
						"pg3 desc",
						"pg4 asc",
						"pg5 desc",
						"jawaban desc",
						"jawaban asc"
						);
				$random = $this->CodeRandom($d_random,1);
				$soal = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $data->id_ujian),$random)->result_array();

				$jawaban = $this->dbsystem->getData('*','el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'id_siswa' => $_SESSION['id_siswa']))->result_array();
				for($x=0;$x<count($soal);$x++) {
					$soal[$x]['jawaban'] = "";
					foreach ($jawaban as $j) {
						if($j['id_pilihan_soal'] == $soal[$x]['id_pilihan_soal'])
						{
							$soal[$x]['jawaban'] = $j['pilihan'];
						}

					}
				}
			}else{
				$random = "id_pilihan_soal asc";

				$soal = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $data->id_ujian),$random)->result_array();

				$jawaban = $this->dbsystem->getData('*','el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'id_siswa' => $_SESSION['id_siswa']))->result_array();
				for($x=0;$x<count($soal);$x++) {
					$soal[$x]['jawaban'] = "";
					foreach ($jawaban as $j) {
						if($j['id_pilihan_soal'] == $soal[$x]['id_pilihan_soal'])
						{
							$soal[$x]['jawaban'] = $j['pilihan'];
						}
						
					}
				}
			}

			//abjad abcde
			if($data->tulisan == "arab"){
				$transfer['pilihan'] = array(
					'pg1' => "ا",
					'pg2' => "ب",
					'pg3' => "ت",
					'pg4' => "ث",
					'pg5' => "ج"
					);
			}
			else if($data->tulisan == "latin")
			{
				$transfer['pilihan'] = array(
					'pg1' => "A",
					'pg2' => "B",
					'pg3' => "C",
					'pg4' => "D",
					'pg5' => "E"
					);	
			}



			//checking status
			if($data->status_ujian == "tertunda")
			{
				$transfer['timer'] = $data->waktu_sisa;

				$this->db->update('el_ujian_siswa',array("pengulangan" => $data->pengulangan + 1),array('id_ujian_siswa' => $id_ujian_siswa));
			}else if($data->status_ujian == "belum"){
				$transfer['timer'] = $data->waktu;

				$this->db->update('el_ujian_siswa',array("pengulangan" => $data->pengulangan + 1,"status_ujian" => "tertunda","waktu_sisa" => $data->waktu),array('id_ujian_siswa' => $id_ujian_siswa));
			}else{
				redirect("login");
			}

			//pengambilan data yang diperlukan
			$transfer['soal'] 		= $soal;
			$transfer['random'] 	= $random;
			$transfer['data'] 		= $data;
			$transfer['kategori'] = $kategori;
			$transfer['santri'] 	= $this->dbmine->listSantri(array('master_rombel_siswa.id_santri' => $_SESSION['id_siswa']))->row();

			// echo "<pre>";
			// print_r($soal);exit;

			$this->myload->display("page/ulangan/mulai",$transfer,"siswa");
		}

		
		
	}

	protected function curang($id_semester=false,$kategori=false,$id_ujian_siswa=false)
	{
		$transfer['kategori'] = $kategori;
		$this->myload->display("page/ulangan/curang",$transfer,"siswa");
	}


	protected function ajax_memilih()
	{
		$pilihan = $_POST['xpilihan'];
		$id_pilihan_soal = $_POST['xid_pilihan_soal'];
		$id_ujian_siswa = $_POST['xid_ujian_siswa'];

		$where = array('id_pilihan_soal' => $id_pilihan_soal, 'id_ujian_siswa' => $id_ujian_siswa, 'id_siswa' => $_SESSION['id_siswa']);
		$cek = $this->dbsystem->getData('*','el_pilihan_jawaban',$where)->row();
		if(count($cek) >= "1")
		{
			$this->db->update('el_pilihan_jawaban',array('pilihan' => $pilihan),$where);
			$this->db->update('el_ujian_siswa',array('waktu_sisa' => $_POST['xtime']),array('id_ujian_siswa' => $id_ujian_siswa));
		}
		else if(count($cek) == 0)
		{
			$this->db->insert('el_pilihan_jawaban',array('id_pilihan_soal' => $id_pilihan_soal, 'id_ujian_siswa' => $id_ujian_siswa, 'id_siswa' => $_SESSION['id_siswa'],'pilihan' => $pilihan));
			$this->db->update('el_ujian_siswa',array('waktu_sisa' => $_POST['xtime']),array('id_ujian_siswa' => $id_ujian_siswa));
		}
		echo $_POST['xnomor'];
	}

	protected function ajax_pindah()
	{
		$time = $_POST['xtime'];
		$id_ujian_siswa = $_POST['xid_ujian_siswa'];
		$id_ujian = $_POST['xid_ujian'];
		$random = $_POST['xrandom'];
		$nomor = $_POST['xnomor'];

		$this->db->update('el_ujian_siswa',array('waktu_sisa' => $_POST['xtime']),array('id_ujian_siswa' => $id_ujian_siswa));

		$aa = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $id_ujian),$random)->result_array();
		$soal = array();
		$soal[0] = $aa[$nomor - 1];
		$jawaban = $this->dbsystem->getData('*','el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'id_siswa' => $_SESSION['id_siswa']))->result_array();
		for($x=0;$x<count($soal);$x++) {
			$soal[$x]['jawaban'] = "";
			foreach ($jawaban as $j) {
				if($j['id_pilihan_soal'] == $soal[$x]['id_pilihan_soal'])
				{
					$soal[$x]['jawaban'] = $j['pilihan'];
				}
				
			}
		}

		$hasil = array();
		$hasil = $soal[0];

		echo json_encode($hasil); 
	}


	protected function proses_nilai($id_semester=false,$kategori = false,$id_ujian_siswa=false,$id_ujian=false)
	{
		$data = $this->dbmine->listUjian(array('el_ujian_siswa.id_ujian_siswa' => $id_ujian_siswa))->row();
		
		/////////////// TIPE PILIHAN
		if($data->tipe_ujian == "pilihan"){

			$soal = $this->dbsystem->getData('*','el_pilihan_soal',array('id_ujian' => $data->id_ujian))->result();
			$jawaban = $this->dbsystem->getData('*','el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'id_siswa' => $_SESSION['id_siswa']))->result();

			
			foreach ($jawaban as $j) {
				foreach ($soal as $s) {
					if($j->id_pilihan_soal == $s->id_pilihan_soal){
						if($j->pilihan == $s->jawaban)
						{
							$this->db->update('el_pilihan_jawaban',array('status_jawaban' => "benar"),array('id_pilihan_jawaban' => $j->id_pilihan_jawaban));
						}else{
							$this->db->update('el_pilihan_jawaban',array('status_jawaban' => "salah"),array('id_pilihan_jawaban' => $j->id_pilihan_jawaban));
						}
					}
				}
			}

			$benar = $this->dbsystem->getData('*','el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'status_jawaban' => "benar"))->num_rows();
			
			$total = count($soal);
			$per_soal = 100 / $total;
			$nilai = substr($per_soal * $benar,0,4);
			
			$update = $this->db->update('el_ujian_siswa',array('nilai_ujian' => $nilai,'status_ujian' => "sudah"),array('id_ujian_siswa' => $id_ujian_siswa));
			$this->session->set_flashdata('berhasil', 'Anda telah menyelesaikan salah satu ujian !');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','index',array('id' => $id_semester, 'id2' => $kategori));
			redirect($redirek);
			//print $update;
		}
		////////////// END PILIHAN

	}



}
/* End of file Ulangan.php */
/* Location: ./application/controllers/Ulangan.php */