<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends MY_Controller {

	function __construct(){
    	parent::__construct();
    	$this->securityForAdminGuru();
    	date_default_timezone_set('Asia/Jakarta');

    	$this->load->model('dbujian');
  	}

	protected function index($id_semester=false,$kategori=false)
	{
		
		$transfer['semester'] = $this->dbujian->daftar_semester()->result();
		$transfer['sekolah'] = $this->dbsystem->getData('*','master_sekolah')->result();
		$transfer['mapel'] = $this->dbsystem->getData('*','el_mapel')->result();
		$transfer['kategori'] = $kategori;

		if($_SESSION['akses'] == "admin"){
			$this->myload->display('page/rekap/show',$transfer);
		}else{
			$this->myload->display('page/rekap/show',$transfer,"guru");
		}
	}

	protected function show_action($id_semester=false,$kategori=false)
	{
		$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','showing',array('id2' => $_POST['id_semester'], 'id3' => $kategori,'id4' => $_POST['id_sekolah'],'id' => $_POST['id_mapel']));
		redirect($redirek);
		// print_r($_POST);
	}

	protected function showing($id_semester=false,$kategori=false,$id_sekolah=false,$id_mapel=false)
	{
			if($id_semester==false or $kategori==false or $id_sekolah==false or $id_mapel==false){
				$smester = $this->dbsystem->getData('*','master_ajaran_semester',array('status' => 'Y'))->row()->id;
				if($kategori){
					$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $smester,'id2' => $kategori));
				}else{
					$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $smester,'id2' => 'harian'));
				}
				redirect($redirek);
			}


		if($_SESSION['akses'] == "admin"){
				$where = array(
				'el_ujian.kategori_ujian' => $kategori,
				'master_sekolah.id' => $id_sekolah,
				'el_mapel.id_mapel' => $id_mapel,
				'el_ujian_launch.id_semester' => $id_semester
				);
			$transfer['list'] = $this->dbujian->dataUjian($where,'el_ujian_launch.waktu_dari desc')->result();
			$transfer['kategori'] = $kategori;
			$transfer['id_sekolah'] = $id_sekolah;

			$this->myload->display('page/rekap/list',$transfer);
		}else{
				$where = array(
				'el_ujian.kategori_ujian' => $kategori,
				'master_sekolah.id' => $id_sekolah,
				'el_mapel.id_mapel' => $id_mapel,
				'master_pegawai.id' => $_SESSION['id_guru'],
				'el_ujian_launch.id_semester' => $id_semester
				);
			$transfer['list'] = $this->dbujian->dataUjian($where,'el_ujian_launch.waktu_dari desc')->result();
			$transfer['kategori'] = $kategori;
			$transfer['id_sekolah'] = $id_sekolah;

			$this->myload->display('page/rekap/list',$transfer,"guru");
		}
		

	}

	protected function list_siswa($id_semester=false,$kategori=false,$id_ujian_launch=false)
	{
		$transfer['ujian'] = $this->dbujian->dataUjian(array('el_ujian_launch.id_ujian_launch' => $id_ujian_launch))->row();
		$transfer['ujian_siswa'] = $this->dbujian->findSantriByUjian(array('el_ujian_siswa.id_ujian_launch' => $id_ujian_launch))->result();
		
			$transfer['kategori'] = $kategori;
			$transfer['id_ujian_launch'] = $id_ujian_launch;

		// print_r($transfer['ujian_siswa']);
		if($_SESSION['akses'] == "admin"){
			$this->myload->display('page/rekap/list_siswa',$transfer);
		}else{
			$this->myload->display('page/rekap/list_siswa',$transfer,"guru");
		}
	} 

	protected function list_siswa_reset($id_semester=false,$kategori=false,$id_ujian_launch=false,$id_ujian_siswa=false,$id_siswa=false,$tipe=false)
	{
		if($tipe == "pilihan")
		{
			$this->db->delete('el_pilihan_jawaban',array('id_ujian_siswa' => $id_ujian_siswa,'id_siswa' => $id_siswa));
			$this->db->update('el_ujian_siswa',array('status_ujian' => 'belum'),array('id_ujian_siswa' => $id_ujian_siswa));

			$this->session->set_flashdata('berhasil', 'Nilai siswa berhasil di reset.');
			$redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','list_siswa',array('id' => $id_semester,'id2' => $kategori, 'id3' => $id_ujian_launch));
			redirect($redirek);
		}

	} 


}

/* End of file Rekap.php */
/* Location: ./application/controllers/Rekap.php */
