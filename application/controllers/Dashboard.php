<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct(){
    	parent::__construct();
    	$this->securityForAll();
    	$this->load->model('dbsantri','dbmine');
  	}

	protected function guru($id_semester=false)
	{
		$this->load->model('dbguru');
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row()->id;

		$transfer['mapel'] = $this->dbguru->findMapelGuru(array('id_guru' => $_SESSION['id_guru']))->result();
		$transfer['rombel'] = $this->dbguru->findRombelGuru(array('id_guru' => $_SESSION['id_guru']))->result();
		$this->myload->display('page/dashboard/dashboard_guru',$transfer,'guru');
	}

	protected function siswa($id_semester=false)
	{
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row()->id;
		$transfer['siswa'] = $this->dbmine->listSantri(array('master_rombel.tahun_ajaran' => $tahun,'master_santri.id' => $_SESSION['id_siswa']))->row();
		$this->myload->display("page/dashboard/dashboard_siswa",$transfer,"siswa");
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
