<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ujian extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //error_reporting(0);
        $this->securityForAdminGuru();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('dbujian', 'dbmine');
    }

    protected function list_admin($id_semester=false, $kategori=false)
    {
        if ($this->session->flashdata('ruang_kosong')) {
            ?>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <div class="modal fade" id="masukanruang" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                            <div class="input-group">
                                <span class="input-group-addon">Pilih Ruang</span>
                                <select type="text" name="ruang">
                                    <option value="1">MA</option>
                                    <option value="2">MTs</option>
                                    <option value="3">SMP</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="submit_ruang" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <script> 
                $('#masukanruang').modal('show');
                $("#submit_ruang").click(function( event ) {

                    $.post( 
                    "<?php echo base_url(); ?>index.php/login/cekruang",
                    { ruang: "Zara" },
                    function(data) {
                        alert(data);
                    }
                );
                        
                });
            </script>
            <?php
        }

        $today = array(
            'el_ujian_launch.id_semester' => $id_semester,
            'el_ujian.kategori_ujian' => $kategori,
            'el_ujian_launch.waktu_sampai > ' => date('Y-m-d')
            );
        $transfer['rombel_list']   = $this->dbsystem->findnihairombel($id_semester);
        $transfer['kategori']      = $kategori;
        $transfer['list_new']      = $this->dbmine->dataUjian($today)->result();
        $transfer['id_semester']   = $id_semester;
        $this->myload->display('page/ujian/list', $transfer);
    }

    public function cek_statussycn()
    {
        $ruang = $this->dbsystem->getData('*', 'el_ruang', array('id' => '1'))->row()->ruang;
        $otherdb = $this->load->database('pusat', true);
        $status_sycn = $otherdb->query("SELECT * FROM el_sycn_ruang
                                         WHERE  id_sycn = $ruang")->row()->status_sycn;
        $data = (int)$status_sycn;

        echo json_encode($data);
    }
      
    protected function upload_ujian($id_semester=false, $kategori=false)
    {
        ?> 
            <div style="width:75%; background-color:#28b2bc; color:#FFFFFF; padding:15px; margin-top:10px; font-size:22px">Upload Data Progress Status</div>
            <br>
            <a style="display:none" id="kembali" class="btn-primary btn btn-sm"  href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori)); ?>">
                <i class="icon-file"></i> Kembali
            </a>
            <br><br>

            <!-- Progress bar holder -->
            <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">Data</li><li style="text-align:right; display:none" id="statusdata">Selesai</li></ul></div>
            <br>
            <div id="progress" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
            <!-- Progress information -->
            <div id="information1" style="width"></div>
            <?php

        $otherdb = $this->load->database('pusat', true);
        $ujian_sudah = $this->db->query("SELECT * FROM el_ujian_siswa 
                                         WHERE  el_ujian_siswa.status_ujian = 'sudah'");
        $i = 1;
        $baris = $ujian_sudah->num_rows();
        foreach ($ujian_sudah->result_array() as $s2) {
            $data = array(
                'nilai_ujian' => $s2['nilai_ujian'],
                'waktu_sisa' => $s2['waktu_sisa'],
                'pengulangan' => $s2['pengulangan'],
                'status_ujian' => $s2['status_ujian'],
                'status_remedial' => $s2['status_remedial']
                );

            
            $percent = intval($i/$baris * 100)."%";
            // Javascript for updating the progress bar and information
            echo '<script language="javascript">
            document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
            document.getElementById("information1").innerHTML="  Upload Berkas  <b>'.$i.'</b> data of <b>'. $baris.'</b> ... '.$percent.'  Completed.";			
            </script>';
            // This is for the buffer achieve the minimum size in order to flush data
            echo str_repeat(' ', 1024*64);
            // Send output to browser immediately
            flush();
            // Tell user that the process is completed
            $otherdb->where('id_ujian_siswa', $s2['id_ujian_siswa']);
            $otherdb->update('el_ujian_siswa', $data);
            $i++;
        }

        echo "<script>document.getElementById('statusdata').style.display='block';document.getElementById('kembali').style.display='block';</script>";
    }

    protected function sycn($id_semester=false, $kategori=false)
    {
        $datasantri_sycn = isset($_POST['datasantri']);
        $paketsoal_sycn = isset($_POST['paketsoal']);
        $ruang = $this->dbsystem->getData('*', 'el_ruang', array('id' => '1'))->row()->ruang;
        $otherdb = $this->load->database('pusat', true);

        if (($datasantri_sycn == null) && ($paketsoal_sycn == null)) {
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        } else {
            ?> 
            <div style="width:75%; background-color:#28b2bc; color:#FFFFFF; padding:15px; margin-top:10px; font-size:22px">Sync Progress Status</div>
            <br>
            <a style="display:none" id="kembali" class="btn-primary btn btn-sm"  href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori)); ?>">
                <i class="icon-file"></i> Kembali
            </a>
            <br><br>
            <?php

            
            

            if ($datasantri_sycn) {
                ?>
                
            <!-- Progress bar holder -->
            <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">DATA 1 : Data Siswa</li><li style="text-align:right; display:none" id="statusdata">Selesai</li></ul></div>
            <br>
            <div id="progress" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
            <!-- Progress information -->
            <div id="information1" style="width"></div>
            <hr style="width:75%; text-align:left; margin-left:0px; padding:0px">

        <?php

                
                $this->db->truncate('master_santri');
                $ceksiswa = $otherdb->query("SELECT
                                    master_santri.id as id_santri,
                                    master_santri.nama,
                                    master_santri.jk,
                                    el_ruang_ujian.ruang,
                                    master_santri.PASSWORD,
                                    master_santri.induk,
                                    master_santri.foto
                                FROM
                                    master_santri
                                    INNER JOIN el_ruang_ujian ON el_ruang_ujian.id_siswa = master_santri.induk 
                                WHERE ruang = $ruang");
                $i = 1;
                $baris = $ceksiswa->num_rows();
                foreach ($ceksiswa->result_array() as $s4) {
                    $data2 = array(
                'id' => $s4['id_santri'],
                'nama' => $s4['nama'],
                'jk' => $s4['jk'],
                'induk' => $s4['induk'],
                'password' => $s4['PASSWORD'],
                'foto' => $s4['foto']
                );
                    $id_insert1 = $this->dbsystem->insertData('master_santri', $data2);
                    $foto = "https://res.cloudinary.com/dqq8siyfu/image/upload/w_200,q_auto:good/fotosantriaws/".$s4['id_santri']."/".$s4['foto'];
                    if (@getimagesize($foto)) {
                        copy($foto, "asset/fotosantri/".$s4['foto']);
                    }
            
                    $percent = intval($i/$baris * 100)."%";
                    // Javascript for updating the progress bar and information
                    echo '<script language="javascript">
			document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
		    document.getElementById("information1").innerHTML="  Download Berkas  <b>'.$i.'</b> row(s) of <b>'. $baris.'</b> ... '.$percent.'  Completed.";			
            </script>';
                    // This is for the buffer achieve the minimum size in order to flush data
                    echo str_repeat(' ', 1024*64);
                    // Send output to browser immediately
                    flush();
                    // Tell user that the process is completed
                    $i++;
                }
                echo "<script>document.getElementById('statusdata').style.display='block';</script>";
                if (!$paketsoal_sycn) {
                    echo "<script>document.getElementById('kembali').style.display='block';</script>";
                }
            }
            if ($paketsoal_sycn) {
                $this->db->truncate('el_ujian_launch');
                $this->db->truncate('el_ujian');
                $this->db->truncate('el_pilihan_soal');
                $this->db->truncate('el_ujian_siswa');
                $this->db->truncate('el_pilihan_jawaban'); ?>
                <!-- Progress bar holder -->
                <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">DATA 2 : Data Ujian</li><li style="text-align:right; display:none" id="statusdata2">Selesai</li></ul></div>
                <br>
                <div id="progress2" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
                <!-- Progress information -->
                <div id="information2" style="width"></div>

                <!-- Sinkron Paket Soal -->
                <!-- Progress bar holder -->
                <hr style="width:75%; text-align:left; margin-left:0px; padding:0px">
                <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">DATA 3 : Paket Soal</li><li style="text-align:right; display:none" id="statusdata3">Selesai</li></ul></div>
                <br>
                <div id="progress3" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
                <!-- Progress information -->
                <div id="information3" style="width"></div>

                <!-- Sinkron Soal -->
                <!-- Progress bar holder -->
                <hr style="width:75%; text-align:left; margin-left:0px; padding:0px">
                <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">DATA 4 : Data Ujian Santri</li><li style="text-align:right; display:none" id="statusdata4">Selesai</li></ul></div>
                <br>
                <div id="progress4" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
                <!-- Progress information -->
                <div id="information4" style="width"></div>

                <!-- Sinkron Mapel -->
                <hr style="width:75%; text-align:left; margin-left:0px">
                <div style="margin-top:10px;width:77%;"><ul class="an"><li style="margin-left:-40px;">DATA 5 : Data Ujian Santri</li><li style="text-align:right; display:none" id="statusdata5">Selesai</li></ul></div>
                <br>
                <div id="progress5" style="width:75%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px"></div>
                <!-- Progress information -->
                <div id="information5" style="width"></div>

               
                <?php
                $ujian = $otherdb->query("SELECT DISTINCT
                                                DATE(el_ujian_launch.waktu_sampai) as tanggal_ujian,
                                                el_ujian.* 
                                            FROM
                                                el_ujian_launch
                                                INNER JOIN el_ujian ON el_ujian_launch.id_ujian = el_ujian.id_ujian 
                                            WHERE
                                                el_ujian_launch.waktu_sampai > CURDATE()");
                $i_el_ujian = 1;
                $baris_el_ujian = $ujian->num_rows();
                foreach ($ujian->result_array() as $s1) {
                    $data1 = array(
                'id_ujian' => $s1['id_ujian'],
                'id_guru' => $s1['id_guru'],
                'id_mapel' => $s1['id_mapel'],
                'id_semester' => $id_semester,
                'nama_ujian' => $s1['nama_ujian'],
                'tipe_ujian' => $s1['tipe_ujian'],
                'kategori_ujian' => $s1['kategori_ujian'],
                'waktu' => $s1['waktu']
                );
                    $id_insert1 = $this->dbsystem->insertData('el_ujian', $data1);


                    $percent_el_ujian = intval($i_el_ujian/$baris_el_ujian * 100)."%";
                    // Javascript for updating the progress bar and information
                    echo '<script language="javascript">
            document.getElementById("progress2").innerHTML="<div style=\"width:'.$percent_el_ujian.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
            document.getElementById("information2").innerHTML="  Download Berkas  <b>'.$i_el_ujian.'</b> row(s) of <b>'. $baris_el_ujian.'</b> ... '.$percent_el_ujian.'  Completed.";
            </script>';
                    // This is for the buffer achieve the minimum size in order to flush data
                    echo str_repeat(' ', 1024*64);
                    // Send output to browser immediately
                    flush();
                    // Tell user that the process is completed
                    $i_el_ujian++;

                    $soal_ujian = $otherdb->select('*')->where('id_ujian', $s1['id_ujian'])->get('el_pilihan_soal');
                    $i_pilihan_soal = 1;
                    $baris_pilihan_soal = $soal_ujian->num_rows();
                    foreach ($soal_ujian->result_array() as $s5) {
                        $data_soal = array(
                    'id_pilihan_soal' => $s5['id_pilihan_soal'],
                    'id_ujian' => $s5['id_ujian'],
                    'pertanyaan' => $s5['pertanyaan'],
                    'pertanyaan_gambar' => $s5['pertanyaan_gambar'],
                    'pg1' => $s5['pg1'],
                    'pg2' => $s5['pg2'],
                    'pg3' => $s5['pg3'],
                    'pg4' => $s5['pg4'],
                    'pg5' => $s5['pg5'],
                    'pg1_gambar' => $s5['pg1_gambar'],
                    'pg2_gambar' => $s5['pg2_gambar'],
                    'pg3_gambar' => $s5['pg3_gambar'],
                    'pg4_gambar' => $s5['pg4_gambar'],
                    'pg5_gambar' => $s5['pg5_gambar'],
                    'jawaban' => $s5['jawaban']
                    );

                        $id_insert_soal = $this->dbsystem->insertData('el_pilihan_soal', $data_soal);
                        if ($s5['pertanyaan_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pertanyaan_gambar'], "file/soal/".$s5['pertanyaan_gambar']);
                        }
                        if ($s5['pg1_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pg1_gambar'], "file/soal/".$s5['pg1_gambar']);
                        }
                        if ($s5['pg2_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pg2_gambar'], "file/soal/".$s5['pg2_gambar']);
                        }
                        if ($s5['pg3_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pg3_gambar'], "file/soal/".$s5['pg3_gambar']);
                        }
                        if ($s5['pg4_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pg4_gambar'], "file/soal/".$s5['pg4_gambar']);
                        }
                        if ($s5['pg5_gambar']) {
                            copy("https://app2.daarululuumlido.com/e_learning/file/soal/".$s5['pg5_gambar'], "file/soal/".$s5['pg5_gambar']);
                        }

                        $percent_pilihan_soal = intval($i_pilihan_soal/$baris_pilihan_soal * 100)."%";
                        // Javascript for updating the progress bar and information
                        echo '<script language="javascript">
                document.getElementById("progress3").innerHTML="<div style=\"width:'.$percent_pilihan_soal.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
                document.getElementById("information3").innerHTML="  Download Berkas  <b>'.$i_pilihan_soal.'</b> row(s) of <b>'. $baris_pilihan_soal.'</b> ... '.$percent_pilihan_soal.'  Completed.";
                </script>';
                        // This is for the buffer achieve the minimum size in order to flush data
                        echo str_repeat(' ', 1024*64);
                        // Send output to browser immediately
                        flush();
                        // Tell user that the process is completed
                        $i_pilihan_soal++;
                    }
                }

                echo "<script>document.getElementById('statusdata2').style.display='block'; document.getElementById('statusdata3').style.display='block';</script>";

                $ujian_launch = $otherdb->query("SELECT 
                                                    DATE( el_ujian_launch.waktu_sampai ) AS tanggal_ujian,
                                                    el_ujian_launch.*,
                                                    el_ujian.nama_ujian 
                                                FROM
                                                    el_ujian_launch
                                                    INNER JOIN el_ujian ON el_ujian_launch.id_ujian = el_ujian.id_ujian 
                                                WHERE
                                                    el_ujian_launch.waktu_sampai > CURDATE( )");
                $i_ujian_launch = 1;
                $baris_ujian_launch = $ujian_launch->num_rows();
                foreach ($ujian_launch->result_array() as $s) {
                    $data_ujian_launch = array(
                    'id_ujian_launch' => $s['id_ujian_launch'],
                    'id_guru' => $s['id_guru'],
                    'id_ujian' => $s['id_ujian'],
                    'id_rombel' => $s['id_rombel'],
                    'id_semester' => $id_semester,
                    'waktu_dari' => str_replace("/", "-", $s['waktu_dari'].":00"),
                    'waktu_sampai' => str_replace("/", "-", $s['waktu_sampai'].":00"),
                    'status_random' => $s['status_random']
                    );
                    $this->dbsystem->insertData('el_ujian_launch', $data_ujian_launch);
                    $percent_ujian_launch = intval($i_ujian_launch/$baris_ujian_launch * 100)."%";
                    // Javascript for updating the progress bar and information
                    echo '<script language="javascript">
            document.getElementById("progress4").innerHTML="<div style=\"width:'.$percent_ujian_launch.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
            document.getElementById("information4").innerHTML="  Download Berkas  <b>'.$i_ujian_launch.'</b> row(s) of <b>'. $baris_ujian_launch.'</b> ... '.$percent_ujian_launch.'  Completed.";
            </script>';
                    // This is for the buffer achieve the minimum size in order to flush data
                    echo str_repeat(' ', 1024*64);
                    // Send output to browser immediately
                    flush();
                    // Tell user that the process is completed
                    $i_ujian_launch++;

                    $ujian_launch_siswa = $otherdb->query("SELECT el_ujian_siswa.*,master_santri.nama,el_ruang_ujian.ruang FROM el_ujian_siswa LEFT JOIN master_santri ON master_santri.id=el_ujian_siswa.id_siswa LEFT JOIN el_ruang_ujian ON master_santri.induk=el_ruang_ujian.id_siswa WHERE el_ujian_siswa.id_ujian_launch=".$s['id_ujian_launch']." AND el_ruang_ujian.ruang=$ruang");
                    $i_ujian_siswa = 1;
                    $baris_ujian_siswa = $ujian_launch_siswa->num_rows();
                    foreach ($ujian_launch_siswa->result_array() as $s2) {
                        $data_ujian_launch_siswa = array(
                                'id_ujian_siswa' => $s2['id_ujian_siswa'],
                                'id_ujian_launch' => $s['id_ujian_launch'],
                                'id_siswa' => $s2['id_siswa'],
                                'nilai_ujian' => $s2['nilai_ujian'],
                                'waktu_sisa' => $s2['waktu_sisa'],
                                'pengulangan' => $s2['pengulangan'],
                                'status_ujian' => $s2['status_ujian'],
                                'status_remedial' => $s2['status_remedial']
                                );
                        $this->dbsystem->insertData('el_ujian_siswa', $data_ujian_launch_siswa);

                        $percent_ujian_siswa = intval($i_ujian_siswa/$baris_ujian_siswa* 100)."%";
                        // Javascript for updating the progress bar and information
                        echo '<script language="javascript">
                document.getElementById("progress5").innerHTML="<div style=\"width:'.$percent_ujian_siswa.';background-image:url('.base_url().'asset/template/img/pbar-ani1.gif);\">&nbsp;</div>";
                document.getElementById("information5").innerHTML="  Download Berkas  <b>'.$i_ujian_siswa.'</b> row(s) of <b>'. $baris_ujian_siswa.'</b> ... '.$percent_ujian_siswa.'  Completed.";
                </script>';
                        // This is for the buffer achieve the minimum size in order to flush data
                        echo str_repeat(' ', 1024*64);
                        // Send output to browser immediately
                        flush();
                        // Tell user that the process is completed
                        $i_ujian_siswa++;
                    }
                }

                echo "<script>document.getElementById('statusdata4').style.display='block'; document.getElementById('statusdata5').style.display='block'; document.getElementById('kembali').style.display='block';</script>";
            } ?>

            <?php
            $otherdb->where('id_sycn', $ruang);
            $otherdb->update('el_sycn_ruang', array('status_sycn' => '1'));
            $this->session->set_flashdata('berhasil', 'Penambahan Ujian Berhasil.');
        }
    }

    protected function list_statusujian($id_semester=false, $kategori=false)
    {
        $transfer['kategori'] = $kategori;
        $transfer['id_semester'] = $id_semester;
        //$ujians = $this->dbmine->jjj($id_semester, $kategori);
        //print_r($ujians);
        //$transfer['transfser'] = $ujians;
        $this->myload->display('page/ujian/monitor_ujian', $transfer);
    }
    protected function list_statusujian_ajax($id_semester=false, $kategori=false)
    {
        $ujians = $this->dbmine->jjj($id_semester, $kategori);
        $no=0;
        foreach ($ujians as $l) {
            $no++; ?>
        <tr>
            <td style="font-size: 14px; border: thin solid #cacaca;">
                <?php echo $no; ?>
            </td>
            <td  style="font-size: 14px; border: thin solid #cacaca;">
                <?php echo $l['nama']; ?>
            </td>
            <td  style="font-size: 14px; border: thin solid #cacaca;">
                <?php echo $l['kelas']; ?>
            </td>
            <td  style="font-size: 14px; border: thin solid #cacaca;">
                <?php echo $l['nama_mapel']; ?>
            </td>
            <td  style="font-size: 14px; border: thin solid #cacaca;">
                <?php echo $l['nilai_ujian']; ?>
            </td>
            <td  style="font-size: 14px; border: thin solid #cacaca;">
                <?php if ($l['status_ujian'] == "sudah") {
                echo "Selesai" ?>
                <?php
            } elseif ($l['status_ujian'] == "belum") {
                ?>
                <span class="label-default label">Belum</span>
                <?php
            } elseif ($l['status_ujian'] == "tertunda") {
                ?>
                <span class="label-warning label">Tertunda</span>
                <?php
            } ?>
            </td>
        </tr>
        <?php
        }
    }

    protected function tambah_admin($id_semester=false, $kategori=false)
    {
        $transfer['kategori'] = $kategori;
        $transfer['mapel'] = $this->dbsystem->getdata('*', 'el_mapel')->result();
        
        $this->myload->display('page/ujian/tambah_admin', $transfer);
    }

    protected function ajax_mapel()
    {
        $ajaran = $this->dbsystem->getData('*', 'master_ajaran', array('status' => 'Y'))->row()->id;
        $where = array(
            'el_mapel_guru.id_mapel' => $_POST['id_mapel'],
            'el_mapel_guru.id_ajaran' => $ajaran
            );
        $data = array();
        $data = $this->dbmine->findGuruByMapel($where)->result_array();
        echo json_encode($data);
    }

    protected function ajax_findsoal()
    {
        $data = array();
        $data = $this->dbsystem->getData('*', 'el_ujian', $_POST)->result_array();
        echo json_encode($data);
    }

    protected function ajax_findrombel()
    {
        $data = array();
        $data = $this->dbmine->findrombel(array('el_rombel_guru.id_guru' => $_POST['id_guru']))->result_array();
        echo json_encode($data);
    }

    protected function tambah_action_admin($id_semester=false, $kategori=false)
    {
        // checking pertama
        if (empty($_POST['id_guru']) or empty($_POST['id_rombel']) or empty($_POST['id_mapel']) or empty($_POST['id_ujian'])) {
            $this->session->set_flashdata('gagal', 'Form tidak Lengkap');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }

        // checking ke 2
        $waktu_dari = strtotime($_POST['waktu_dari']);
        $waktu_sampai = strtotime($_POST['waktu_sampai']);
        $waktu_sekarang = strtotime(date('Y-m-d H:i'));
        if ($waktu_sekarang > $waktu_sampai) {
            $this->session->set_flashdata('gagal', 'Waktu Yang ditetapkan tidak boleh hari yang sudah berlalu');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        } elseif ($waktu_sampai < $waktu_dari) {
            $this->session->set_flashdata('gagal', 'Penetapan waktu salah.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }

        $data = array(
            'id_guru' => $_POST['id_guru'],
            'id_ujian' => $_POST['id_ujian'],
            'id_rombel' => $_POST['id_rombel'],
            'id_semester' => $id_semester,
            'waktu_dari' => str_replace("/", "-", $_POST['waktu_dari'].":00"),
            'waktu_sampai' => str_replace("/", "-", $_POST['waktu_sampai'].":00"),
            'status_random' => $_POST['status_random']
            );
        $id_insert = $this->dbsystem->insertData('el_ujian_launch', $data);
        if ($id_insert) {
            $siswa = $this->dbmine->findSantriByRombel(array('master_rombel_siswa.id_rombel' => $_POST['id_rombel']))->result();
            foreach ($siswa as $s) {
                $dd = array(
                    'id_siswa' => $s->id_santri,
                    'id_ujian_launch' => $id_insert
                    );
                $this->db->insert('el_ujian_siswa', $dd);
            }
            $this->session->set_flashdata('berhasil', 'Penambahan Ujian Berhasil.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        } else {
            $this->session->set_flashdata('gagal', 'Gagal Penambahan Ujian.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_admin', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }
    }


    protected function batalkan($id_semester=false, $kategori=false, $id_ujian_launch)
    {
        $where = array(
            'id_ujian_launch' => $id_ujian_launch,
            'status_ujian' => "sudah"
            );
        $cek = $this->dbsystem->getData('*', 'el_ujian_siswa', $where)->num_rows();
        if ($cek == 0) {
            $this->db->delete('el_ujian_siswa', array('id_ujian_launch' => $id_ujian_launch));
            $this->db->delete('el_ujian_launch', array('id_ujian_launch' => $id_ujian_launch));
            
            $this->session->set_flashdata('berhasil', 'Data Ujian berhasil di hapus.');
            
            if ($_SESSION['akses'] == "admin") {
                $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori));
            } else {
                $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_guru', array('id' => $id_semester,'id2' => $kategori));
            }
            redirect($redirek);
        } else {
            $this->session->set_flashdata('gagal', 'Gagal Pembatalan Karena Ujian sudah dikerjakan siswa.');
            if ($_SESSION['akses'] == "admin") {
                $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori));
            } else {
                $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_guru', array('id' => $id_semester,'id2' => $kategori));
            }
            
            redirect($redirek);
        }
    }


    // This is Teacher Section
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    protected function list_guru($id_semester=false, $kategori=false)
    {
        $where = array(
                'el_ujian_launch.id_semester' => $id_semester,
                'el_ujian.kategori_ujian' => $kategori,
                'el_ujian_launch.waktu_sampai >=' => date('Y-m-d h:i:s'),
                'el_ujian_launch.id_guru' => $_SESSION['id_guru']
                );

        $transfer['kategori'] = $kategori;
        $transfer['list_new'] = $this->dbmine->dataUjian($where)->result();

        $this->myload->display('page/ujian/list_guru', $transfer, "guru");
    }

    protected function tambah_guru($id_semester=false, $kategori=false)
    {
        $this->load->model('dbguru');
        $transfer['mapel'] = $this->dbguru->findMapelGuru(array('id_guru' => $_SESSION['id_guru']))->result();
        // echo "<pre>";
        // print_r($transfer);exit;
        $transfer['rombel'] = $this->dbguru->findRombelGuru(array('id_guru' => $_SESSION['id_guru']))->result();
        $transfer['kategori'] = $kategori;
        $this->myload->display('page/ujian/tambah_guru', $transfer, 'guru');
    }

    protected function ajax_findsoal_guru()
    {
        $where = $_POST;
        $where['id_guru'] = $_SESSION['id_guru'];

        $data = array();
        $data = $this->dbsystem->getData('*', 'el_ujian', $where)->result_array();
        echo json_encode($data);
    }

    protected function tambah_action_guru($id_semester=false, $kategori=false)
    {
        // checking pertama
        if (empty($_POST['id_rombel']) or empty($_POST['id_ujian'])) {
            $this->session->set_flashdata('gagal', 'Form tidak Lengkap');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_guru', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }

        // checking ke 2
        $waktu_dari = strtotime($_POST['waktu_dari']);
        $waktu_sampai = strtotime($_POST['waktu_sampai']);
        $waktu_sekarang = strtotime(date('Y-m-d H:i'));
        if ($waktu_sekarang > $waktu_sampai) {
            $this->session->set_flashdata('gagal', 'Waktu Yang ditetapkan tidak boleh hari yang sudah berlalu');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_guru', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        } elseif ($waktu_sampai < $waktu_dari) {
            $this->session->set_flashdata('gagal', 'Penetapan waktu salah.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_guru', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }

        // proses
        $data = array(
            'id_guru' => $_SESSION['id_guru'],
            'id_ujian' => $_POST['id_ujian'],
            'id_rombel' => $_POST['id_rombel'],
            'id_semester' => $id_semester,
            'waktu_dari' => str_replace("/", "-", $_POST['waktu_dari'].":00"),
            'waktu_sampai' => str_replace("/", "-", $_POST['waktu_sampai'].":00"),
            'status_random' => $_POST['status_random']
            );
        $id_insert = $this->dbsystem->insertData('el_ujian_launch', $data);
        if ($id_insert) {
            $siswa = $this->dbmine->findSantriByRombel(array('master_rombel_siswa.id_rombel' => $_POST['id_rombel']))->result();
            foreach ($siswa as $s) {
                $dd = array(
                    'id_siswa' => $s->id_santri,
                    'id_ujian_launch' => $id_insert
                    );
                $this->db->insert('el_ujian_siswa', $dd);
            }
            $this->session->set_flashdata('berhasil', 'Penambahan Ujian Berhasil.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_guru', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        } else {
            $this->session->set_flashdata('gagal', 'Gagal Penambahan Ujian.');
            $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'tambah_guru', array('id' => $id_semester,'id2' => $kategori));
            redirect($redirek);
        }
    }
}

/* End of file Ujian.php */
/* Location: ./application/controllers/Ujian.php */
