<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database','session','encrypt');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','html');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('dbsystem','mylibrary/mycrypt','mylibrary/myload');
