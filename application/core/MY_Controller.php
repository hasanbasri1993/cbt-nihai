<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  function __construct(){
    parent::__construct();
  }

  function mytoken($url){
        $data    = $this->nama_fungsi->dekripsi($url);
        if($data != false){
            if (method_exists($this, trim($data['function']))){
                if(!empty($data['params'])){
                    return call_user_func_array(array($this, trim($data['function'])), $data['params']);
                }else{
                   return $this->$data['function']();
                }
            }
       }
       show_404();
  }

  function mysecurity($url)
  {
    $data = $this->mycrypt->dekripsi($url);
    if($data != false){
          if (method_exists($this, trim($data['function']))){
              if(!empty($data['params'])){
                  return call_user_func_array(array($this, trim($data['function'])), $data['params']);
              }else{
                 return $this->$data['function']();
              }
          }
     }
     show_404();
  }

  function securityForAdmin()
  {
    if(isset($_SESSION['username']) or isset($_SESSION['akses']) or isset($_SESSION['grand']))
    {
      if($_SESSION['grand'] == "qq_elearning")
      {
        if($_SESSION['akses'] != "admin")
        {
          // $id_semester = $this->dbsystem->getData('*','master_ajaran_semester',array('status' => 'Y'))->row()->id;
          // $redirek = base_url().'index.php/'.$this->mycrypt->enkripsi('data_guru','index',array('id' => $id_semester));
          // redirect($redirek);
        }
      }
      else
      {
        redirect('login');  
      }
    }
    else
    {
      redirect('login');
    }
  }

  function securityForAdminGuru()
  {
    if(isset($_SESSION['username']) or isset($_SESSION['akses']) or isset($_SESSION['grand']))
    {
      if($_SESSION['grand'] == "qq_elearning")
      {
        if($_SESSION['akses'] != "admin" and $_SESSION['akses'] != "guru")
        {
          redirect('login');
        }
      }
      else
      {
        redirect('login');  
      }
    }
    else
    {
      redirect('login');
    }
  }

  function securityForAll()
  {
    if(isset($_SESSION['username']) or isset($_SESSION['akses']) or isset($_SESSION['grand']))
    {
      if($_SESSION['grand'] == "qq_elearning")
      {
        
      }
      else
      {
        redirect('login');  
      }
    }
    else
    {
      redirect('login');
    }
  }


  function CodeRandom($karakter=array(),$panjang=false)
  {
    $hasil = '';
    for($i=0;$i<$panjang;$i++)
    {
      $no = rand(0,count($karakter)-1);
      $kata = $karakter[$no];
      $hasil = $hasil.$kata;
    }
    return $hasil;
  }


}

