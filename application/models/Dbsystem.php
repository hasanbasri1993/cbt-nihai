<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbsystem extends CI_Model {

	function getData($select=false,$table=false,$where=false,$order=false,$limit=false,$start=false)
	{
		$this->db->select($select);
		if($where)$this->db->where($where);
		if($limit)$this->db->limit($limit,$start);
		if($order==true)$this->db->order_by($order);
		return $this->db->get($table);
	}
	function getJoin($select=false,$table=false,$where=false,$join=false,$join2=false,$join3=false,$order=false,$limit=false,$start=false)
	{
		$this->db->select($select);
		if($where)$this->db->where($where);
		if($join)
		{
			if(count($join) > 1){
				for ($i=0; $i < count($join); $i++) { 
					$this->db->join($join[$i],$join2[$i],$join3[$i]);
				}
			}

			if(count($join) == 1){
				$this->db->join($join,$join2,$join3);
			}
		}
		if($limit)$this->db->limit($limit,$start);
		if($order==true)$this->db->order_by($order);
		return $this->db->get($table);
	}
	
	function insertData($table=false,$param_data=false)
	{
		$this->db->insert($table,$param_data);
		return $this->db->insert_id();
	}

	function updateData($table=false,$param_data=false,$where=false)
	{
		$data = $this->db->update($table,$param_data,$where);
		return $data;
		
	}

	function findnihairombel($semester)
	{
		$sql ="
		SELECT
			CONCAT( master_sekolah.sekolah, ' ', master_kelas.kelas, ' ', master_rombel.rombel ) AS kelas,
			master_rombel.id as id_rombel
		FROM
			master_rombel
			JOIN master_kelas ON master_rombel.id_kelas = master_kelas.id
			JOIN master_sekolah ON master_kelas.id_sekolah = master_sekolah.id
			JOIN master_ajaran_semester ON master_ajaran_semester.id_ajaran = master_rombel.tahun_ajaran 
		WHERE
			master_ajaran_semester.id = $semester 
			AND ( kelas = 'XII' OR kelas = '6' )";
			$hasil = $this->db->query($sql);
			return $hasil->result_array();
	}


	// --------------------------------------------------------------------------
	// ---------------------------Encryption Kiikun------------------------------
	// -------------------------------------------------------------------------- 


}
