<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mytime extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	// %a %m %d %y %h %i
  	function DateDiffQQ($date_1 , $date_2 , $differenceFormat = '%a' )
  	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);

		$interval = date_diff($datetime1, $datetime2);
		return $interval->format($differenceFormat);
  	}	

}

/* End of file Mytime.php */
/* Location: ./application/models/mylibrary/Mytime.php */