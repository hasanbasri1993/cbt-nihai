<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mycrypt extends CI_Model {
	function __construct(){
    	parent::__construct();
    	//Your Costume
  	}
  	
	public $key="kiikun";
	public $key2="ryukun";
	public $number = "-38";
	
	function enkripsi($class=false,$function=false, $parameter = array())
	{
		$key = substr(md5($this->key), $this->number);
		$key2 = substr(md5($this->key2), $this->number);
		if($class!= false && $function != false)
		{
			if(!empty($parameter)){
				$param = '';
				foreach($parameter as $value){
		        	$param = $param.$this->encode_kata($value).'/';
		      	}
		      	$param = str_replace('/', $key2, $param);

		      	$data = $this->encode_kata($function).'/'.$param;
		      	$data = str_replace("/",$key, $data);
		      	return $class.'/mysecurity/'.$data;
		      }else{
		      	$data = $this->encode_kata($function).'/';
		      	$data = str_replace("/",$key, $data);
		      	return $class.'/mysecurity/'.$data;
		      }

		} 
      	
	}
	
	function encode_kata($kata="")
	{
		return base64_encode($kata);
		// return base64_encode(createCode($kata));
	}

	function decode_kata($kata="")
	{
		return base64_decode($kata);
		// return translateCode(base64_decode($kata));
	}

	function dekripsi($url)
	{
		$key = substr(md5($this->key), $this->number);
		$key2 = substr(md5($this->key2), $this->number);

		$first = str_replace($key,"/",$url);
		$first = explode('/', $first);

		if(!empty($first[1])){
			$parameternya = str_replace($key2, "/", $first[1]);
			$parameternya = explode("/", $parameternya);

			$zz = array();
			for($x = 0;$x<count($parameternya)-1;$x++)
			{
				$zz[$x] = $this->decode_kata($parameternya[$x]);
			}

			$array = array(
				'function' => $this->decode_kata($first[0]),
				'params' => $zz
			);
		}else{
			$array = array(
				'function' => $this->decode_kata($first[0]),
				'params' => array()
			);
		}
		
		
		return $array;
	}




	function encode_kata_2($kata=false)
	{
		return $this->encrypt->encode($kata,$this->key);
	}

	function decode_kata_2($kata=false)
	{
		return $this->encrypt->decode($kata,$this->key);
	}
}

/* End of file Mycrypt.php */
/* Location: ./application/models/Mycrypt.php */