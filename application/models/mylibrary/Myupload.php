<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myupload extends CI_Model {

	function __construct(){
		parent::__construct();
	}

  function gambar($name=false,$tempat_upload=false)
  {
    $config['upload_path']    = $tempat_upload;
    $config['allowed_types']  = "jpg|png|jpeg|png|bmp|gif";
    $config['max_size']     = 5000;
    $config['encrypt_name']   = true;

    $this->load->library('upload',$config);
    if($this->upload->do_upload($name))
    {
      $data = $this->upload->data();
      $data['status'] = "berhasil";
      return $data; 
    }
    else
    {
      $data = $this->upload->display_errors();
      $data['status'] = "gagal";
      return $data;
    }

  }

  function gambar_resizeW($name=false,$tempat_upload=false,$max_width=false)
  {
    $config['upload_path']    = $tempat_upload;
    $config['allowed_types']  = "jpg|png|jpeg|png|bmp|gif";
    $config['max_size']     = 5000;
    $config['encrypt_name']   = true;

    $this->load->library('upload',$config);
    if($this->upload->do_upload($name))
    {
      $data = $this->upload->data();
      $data['status'] = "berhasil";

      if($data['image_width'] > $max_width)
      {
        $persen = $max_width * 100 / $data['image_width'];
        $height = $persen * $data['image_height'] / 100;
            
        $this->load->library('image_lib');
        $this->image_lib->initialize(array(
          'image_library' => 'gd2',
          'source_image' => $tempat_upload.$data['file_name'],
          'maintain_ratio' => false,
          'quality' => '80%',
          'width' => $max_width,
          'height' => $height
        ));
        $this->image_lib->resize();
      }

      return $data; 
    }
    else
    {
      $data = $this->upload->display_errors();
      $data['status'] = "gagal";
      return $data;
    }
  }

  function gambar_resizeH($name=false,$tempat_upload=false,$max_height=false)
  {
    $config['upload_path']    = $tempat_upload;
    $config['allowed_types']  = "jpg|png|jpeg|png|bmp|gif";
    $config['max_size']     = 5000;
    $config['encrypt_name']   = true;

    $this->load->library('upload',$config);
    if($this->upload->do_upload($name))
    {
      $data = $this->upload->data();
      $data['status'] = "berhasil";

      if($data['image_height'] > $max_height)
      {
        $persen = $max_height * 100 / $data['image_height'];
        $width = $persen * $data['image_height'] / 100;
            
        $this->load->library('image_lib');
        $this->image_lib->initialize(array(
          'image_library' => 'gd2',
          'source_image' => $tempat_upload.$data['file_name'],
          'maintain_ratio' => false,
          'quality' => '80%',
          'width' => $width,
          'height' => $max_height
        ));
        $this->image_lib->resize();
      }

      return $data; 
    }
    else
    {
      $data = $this->upload->display_errors();
      $data['status'] = "gagal";
      return $data;
    }
  }


}

/* End of file Myupload.php */
/* Location: ./application/models/mylibrary/Myupload.php */