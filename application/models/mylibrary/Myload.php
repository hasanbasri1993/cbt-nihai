<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myload extends CI_Model {

	function __construct(){
    	parent::__construct();
    	//Your Costume
  	}

	function display($param=false,$data=false,$kategori=false)
	{
		$data['cf_tahun_semester'] = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
		$data['cf_semester'] = $this->dbsystem->getData('*','master_ajaran_semester',array('status' => 'Y'))->row();

		if($kategori==false)
		{
			if($data)$this->load->vars($data);
			$this->load->view('template/header');
			$this->load->view('template/header_menu');
			$this->load->view($param);
			$this->load->view('template/footer');
		}
		else if($kategori=="guru")
		{
			if($data)$this->load->vars($data);
			$this->load->view('template/header');
			$this->load->view('template/header_menu_guru');
			$this->load->view($param);
			$this->load->view('template/footer');
		}
		else if($kategori=="siswa")
		{
			if($data)$this->load->vars($data);
			$this->load->view('template/header');
			$this->load->view('template/header_menu_siswa');
			$this->load->view($param);
			$this->load->view('template/footer');
		}
	}
	  
	function view($param=false,$data=false)
	{
		$data['cf_tahun_semester'] = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row();
		$data['cf_semester'] = $this->dbsystem->getData('*','master_ajaran_semester',array('status' => 'Y'))->row();

		if($data)$this->load->vars($data);
		$this->load->view($param);
	}

}

/* End of file Myload.php */
/* Location: ./application/models/mylibrary/Myload.php */