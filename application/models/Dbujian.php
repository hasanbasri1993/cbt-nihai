<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbujian extends CI_Model {

	function dataUjian($where=false,$order_by=false)
	{
		$this->db->select('el_ujian_launch.waktu_dari,el_ujian_launch.waktu_sampai,el_ujian_launch.id_ujian_launch,el_ujian_launch.status_random');
		$this->db->select('master_rombel.*,master_kelas.*,master_sekolah.sekolah');
		$this->db->select('el_ujian.*,master_pegawai.nama');
		$this->db->select('el_mapel.nama_mapel');
		$this->db->select('master_ajaran.*, master_ajaran_semester.*');
		$this->db->join('el_ujian','el_ujian.id_ujian = el_ujian_launch.id_ujian');
		$this->db->join('master_rombel','master_rombel.id = el_ujian_launch.id_rombel');
		$this->db->join('master_pegawai','master_pegawai.id = el_ujian_launch.id_guru');
		$this->db->join('el_mapel','el_mapel.id_mapel = el_ujian.id_mapel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->join('master_sekolah','master_sekolah.id = master_kelas.id_sekolah');
		$this->db->join('master_ajaran_semester', 'master_ajaran_semester.id = el_ujian.id_semester');
		$this->db->join('master_ajaran','master_ajaran.id = master_ajaran_semester.id_ajaran');
		if($where)$this->db->where($where);
		if($order_by)$this->db->order_by($order_by);
		return $this->db->get('el_ujian_launch');
	}

	function findGuruByMapel($where=false)
	{
		$this->db->select('master_pegawai.*,el_mapel.*');
		$this->db->join('master_pegawai','master_pegawai.id = el_mapel_guru.id_guru');
		$this->db->join('el_mapel','el_mapel.id_mapel = el_mapel_guru.id_mapel');
		if($where)$this->db->where($where);
		$this->db->order_by('master_pegawai.nama asc');
		return $this->db->get('el_mapel_guru');
	}

	function findRombel($where=false)
	{
		$this->db->select('master_rombel.*, master_sekolah.sekolah,master_kelas.kelas');
		$this->db->select('el_rombel_guru.*');
		$this->db->join('master_rombel','master_rombel.id = el_rombel_guru.id_rombel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->join('master_sekolah','master_sekolah.id = master_kelas.id_sekolah');
		if($where)$this->db->where($where);
		$this->db->order_by('master_sekolah.sekolah asc');
		return $this->db->get('el_rombel_guru');
	}

	function findSantriByRombel($where=false)
	{
		$this->db->select('master_rombel_siswa.*,master_santri.id,master_santri.nama,master_santri.induk,master_santri.password');
		$this->db->join('master_santri','master_santri.id = master_rombel_siswa.id_santri');
		if($where)$this->db->where($where);
		$this->db->order_by('master_santri.nama asc');
		return $this->db->get('master_rombel_siswa');
	}

	function findSantriByUjian($where=false,$select=false,$group=false)
	{
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row()->id;
		$this->db->select('master_santri.nama,master_santri.jk,master_rombel_siswa.*,master_rombel.*,el_ujian_siswa.*,master_kelas.*');
		if($select)$this->db->select($select);
		$this->db->join('master_santri','master_santri.id = el_ujian_siswa.id_siswa');
		$this->db->join('master_rombel_siswa','master_santri.id = master_rombel_siswa.id_santri');
		$this->db->join('master_rombel','master_rombel.id = master_rombel_siswa.id_rombel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->where(array('master_rombel.tahun_ajaran' => $tahun));
		if($where)$this->db->where($where);
		if($group)$this->db->group_by($group);
		return $this->db->get('el_ujian_siswa');

	}
	
	function jjj($id_semester,$kategori){
		
		$where = array(
			'el_ujian_launch.id_semester' => $id_semester,
			'el_ujian.kategori_ujian' => $kategori,
			'el_ujian_launch.waktu_sampai >=' => date('Y-m-d h:i:s')
			);
		
		$ujians = $this->dataUjian($where)->result_array();
		$whereidujian="";
		foreach($ujians as $row)
			{
				$whereidujian.= "el_ujian_siswa.id_ujian_launch = ". $row['id_ujian_launch']." OR ";
			}
		 $where  = substr($whereidujian, 0, -3);
		 $tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row()->id;
		 $sql = "SELECT
							master_santri.nama,
							CONCAT( master_sekolah.sekolah, ' ', master_kelas.kelas, ' ', master_rombel.rombel ) AS kelas,
							el_ujian_siswa.status_ujian,
							el_ujian_siswa.status_remedial,
							el_mapel.nama_mapel,
							el_ujian_siswa.id_ujian_siswa,
							el_ujian_siswa.nilai_ujian
						FROM
							el_ujian_siswa
							JOIN master_santri ON master_santri.id = el_ujian_siswa.id_siswa
							JOIN master_rombel_siswa ON master_santri.id = master_rombel_siswa.id_santri
							JOIN master_rombel ON master_rombel.id = master_rombel_siswa.id_rombel
							JOIN master_kelas ON master_kelas.id = master_rombel.id_kelas
							JOIN master_sekolah ON master_sekolah.id = master_kelas.id_sekolah
							JOIN el_ujian_launch ON el_ujian_siswa.id_ujian_launch = el_ujian_launch.id_ujian_launch
							JOIN el_ujian ON el_ujian_launch.id_ujian = el_ujian.id_ujian
							JOIN el_mapel ON el_ujian.id_mapel = el_mapel.id_mapel 
						WHERE
							master_rombel.tahun_ajaran = $tahun
							AND el_ujian_siswa.status_remedial = 'N' 
							AND ( $where )";
		  $hasil = $this->db->query($sql);
			return $hasil->result_array();
	}
	
	
	
	function findnihairombel($semester)
	{
		$sql ="
		SELECT
			CONCAT( master_sekolah.sekolah, ' ', master_kelas.kelas, ' ', master_rombel.rombel ) AS kelas,
			master_rombel.id as id_rombel
		FROM
			master_rombel
			JOIN master_kelas ON master_rombel.id_kelas = master_kelas.id
			JOIN master_sekolah ON master_kelas.id_sekolah = master_sekolah.id
			JOIN master_ajaran_semester ON master_ajaran_semester.id_ajaran = master_rombel.tahun_ajaran 
		WHERE
			master_ajaran_semester.id = $semester 
			AND ( kelas = 'XII' OR kelas = '6' )";
			$hasil = $this->db->query($sql);
			return $hasil->result_array();
	}
	
	
	function findidujianlaunchmapel($semester,$id_rombel)
	{
			$sql1 = "SELECT
								id_ujian_launch,
								nama_mapel
							FROM
								 el_ujian
								JOIN  el_ujian_launch ON  el_ujian.id_ujian =  el_ujian_launch.id_ujian
								JOIN  el_mapel ON  el_ujian.id_mapel =  el_mapel.id_mapel 
							WHERE
								el_ujian_launch.id_rombel = $id_rombel
								AND el_ujian_launch.id_semester = $semester";
			$attributes = $this->db->query($sql1)->result_array();
			return $attributes;
	}
	
		function findidujianlaunchbyrombel1($semester,$id_rombel)
	{
			$attributes = $this->findidujianlaunchmapel($semester,$id_rombel);
			$nilai = "";
			$statusnilai = "";
			$idujiansiswa = "";
			foreach($attributes as $row)
			{
				$idujiansiswa.= "IFNULL((SELECT a.id_ujian_siswa FROM el_ujian_siswa a WHERE a.id_ujian_launch = ".$row['id_ujian_launch']." AND a.id_siswa=s.id GROUP BY a.id_siswa),0)'idujiansiswa".$row['nama_mapel']."' ,";
				$nilai.= "IFNULL((SELECT a.nilai_ujian FROM el_ujian_siswa a WHERE a.id_ujian_launch = ".$row['id_ujian_launch']." AND a.id_siswa=s.id GROUP BY a.id_siswa),0)'".$row['nama_mapel']."' ,";
				$statusnilai.= "IFNULL((SELECT a.status_ujian FROM el_ujian_siswa a WHERE a.id_ujian_launch = ".$row['id_ujian_launch']." AND a.id_siswa=s.id GROUP BY a.id_siswa),0)'Status".$row['nama_mapel']."' ,";
				
			}
			$sql ="
				SELECT 
				s.nama,s.id,
				CONCAT(master_sekolah.sekolah,' ',kelas,' ',rombel) AS kelas,$idujiansiswa $nilai $statusnilai
				s.jk 'L/P'
					FROM master_santri s
				left join master_rombel_siswa on master_rombel_siswa.id_santri = s.id
				left join master_rombel on master_rombel.id = master_rombel_siswa.id_rombel
				left join master_kelas on master_kelas.id = master_rombel.id_kelas
				left join master_sekolah on master_sekolah.id = master_kelas.id_sekolah
				left join master_ajaran on master_ajaran.id = master_rombel.tahun_ajaran
				WHERE master_rombel.id = $id_rombel
				ORDER BY master_sekolah.sekolah ASC, kelas ASC, rombel ASC, s.nama";
			$hasil = $this->db->query($sql);
			return $hasil->result_array();
}

	function daftar_semester($where=false)
	{
		$this->db->select('master_ajaran_semester.*');
		$this->db->select('master_ajaran.tahun');
		$this->db->join('master_ajaran','master_ajaran.id = master_ajaran_semester.id_ajaran');
		if($where)$this->db->where($where);
		$this->db->order_by('master_ajaran.tahun asc');
		return $this->db->get('master_ajaran_semester');
	}
	

}


