<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbguru extends CI_Model {

	function dataGuru($select=false,$where=false,$order_by=false,$group=false)
	{
		$this->db->select('el_mapel_guru.*,master_pegawai.*');
		if($select)$this->db->select($select);
		$this->db->join('master_pegawai','el_mapel_guru.id_guru = master_pegawai.id','inner');
		if($where)$this->db->where($where);
		if($order_by)$this->db->order_by($order_by);
		if($group)$this->db->group_by($group);
		return $this->db->get('el_mapel_guru');
	}

	function findMapelGuru($where=false)
	{
		$this->db->select('el_mapel_guru.*,el_mapel.nama_mapel');
		$this->db->join('el_mapel','el_mapel.id_mapel = el_mapel_guru.id_mapel');
		if($where)$this->db->where($where);
		return $this->db->get('el_mapel_guru');
	}

	function findRombelGuru($where=false)
	{
		$this->db->select('el_rombel_guru.*,master_rombel.*,master_sekolah.sekolah, master_kelas.kelas');
		$this->db->join('master_rombel','master_rombel.id = el_rombel_guru.id_rombel');
		$this->db->join('master_kelas','master_rombel.id_kelas = master_kelas.id');
		$this->db->join('master_sekolah','master_kelas.id_sekolah = master_sekolah.id');
		if($where)$this->db->where($where);
		return $this->db->get('el_rombel_guru');
	}


}

/* End of file Dbguru.php */
/* Location: ./application/models/Dbguru.php */