<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbujian extends CI_Model {

	function dataUjian($where=false,$order_by=false)
	{
		$this->db->select('el_ujian_launch.waktu_dari,el_ujian_launch.waktu_sampai,el_ujian_launch.id_ujian_launch,el_ujian_launch.status_random');
		$this->db->select('master_rombel.*,master_kelas.*,master_sekolah.sekolah');
		$this->db->select('el_ujian.*,master_pegawai.nama');
		$this->db->select('el_mapel.nama_mapel');
		$this->db->join('el_ujian','el_ujian.id_ujian = el_ujian_launch.id_ujian');
		$this->db->join('master_rombel','master_rombel.id = el_ujian_launch.id_rombel');
		$this->db->join('master_pegawai','master_pegawai.id = el_ujian_launch.id_guru');
		$this->db->join('el_mapel','el_mapel.id_mapel = el_ujian.id_mapel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->join('master_sekolah','master_sekolah.id = master_kelas.id_sekolah');
		if($where)$this->db->where($where);
		if($order_by)$this->db->order_by($order_by);
		return $this->db->get('el_ujian_launch');
	}

	function findGuruByMapel($where=false)
	{
		$this->db->select('master_pegawai.*,el_mapel.*');
		$this->db->join('master_pegawai','master_pegawai.id = el_mapel_guru.id_guru');
		$this->db->join('el_mapel','el_mapel.id_mapel = el_mapel_guru.id_mapel');
		if($where)$this->db->where($where);
		$this->db->order_by('master_pegawai.nama asc');
		return $this->db->get('el_mapel_guru');
	}

	function findRombel($where=false)
	{
		$this->db->select('master_rombel.*, master_sekolah.sekolah,master_kelas.kelas');
		$this->db->select('el_rombel_guru.*');
		$this->db->join('master_rombel','master_rombel.id = el_rombel_guru.id_rombel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->join('master_sekolah','master_sekolah.id = master_kelas.id_sekolah');
		if($where)$this->db->where($where);
		$this->db->order_by('master_sekolah.sekolah asc');
		return $this->db->get('el_rombel_guru');
	}

	function findSantriByRombel($where=false)
	{
		$this->db->select('master_rombel_siswa.*,master_santri.id,master_santri.nama,master_santri.induk,master_santri.password');
		$this->db->join('master_santri','master_santri.id = master_rombel_siswa.id_santri');
		if($where)$this->db->where($where);
		$this->db->order_by('master_santri.nama asc');
		return $this->db->get('master_rombel_siswa');
	}

	function findSantriByUjian($where)
	{
		$tahun = $this->dbsystem->getData('*','master_ajaran',array('status' => 'Y'))->row()->id;
		$this->db->select('master_santri.*,master_rombel_siswa.*,master_rombel.*,el_ujian_siswa.*,master_kelas.*');
		$this->db->join('master_santri','master_santri.id = el_ujian_siswa.id_siswa');
		$this->db->join('master_rombel_siswa','master_santri.id = master_rombel_siswa.id_santri');
		$this->db->join('master_rombel','master_rombel.id = master_rombel_siswa.id_rombel');
		$this->db->join('master_kelas','master_kelas.id = master_rombel.id_kelas');
		$this->db->where(array('master_rombel.tahun_ajaran' => $tahun));
		if($where)$this->db->where($where);
		return $this->db->get('el_ujian_siswa');

	}

	function daftar_semester($where=false)
	{
		$this->db->select('master_ajaran_semester.*');
		$this->db->select('master_ajaran.tahun');
		$this->db->join('master_ajaran','master_ajaran.id = master_ajaran_semester.id_ajaran');
		if($where)$this->db->where($where);
		$this->db->order_by('master_ajaran.tahun asc');
		return $this->db->get('master_ajaran_semester');
	}
	

}

/* End of file Dbujian.php */
/* Location: ./application/models/Dbujian.php */
