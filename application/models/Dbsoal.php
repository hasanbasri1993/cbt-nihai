<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbsoal extends CI_Model {

	function dataSoal($select=false,$where=false,$order=false)
	{
		$this->db->select('el_ujian.id_ujian, el_ujian.nama_ujian, el_ujian.tipe_ujian, el_ujian.kategori_ujian, el_ujian.waktu');
		$this->db->select('el_mapel.*');
		$this->db->select('master_pegawai.nip, master_pegawai.nama');	
		if($select)$this->db->select($select);
		$this->db->join('el_mapel','el_mapel.id_mapel = el_ujian.id_mapel','inner');
		$this->db->join('master_pegawai','master_pegawai.id = el_ujian.id_guru','left');
		if($where)$this->db->where($where);
		if($order)$this->db->order_by($order);
		return $this->db->get('el_ujian');
		
	}

	function MapelGuru($select=false,$where=false,$order=false)
	{
		if($select)$this->db->select($select);
		$this->db->select('el_mapel.nama_mapel, el_mapel_guru.*');
		if($where)$this->db->where($where);
		$this->db->join('el_mapel','el_mapel.id_mapel = el_mapel_guru.id_mapel');
		return $this->db->get('el_mapel_guru');
	}

}

/* End of file Dbujian.php */
/* Location: ./application/models/Dbujian.php */