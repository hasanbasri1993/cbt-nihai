<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbsantri extends CI_Model {

	function listSantri($where=false)
	{
		$this->db->select('master_sekolah.sekolah, master_kelas.kelas, master_rombel_siswa.*,master_rombel.rombel , master_santri.id,master_santri.nama,master_santri.foto,master_santri.induk,master_santri.nama,master_santri.password');
		$this->db->join('master_santri','master_santri.id = master_rombel_siswa.id_santri');
		$this->db->join('master_rombel','master_rombel.id = master_rombel_siswa.id_rombel');
		$this->db->join('master_kelas','master_rombel.id_kelas = master_kelas.id');
		$this->db->join('master_ajaran','master_ajaran.id = master_rombel.tahun_ajaran');
		$this->db->join('master_sekolah','master_kelas.id_sekolah = master_sekolah.id');
		if($where)$this->db->where($where);
		return $this->db->get('master_rombel_siswa');
	}	


	function listUjian($where=false)
	{
		$this->db->select('el_ujian_siswa.*,el_ujian_launch.*');
		$this->db->select('el_ujian.*');
		$this->db->select('el_mapel.*');
		$this->db->join('el_ujian_launch','el_ujian_launch.id_ujian_launch = el_ujian_siswa.id_ujian_launch');
		$this->db->join('el_ujian','el_ujian.id_ujian = el_ujian_launch.id_ujian');
		$this->db->join('el_mapel','el_ujian.id_mapel = el_mapel.id_mapel');
		if($where)$this->db->where($where);
		return $this->db->get('el_ujian_siswa');
	}



}

/* End of file Dbsantri.php */
/* Location: ./application/models/Dbsantri.php */
