<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Admin E-Learning
			</h3>
            
            <a data-toggle="modal" href='#modal-id' class="pull-right btn-primary" style="margin:0px;">
                <h3 style=""><i class="icon-plus"></i> Klik disini untuk tambah</h3>
            </a>
            
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
        	 <table class="table table-stripped table-hover table-responsive">
                <thead>
                    <tr class="bg-primary">
                        <td style="text-align:center;">No</td>
                        <td style="text-align:center;">Username</td>
                        <td style="text-align:center;">Akses</td>
                        <td style="text-align:center;">Action</td>
                    </tr>
                </thead>   
                <tbody>
                <?php $no=0;foreach ($list as $l) {$no++; ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no; ?></td>
                        <td style="text-align:center;"><?php echo $l->username; ?></td>
                        <td style="text-align:center;"><?php echo $l->akses; ?></td>
                        <td style="text-align:center;">

                            <a class="label-success label" data-toggle="modal" href='#modal-edit<?php echo $l->id_user; ?>'>
                                <i class="icon-edit"></i> Edit
                            </a>
                            
                            <a class="label-danger label" style="margin-left:10px;" data-toggle="modal" href='#modal-hapus<?php echo $l->id_user; ?>'>
                                <i class="icon-remove"></i> Hapus
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
             </table>
        </div>
    </div>
</div>


<?php foreach ($list as $l) { ?>
       
     <div id="modal-edit<?php echo $l->id_user; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-edit"></i> Edit Data Admin</h3>
      </div>

      <div class="modal-body">
        <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_admin','edit_admin',array('id' => $cf_semester->id,'id2' => $l->id_user)); ?>">

        <table class="table table-responsive">
            <tr>
                <td>USERNAME</td>
                <td>   
                    <input class="form-control" name="username" type="text" value="<?php echo $l->username; ?>" required>
                </td>
            </tr>

            <tr>
                <td>PASSWORD</td>
                <td>
                    <input class="form-control" name="password" type="password" value="<?php echo $l->password; ?>" required>
                </td>
            </tr>
            
            <tr>
                <td>
                    
                </td>
                <td style="text-align:left;">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </td>
            </tr>
        </table>
        </form>
      </div>
    </div>



    <div id="modal-hapus<?php echo $l->id_user; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-remove"></i> Hapus Data Admin</h3>
      </div>

      <div class="modal-body">
        Apakah Anda Yakin ingin Menghapus Admin ini ??
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
            Tidak
        </button>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_admin','hapus_admin',array('id' => $cf_semester->id,'id2' => $l->id_user)); ?>">
            Ya
        </a>
    </div>
    </div>


<?php } ?>



<div id="modal-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Penambahan Data Admin</h3>
  </div>
  <div class="modal-body">
    <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_admin','tambah_admin',array('id' => $cf_semester->id)); ?>">

        <table class="table table-responsive">

            <tr>
                <td>USERNAME</td>
                <td><input class="form-control" name="username" type="text" required></td>
            </tr>

            <tr>
                <td>PASSWORD</td>
                <td><input class="form-control" name="password" type="password" required></td>
            </tr>

            <tr>
                <td>
                    
                </td>
                <td style="text-align:left;">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </td>
            </tr>
        </table>
    </form>
  </div>
</div>

