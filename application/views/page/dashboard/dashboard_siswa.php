<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-dashboard"></i>
            <h3>
            	Dashboard Siswa
			</h3>
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
        	 
            <h3 class="text-center" style="font-family: Times New Roman;text-decoration: underline;">Selamat Datang di Sistem E-Learning !</h3>

            <style type="text/css">
                table.kiki td {
                    padding:10px 30px;
                }
            </style>
            <br>
            <div style="width:50%;float:left;">
                <table class="kiki">
                    <tr>
                        <td>
                            <b>No Induk</b>
                        </td>
                        <td>
                            <?php echo $siswa->induk; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Nama Siswa</b>
                        </td>
                        <td>
                            <?php echo $siswa->nama; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Kelas</b>
                        </td>
                        <td>
                            <?php echo $siswa->kelas."-".$siswa->rombel; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Sekolah</b>
                        </td>
                        <td>
                            <?php echo $siswa->sekolah; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:50%;float:left;text-align:right;">
                <?php if($siswa->foto != ""){ ?>
                    <img src="<?php echo base_url();?>asset/fotosantri/<?php echo $siswa->foto; ?>" style="width:50%;">
                <?php }else{ ?>
                    <i>Tidak ada foto</i>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

