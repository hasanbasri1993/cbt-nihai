<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-dashboard"></i>
            <h3>
            	Dashboard Guru
			</h3>
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
        	 
            <h2 class="text-center" style=""> Halo , <?php echo $_SESSION['username']; ?> !</h2>
            <p>
                Akun anda terdaftar sebagai GURU pada sistem E-Learning, dan mempunyai akses untuk mata pelajaran :
                <ul >
                <?php $no=0;foreach ($mapel as $m) {$no++; ?>
                    <li><?php echo $no.". ".$m->nama_mapel; ?></li>
                <?php } ?>
                </ul>
            </p>

            <p>
                Dan Akun anda mempunyai akses untuk memberikan ujian pada Kelas :
                <ul>
                <?php $no=0;foreach ($rombel as $r) {$no++; ?>
                    <li><?php echo $no.". ".$r->sekolah." (".$r->kelas."-".$r->rombel.")"; ?></li>
                <?php } ?>
                </ul>
            </p>
        </div>
    </div>
</div>

