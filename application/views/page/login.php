<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Login - E-Learning</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="E-Learning | Yuka Indonesia Media | Pesantren Cyber | Sekolah Pintar | Digital School | Daarul ulum lido | Daarululum" />
    <meta name="description" content="Aplikasi E-Learning Berbasis web dinamis, bertujuan untuk mempermudah aktivitas antara guru dan siswa.">
    <meta name="author" content="muhammadrejeki29@gmail.com">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    

	<link href="<?php echo base_url(); ?>asset/template/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>asset/template/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>asset/template/css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/template/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/template/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				E-Learning			
			</a>		
			
			<div class="nav-collapse">
				<!-- <ul class="nav pull-right">
					
					<li class="">						
						<a href="signup.html" class="">
							Don't have an account?
						</a>
						
					</li>
					
					<li class="">						
						<a href="index.html" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul> -->
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('login','action'); ?>" method="post">
		
			<h1 class="text-center" style="text-align:center;">E-Learning</h1>		
			
			<div class="login-fields">
				
				<p>Silahkan masukan username dan password.</p>
				
				<div class="field">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" required />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" required />
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Biarkan saya tetap masuk.</label>
				</span>
									
				<button class="button btn btn-success btn-large">Masuk</button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<?php if($this->session->flashdata('gagal') != ""){?>
<div class="account-container " style="background-color:#ba3434;margin-top:10px;">
		<div class="alert alert-danger" style="margin-left:5px;margin-bottom:0px;margin-right:5px;">
		<?php echo $this->session->flashdata('gagal'); ?>
		</div>
</div>
<?php } ?>

<script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url(); ?>asset/template/js/bootstrap.js"></script>

<script src="<?php echo base_url(); ?>asset/template/js/signin.js"></script>

</body>

</html>
