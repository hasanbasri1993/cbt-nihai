<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Ujian Kategori <?php echo ucfirst($kategori); ?>
              
			     </h3>
           <span class="pull-right">
              Tipe Ujian : <?php echo $ujian->tipe_ujian; ?>
           </span>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>


            <div>
              <a  class="label label-warning"  href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','showing',array('id' => $cf_semester->id,'id2' => $kategori,'id3' => $ujian->id_sekolah, 'id4' => $ujian->id_mapel)); ?>" >
                <i class="icon-hand-left"></i> Kembali  
              </a>
               Data Ujian Siswa Kelas <?php echo $ujian->kelas."-".$ujian->rombel; ?> Ujian <?php echo $ujian->nama_ujian; ?>.
              <a class="pull-right label label-info" target="_blank" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','list_siswa_print',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $id_ujian_launch)); ?>">
                <i class="icon-print"></i> Print
              </a>
            </div>

              <style type="text/css">
                .table tr td{
                  text-align:center;
                }
              </style>
            <table class="table table-responsive text-center text-stripped text-bordered">
              <tr class="bg-primary">
                <td>No</td>
                <td>Nama Siswa</td>
                <td>Kelas</td>
                <td>Nilai</td>
                <td>status ujian</td>
                <td></td>
              </tr>
              <?php $no=0;foreach ($ujian_siswa as $l) {$no++; ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $l->nama; ?></td>
                  <td><?php echo $l->kelas."-".$l->rombel; ?></td>
                  <td>
                    <?php if($l->status_ujian == "sudah"){ ?>
                      <?php echo $l->nilai_ujian; ?>
                    <?php }else if($l->status_ujian == "belum"){ ?>
                      -
                    <?php }else if($l->status_ujian == "tertunda"){ ?>
                      -
                    <?php } ?>
                  </td>
                  <td>
                    <?php if($l->status_ujian == "sudah"){ ?>
                      <span class="label-success label"> Selesai </span>
                    <?php }else if($l->status_ujian == "belum"){ ?>
                      <span class="label-default label"> Belum </span>
                    <?php }else if($l->status_ujian == "tertunda"){ ?>
                      <span class="label-warning label"> Tertunda </span>
                    <?php } ?>
                  </td>
                  <td>
                  <?php if($l->status_ujian == "sudah"){ ?>
                    <!-- <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa',array('id' => $cf_semester->id,'id2' => $kategori)); ?>" class="label label-info">
                      <i class="icon-file-o"></i> Lihat Nilai
                    </a> -->

                    <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa_reset',array('id' => $cf_semester->id,'id2' => $kategori,'id3' => $l->id_ujian_launch, 'id4' => $l->id_ujian_siswa,'id5' => $l->id_siswa,'id6' => $ujian->tipe_ujian)); ?>" class="label label-info" onclick="return confirm('Reset Nilai siswa ini ??')">
                      <i class="icon-cog"></i> Reset
                    </a>
                  <?php } ?>
                  </td>
                </tr>
              <?php } ?>
            </table>

        </div>
    </div>
</div>
