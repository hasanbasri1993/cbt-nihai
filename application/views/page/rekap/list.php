<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Ujian Kategori <?php echo ucfirst($kategori); ?> 
			</h3>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            <style type="text/css">
              .table tr td{
                text-align:center;
              }
            </style>
            <table class="table table-responsive text-center text-stripped text-bordered">
              <tr class="bg-primary">
                <td>No</td>
                <td>Nama Ujian</td>
                <td>Nama Mata Pelajaran</td>
                <td>Kelas</td>
                <td>Mulai</td>
                <td>Sampai</td>
                <td></td>
              </tr>
              <?php $no=0;foreach ($list as $l) {$no++; ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $l->nama_ujian; ?></td>
                  <td><?php echo $l->nama_mapel; ?></td>
                  <td><?php echo $l->sekolah." ".$l->kelas."-".$l->rombel; ?></td>
                  <td><?php echo date('d-M-Y H:i:s',strtotime($l->waktu_dari)); ?></td>
                  <td><?php echo date('d-M-Y H:i:s',strtotime($l->waktu_sampai)); ?></td>
                  <td>

                    <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa',array('id' => $cf_semester->id,'id2' => $kategori,'id3' => $l->id_ujian_launch)); ?>" class="label label-info">
                      <i class="icon-eye-open"></i> Detail
                    </a>
                  </td>
                </tr>
              <?php } ?>
            </table>

        </div>
    </div>
</div>
