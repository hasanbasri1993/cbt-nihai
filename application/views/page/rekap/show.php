<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Penambahan Ujian <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','show_action',array('id_semester' => $cf_semester->id,'id2' => $kategori)); ?>">
              
              <table class="table">
                  <tr>
                      <td>Pilih Semester</td>
                      <td>
                          <select name="id_semester">
                            <?php foreach ($semester as $s) { ?>
                              <option value="<?php echo $s->id; ?>" >
                                  <?php echo $s->tahun.' '.$s->semester; ?>
                              </option>
                            <?php } ?>
                          </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Pilih Sekolah</td>
                      <td>
                          <select name="id_sekolah">
                            <?php foreach ($sekolah as $s) { ?>
                              <option value="<?php echo $s->id; ?>" >
                                  <?php echo $s->sekolah; ?>
                              </option>
                            <?php } ?>
                          </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Pilih Mata Pelajaran</td>
                      <td>
                          <select name="id_mapel">
                            <?php foreach ($mapel as $m) { ?>
                              <option value="<?php echo $m->id_mapel; ?>" >
                                  <?php echo $m->nama_mapel; ?>
                              </option>
                            <?php } ?>
                          </select>
                      </td>
                  </tr>
                  <tr>
                      <td></td>
                      <td>
                          <button class="btn-default btn" type="submit">
                              <i class="icon-search"></i> Pencarian
                          </button>
                      </td>
                  </tr>
              </table>


              
            </form>

        </div>
    </div>
</div>
