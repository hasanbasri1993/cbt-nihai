<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <title>E-Learning (Pesantren Daarululum)</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="E-Learning | Yuka Indonesia Media | Pesantren Cyber | Sekolah Pintar | Digital School | Daarul ulum lido | Daarululum" />
  <meta name="description" content="Aplikasi E-Learning Berbasis web dinamis, bertujuan untuk mempermudah aktivitas antara guru dan siswa.">
  <meta name="author" content="muhammadrejeki29@gmail.com">
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <meta name="apple-mobile-web-app-capable" content="yes"> 


  <link href="<?php echo base_url(); ?>asset/template/css/bootstrap.min.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/bootstrap-responsive.min.css" rel="stylesheet"> -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
          rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/font-awesome.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/style.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/pages/dashboard.css" rel="stylesheet"> -->
  <!-- <script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script>  -->
  <style type="text/css">
    .bg-primary{
      background-color: #00ba8b;
      color:white;
    }
    .text-center{
      text-align:center;
    }
  </style>

</head>
<body>
<!-- Data Ujian Siswa Kelas <?php echo $ujian->kelas."-".$ujian->rombel; ?> Ujian <?php echo $ujian->nama_ujian; ?>. -->
<div class="container" style="margin-top:60px;">
            <style type="text/css">
              .table tr td{
                text-align:center;
              }
              .tb-kiki{
                width:60%;
                border-color:#00ba8b;
              }
              .tb-kiki tr td{
                padding:3px;
                border-color:#00ba8b;
              }
              .tb-kiki  tr{
                border-color:#00ba8b;
              }
              .macan tr td{
                text-align:center;
              }
            </style>

            <br>
            <style>
							.table-bordered th{
								    background: #00ba8b;
							}
						</style>
            <table data-show-print="true" class="table table-responsive text-stripped table-bordered text-bordered">
							<thead class="bg-primary">
								<tr>
									<th> <p class="text-center"> No </p></th>
									<th> <p class="text-center"> Nama Siswa </p></th>
									<th> <p class="text-center"> Kelas </p></th>
									 <?php $no=0;foreach ($ujian_siswa_mapel as $k) {$no++; ?>
									<th> <p class="text-center"> <?php echo $k['nama_mapel']; ?> </p></th>
									<?php } ?>
								</tr>
							</thead>
              <?php $no=0;foreach ($ujian_siswa as $l) {$no++; ?>
                <tr>
									<td> <p class="text-center"> <?php echo $no; ?> </p></td>
                  <td> <p class="text-left">
										<?php echo $l['nama']; ?>
										</p></td>
                  <td><?php echo $l['kelas']; ?></td>
									
									<?php foreach ($ujian_siswa_mapel as $k) { ?>
									<td> <p class="text-center">
											<?php if($l['Status'.$k['nama_mapel']] == "sudah"){ ?>
                      
										<a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa_reset_allpelajaran',array('id' => $id_semester,'id1' => $id_rombel, 'id2' => $l['idujiansiswa'.$k['nama_mapel']] ,'id3' => $l['id'])); ?>" class="label label-info" onclick="return confirm('Reset Nilai siswa ini ??')">
                     <?php echo $l[$k['nama_mapel']]; ?>
                    </a>
												<?php }else if($l['Status'.$k['nama_mapel']] == "belum"){ ?>
													<span class="label-default label"> Belum </span>
												<?php }else if($l['Status'.$k['nama_mapel']] == "tertunda"){ ?>
													<span class="label-warning label"> Tertunda </span>
												<?php } ?>
										</p>
										
										
										
									</td>
									<?php } ?>
                </tr>
              <?php } ?>
            </table>
</div>

<script type="text/javascript">
  window.print();
</script>


<!-- <script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/excanvas.min.js"></script>  -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/chart.min.js" type="text/javascript"></script>  -->
<script src="<?php echo base_url(); ?>asset/template/js/bootstrap.js"></script>
<!-- <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>asset/template/js/full-calendar/fullcalendar.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/base.js"></script>  -->
 

<!-- inicountdown -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>/asset/countdown/jquery.countdownTimer.js"></script> -->

</body>
</html>
