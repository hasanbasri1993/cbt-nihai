<link href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="row all-icons" style="margin:0px 0px;">
	<div class="widget">
		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>
				Daftar Nilai Ujian
			</h3>
		</div>
		<div class="widget-content">
			<?php if($this->session->flashdata('gagal') != ""){?>
			<div style="background-color:red;border-radius:5px;">
				<div class="alert alert-danger" style="margin-left:5px;">
					<?php echo $this->session->flashdata('gagal'); ?>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->flashdata('berhasil') != ""){?>
			<div style="background-color:green;border-radius:5px;">
				<div class="alert alert-success" style="margin-left:5px;">
					<?php echo $this->session->flashdata('berhasil'); ?>
				</div>
			</div>
			<?php } ?>

			<div class="btn btn-sm btn-info btn-sm form-control" >
				<a style="color:white;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' =>'nihai')); ?>">
                <i class="icon-hand-left"></i> Kembali  
              </a>
			</div>
			<div class="form-control">
				<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa_allpelajaran'); ?>">
					<select onchange="this.form.submit()" id="pilihrombel" name="id_rombel"></select>
					<input type="hidden" name="id_semester" value="<?php echo $id_semester; ?>"/>
				</form>
			</div>
			<style>
				.table-bordered th {
					background: #00ba8b;
				}
			</style>
			<table id="rekap" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>
							<p class="text-center"> No </p>
						</th>
						<th>
							<p class="text-center"> Nama Siswa </p>
						</th>
						<th>
							<p class="text-center"> Kelas </p>
						</th>
						<?php $no=0;foreach ($ujian_siswa_mapel as $k) {$no++; ?>
						<th>
							<p class="text-center">
								<?php echo $k['nama_mapel']; ?> </p>
						</th>
						<?php } ?>
					</tr>
				</thead>
				<?php $no=0;foreach ($ujian_siswa as $l) {$no++; ?>
				<tr>
					<td>
						<p class="text-center">
							<?php echo $no; ?> </p>
					</td>
					<td>
						<p class="text-left">
							<?php echo $l['nama']; ?>
						</p>
					</td>
					<td>
						<?php echo $l['kelas']; ?>
					</td>

					<?php foreach ($ujian_siswa_mapel as $k) { ?>
					<td>
						<p class="text-center">
							<?php if($l['Status'.$k['nama_mapel']] == "sudah"){ ?>

							<a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa_reset_allpelajaran',array('id' => $id_semester,'id1' => $id_rombel, 'id2' => $l['idujiansiswa'.$k['nama_mapel']] ,'id3' => $l['id'])); ?>" class="label label-info"
							 onclick="return confirm('Reset Nilai siswa ini ??')">
								<?php echo $l[$k['nama_mapel']]; ?>
							</a>
							<?php }else if($l['Status'.$k['nama_mapel']] == "belum"){ ?>
							<span class="label-default label"> Belum </span>
							<?php }else if($l['Status'.$k['nama_mapel']] == "tertunda"){ ?>
							<span class="label-warning label"> Tertunda </span>
							<?php } ?>
						</p>



					</td>
					<?php } ?>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
		
      $("#pilihrombel > option").remove();
      var opt1 = $('<option />');
       
      opt1.text('Pilih Rombel');
      $('#pilihrombel').append(opt1);
          $.post("<?php echo base_url() ?>index.php/rekap/pilihrombel",{idnya:<?php echo $id_semester; ?>}).done(function(data){
               {
                 var obj = JSON.parse(data);
                     $.each(obj, function(k, v) {
                       var opt = $('<option />');
                       opt.val(v.idrombel);
                       opt.text(v.rombel);
                       $('#pilihrombel').append(opt);
                     });
               }
    });
		
		$('#rekap').DataTable({
			dom: 'Bfrtip',
			paging: false,
			scrollX: true,
			buttons: [{
					extend: 'copyHtml5',
					text: '<i class="fa fa-files-o"></i> Copy',
					titleAttr: 'Copy'
				},
				{
					extend: 'excelHtml5',
					text: '<i class="fa fa-file-excel-o"></i> Excel',
					titleAttr: 'Excel'
				},
				{
					extend: 'csvHtml5',
					text: '<i class="fa fa-file-text-o"></i> CVS',
					titleAttr: 'CSV'
				},
				{
					extend: 'pdfHtml5',
					text: '<i class="fa fa-file-pdf-o"></i> PDF',
					titleAttr: 'PDF'
				},
				{
					extend: 'print',
					text: '<i class="fa fa fa-print"></i> Print',
					titleAttr: 'PDF'
				}

			]
		});
	});
</script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>