<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <title>E-Learning (Pesantren Daarululum)</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="E-Learning | Yuka Indonesia Media | Pesantren Cyber | Sekolah Pintar | Digital School | Daarul ulum lido | Daarululum" />
  <meta name="description" content="Aplikasi E-Learning Berbasis web dinamis, bertujuan untuk mempermudah aktivitas antara guru dan siswa.">
  <meta name="author" content="muhammadrejeki29@gmail.com">
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <meta name="apple-mobile-web-app-capable" content="yes"> 


  <link href="<?php echo base_url(); ?>asset/template/css/bootstrap.min.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/bootstrap-responsive.min.css" rel="stylesheet"> -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
          rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/font-awesome.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/style.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo base_url(); ?>asset/template/css/pages/dashboard.css" rel="stylesheet"> -->
  <!-- <script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script>  -->
  <style type="text/css">
    .bg-primary{
      background-color: #00ba8b;
      color:white;
    }
    .text-center{
      text-align:center;
    }
  </style>

</head>
<body>
<!-- Data Ujian Siswa Kelas <?php echo $ujian->kelas."-".$ujian->rombel; ?> Ujian <?php echo $ujian->nama_ujian; ?>. -->
<div class="container" style="margin-top:60px;">
            <style type="text/css">
              .table tr td{
                text-align:center;
              }
              .tb-kiki{
                width:60%;
                border-color:#00ba8b;
              }
              .tb-kiki tr td{
                padding:3px;
                border-color:#00ba8b;
              }
              .tb-kiki  tr{
                border-color:#00ba8b;
              }
              .macan tr td{
                text-align:center;
              }
            </style>
            <table border="0px" class="tb-kiki ">
              <tr>
                <td>Nama Ujian</td>
                <td>: <?php echo $ujian->nama_ujian; ?></td>
              </tr>
              <tr>
                <td>Tipe Ujian</td>
                <td>: <?php echo $ujian->tipe_ujian; ?></td>
              </tr>
              <tr>
                <td>Kategori Ujian</td>
                <td>: <?php echo $ujian->kategori_ujian; ?></td>
              </tr>
              <tr>
                <td>Mata Pelajaran</td>
                <td>: <?php echo $ujian->nama_mapel; ?></td>
              </tr>
              <tr>
                <td>Tanggal Ujian</td>
                <td>: Dari <?php echo date('d-m-Y',strtotime($ujian->waktu_dari)).' sampai '.date('d-m-Y',strtotime($ujian->waktu_sampai)); ?></td>
              </tr>
              <tr>
                <td>Tahun Ajaran</td>
                <td>: <?php echo $ujian->tahun; ?> Semester <?php echo $ujian->semester; ?></td>
              </tr>
            </table>
            <br>
            <table class="tb-kiki macan" style="width:100%;" border="1px">
              <tr style="background-color: #00ba8b;color: white;">
                <td>No</td>
                <td>Nama Siswa</td>
                <td>Kelas</td>
                <td>Nilai</td>
                <!-- <td>status ujian</td> -->
                <!-- <td></td> -->
              </tr>
              <?php $no=0;foreach ($ujian_siswa as $l) {$no++; ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $l->nama; ?></td>
                  <td><?php echo $l->kelas."-".$l->rombel; ?></td>
                  <td>
                    <?php if($l->status_ujian == "sudah"){ ?>
                      <?php echo $l->nilai_ujian; ?>
                    <?php }else if($l->status_ujian == "belum"){ ?>
                      -
                    <?php }else if($l->status_ujian == "tertunda"){ ?>
                      -
                    <?php } ?>
                  </td>
                  
                </tr>
              <?php } ?>
            </table>
</div>

<script type="text/javascript">
  window.print();
</script>


<!-- <script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/excanvas.min.js"></script>  -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/chart.min.js" type="text/javascript"></script>  -->
<script src="<?php echo base_url(); ?>asset/template/js/bootstrap.js"></script>
<!-- <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>asset/template/js/full-calendar/fullcalendar.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/template/js/base.js"></script>  -->
 

<!-- inicountdown -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>/asset/countdown/jquery.countdownTimer.js"></script> -->

</body>
</html>
