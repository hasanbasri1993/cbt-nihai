<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Penambahan Ujian <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap','list_siswa_allpelajaran'); ?>">
                <table class="table">
                    <tr>
                        <td>Pilih Semester</td>
                        <td>
							<select id="pilihsemester" name="id_semester">
							<option value="">Pilih Semester</option>
                            <?php foreach ($semester as $s) { ?>
                            <option value="<?php echo $s->id; ?>" >
                            <?php echo $s->tahun.' '.$s->semester; ?>
                            </option>
                            <?php } ?>
                          </select>
                        </td>
                    </tr>
					<tr id="divrombel" style="display:none;">
                        <td>Pilih Rombel</td>
                        <td>
                            <select  onchange="this.form.submit()"  id="pilihrombel" name="id_rombel"></select>
						</td>
					</tr>
                </table>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $('#pilihsemester').change(function(){
      $("#divrombel").show();
      $("#pilihrombel > option").remove();
      var opt1 = $('<option />');
      opt1.val('');
      opt1.text('Pilih Rombel');
      $('#pilihrombel').append(opt1);
      var country_id = $('#pilihsemester').val();
          $.post("<?php echo base_url() ?>index.php/rekap/pilihrombel",{idnya:country_id}).done(function(data){
               {
                 var obj = JSON.parse(data);
                     $.each(obj, function(k, v) {
                       var opt = $('<option />');
                       opt.val(v.idrombel);
                       opt.text(v.rombel);
                       $('#pilihrombel').append(opt);
                     });


               }
            });
    });


});
</script>