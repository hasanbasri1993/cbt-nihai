<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Cannot Follow Exam <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <div style="background-color:red;border-radius:5px;">
                <div class="alert alert-danger" style="margin-left:5px;">
                    Maaf Anda sudah terlalu banyak melakukan pengulangan dalam ujian ini, Hubungi Pihak Administrator !
                </div>
            </div>

        </div>
    </div>
</div>