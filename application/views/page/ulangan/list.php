<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Ujian <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            <style type="text/css">
                table.table td{
                    text-align:center;
                }
            </style>

            <p>Daftar Ujian Anda !</p>
            <table class="table table-responsive table-striped text-center">
                <thead>
                    <tr class="bg-primary">
                        <td>No</td>
                        <td>Soal Ujian</td>
                        <td>Mata Pelajaran</td>
                        <td>Tipe Ujian</td>
                        <td>Waktu Pengerjaan</td>
                        <td>Batas Terakhir Ujian</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=0;foreach ($list as $l) {$no++; ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $l->nama_ujian; ?></td>
                        <td><?php echo $l->nama_mapel; ?></td>
                        <td><?php echo $l->tipe_ujian; ?></td>
                        <td><?php echo $l->waktu; ?> menit</td>
                        <td><?php echo date('d-M-Y H:i:s',strtotime($l->waktu_sampai)); ?></td>
                        <td>
                            <a class="label-success label" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','mulai',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $l->id_ujian_siswa)); ?>" onclick="return confirm('Apakah anda yakin ingin memulai ujian ini ??')">
                                <i class="icon-start"></i> Mulai Ujian !
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>


        </div>
    </div>
</div>