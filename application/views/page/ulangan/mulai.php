
<style>
@font-face {
  font-family: 'Lateef';
  font-style: normal;
  font-weight: 400;
  src: url(<?php echo base_url();?>asset/template/fonts/LateefRegOT.ttf);
  unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
}
	.text-right, #pg1, #pg2, #pg3, #pg4, #pg5{
		font-family: 'Lateef',cursive;
		font-size:32px;
	}
</style>
                         
<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
                Ujian <?php echo ucfirst($kategori); ?>
                <b>
                    TIMER ( <span id="ms_timer"></span> )

                    <script>

                        $(function(){
                            $('#ms_timer').countdowntimer({
                                minutes : <?php if($timer == "0" or $timer == "00"){echo "01";}else{echo $timer;} ?>,
                                seconds : 00,
                                size : "lg"
                            });
                        });
                        var intervalnya = setInterval(function(){ 
                            if($("#ms_timer").html() == "00:00"){
                                window.location.href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','proses_nilai',array('id_semester' => $cf_semester->id, 'id2' => $kategori, 'id3' => $data->id_ujian_siswa,'id4' => $data->id_ujian)); ?>";
                            }
                         }, 1000);

                        function ff()
                        {
                            clearInterval(intervalnya);
                        }
                    </script>
                </b>
            </h3>
        </div>
        <div class="widget-content" style="padding:10px;">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <div class="page-header" style="padding:0px;margin-top:0px;">
                <table class="table table-responsive table-bordered" border="0px;">
                    <tr>
                        <td>
                            <table class="table">
                                <tr>
                                    <td style="border:0px;">Nama Siswa</td>
                                    <td style="border:0px;">: <?php echo $santri->nama; ?></td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">Kelas</td>
                                    <td style="border:0px;">: <?php echo $santri->kelas."-".$santri->rombel; ?></td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">Sekolah</td>
                                    <td style="border:0px;">: <?php echo $santri->sekolah; ?></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table class="table">
                                <tr>
                                    <td style="border:0px;">Nama Mata Pelajaran</td>
                                    <td style="border:0px;">: <?php echo $data->nama_mapel; ?></td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">Nama Soal Ujian</td>
                                    <td style="border:0px;">: <?php echo $data->nama_ujian; ?></td>
                                </tr>
                                <tr>
                                    <td style="border:0px;">Waktu</td>
                                    <td style="border:0px;">: <?php echo $data->waktu; ?> menit</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="background-color:#00ba8b !important;padding:10px;color:white;">
                Jika Anda sudah selesai mengerjakan soal, silahkan 
                <a data-toggle="modal" href='#modal-akhiri' class="label label-primary">
                    Klik Disini
                </a> 

                <div class="modal fade" id="modal-akhiri">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                            <div class="modal-body" style="color:black">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Apakah anda yakin ingin mengakhiri Ujian ini ????</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','proses_nilai',array('id_semester' => $cf_semester->id, 'id2' => $kategori, 'id3' => $data->id_ujian_siswa,'id4' => $data->id_ujian)); ?>" class="btn btn-primary">Ya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="" style="padding:0px;margin-top:10px;">
                <div style="min-height: 210px;vertical-align: middle;">

                <?php if($soal[0]['pertanyaan_gambar'] != ""){ ?>
                
                    <?php if($data->paragraph == "kiri"){ ?>
                    <h4 class='text-left'>
                        <div class="text-left" id="pertanyaan">
                        Pertanyaan No.1 <br><br>
                        1) <?php echo $soal[0]['pertanyaan']; ?>
                        </div>
                        <hr>
                        <span id="pertanyaan_gambar">
                        <a class="pull-right" data-toggle="modal" href='#modal-pertanyaan'>
                            <img src="<?php echo base_url().'file/soal/'.$soal[0]['pertanyaan_gambar']; ?>" style="width:80px">
                        </a>
                        <div class="modal fade" id="modal-pertanyaan">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                   
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pertanyaan_gambar']; ?>" style="width:100%">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        </span>
                    </h4>
                    


                    <?php }else if($data->paragraph == "kanan"){ ?>



                    <p class='text-right' style="width:100%;text-align:right">
                        <div id="pertanyaan">
                        Pertanyaan No.1 <br><br>
                        <?php echo $soal[0]['pertanyaan']; ?>
                        </div>
                        <hr>
                        <span id="pertanyaan_gambar">
                        <a class="pull-left" data-toggle="modal" href='#modal-pertanyaan'>
                            <img src="<?php echo base_url().'file/soal/'.$soal[0]['pertanyaan_gambar']; ?>" style="width:80px">
                        </a>
                        <div class="modal fade" id="modal-pertanyaan">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                   
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pertanyaan_gambar']; ?>" style="width:100%">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        </span>
                        
                    </p>
                    <?php } ?>
       
                <?php }else{ ?>

                    <?php if($data->paragraph == "kiri"){ ?>
                        <div class="text-left" id="pertanyaan">
                        Pertanyaan No.1 <br><br>
                        <?php echo $soal[0]['pertanyaan']; ?>
                        </div>
                        <hr>
                        <span class="pertanyaan_gambar">
                            
                        </span>
                    <?php }else if($data->paragraph == "kanan"){ ?>
                        <div dir="rtl" class="text-right" id="pertanyaan">
                        Pertanyaan No.1 <br><br>
                        <?php echo $soal[0]['pertanyaan']; ?>
                        </div>
                        <hr>
                        <span id="pertanyaan_gambar">

                        </span>
                    <?php } ?>

                <?php } ?>



                <!-- pilhan -->


                <?php if($data->paragraph=="kiri"){ ?>
                <p style="margin:0px 0px 0px 20px">
									<div class="radio">
                    <label id="pg1">
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg1'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg1','1')"
                        <?php if($soal[0]['jawaban'] == "pg1")echo "checked"; ?>
                        > 
                        <?php echo $soal[0]['pg1']; ?>
											
											
                    </label>
										 <span id="pg1_gambar">
                    <?php if($soal[0]['pg1_gambar'] != ""){ ?>
                    
                        <a class="label label-info" data-toggle="modal" href='#modal-pg1'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg1_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php } ?>
                    </span>
									</div>
                   
                </p>

                <p style="margin:0px 0px 0px 20px">
									<div class="radio">
                    <label id="pg2">
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg2'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg2','1')"
                        <?php if($soal[0]['jawaban'] == "pg2")echo "checked"; ?>
                        > 
                        <?php echo $soal[0]['pg2']; ?>
											
                    </label>
										 <span id="pg2_gambar">
                    <?php if($soal[0]['pg2_gambar'] != ""){ ?>
                    
                        <a class="label label-info" data-toggle="modal" href='#modal-pg2'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg2">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg2_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php } ?>
                    </span>
									</div>
                   
                </p>

                <p style="margin:0px 0px 0px 20px">
									<div class="radio">
                    <label id="pg3">
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg3'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg3','1')"
                        <?php if($soal[0]['jawaban'] == "pg3")echo "checked"; ?>
                        > 
                        <?php echo $soal[0]['pg3']; ?>
											</label>		
										<span id="pg3_gambar">
                    <?php if($soal[0]['pg3_gambar'] != ""){ ?>
                    
                        <a class="label label-info" data-toggle="modal" href='#modal-pg3'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg3">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg3_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
										</div>
                </p>

								<p style="margin:0px 0px 0px 20px">
									<div class="radio">
                    <label id="pg4">
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg4'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg4','1')"<?php if($soal[0]['jawaban'] == "pg4")echo "checked"; ?>> 
                        <?php echo $soal[0]['pg4']; ?>
                    </label>
										<span id="pg4_gambar">
                   		 <?php if($soal[0]['pg4_gambar'] != ""){ ?>
                    
                        <a class="label label-info" data-toggle="modal" href='#modal-pg4'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg4">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg4_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php } ?>
                    </span>
									</div>
                </p>
                <p style="margin:0px 0px 0px 20px">
									<div class="radio">
                    <label id="pg5">
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg5'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg5','1')"
                        <?php if($soal[0]['jawaban'] == "pg5")echo "checked"; ?>
                        > 
                        <?php echo $soal[0]['pg5']; ?>
                    </label>
										<span id="pg5_gambar">
                    <?php if($soal[0]['pg5_gambar'] != ""){ ?>
                    
                        <a class="label label-info" data-toggle="modal" href='#modal-pg5'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg5">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg5_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php } ?>
                    </span>
									</div>
                </p>


                <?php }else if($data->paragraph == "kanan"){ ?>
                <!-- batas -->
                <p style="margin:0px 20px 0px 0px;text-align:right;">
                    
                    <span id="pg1">
                        <?php echo $soal[0]['pg1']; ?>
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg1'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg1','1')"
                        <?php if($soal[0]['jawaban'] == "pg1")echo "checked"; ?>
                        >
                    </span>

                    <span id="pg1_gambar">
                    <?php if($soal[0]['pg1_gambar'] != ""){ ?>
                        <a class="label label-info" data-toggle="modal" href='#modal-pg1'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg1_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
                </p>

                <p style="margin:0px 20px 0px 0px;text-align:right;">
                    
                    <span id="pg2">
                        <?php echo $soal[0]['pg2']; ?>
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg2'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg2','1')"
                        <?php if($soal[0]['jawaban'] == "pg2")echo "checked"; ?>
                        >
                    </span>

                    <span id="pg2_gambar">
                    <?php if($soal[0]['pg2_gambar'] != ""){ ?>
                        <a class="label label-info" data-toggle="modal" href='#modal-pg2'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg2">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg2_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
                </p>

                <p style="margin:0px 20px 0px 0px;text-align:right;">
                    
                    <span id="pg3">
                        <?php echo $soal[0]['pg3']; ?>
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg3'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg3','1')"
                        <?php if($soal[0]['jawaban'] == "pg3")echo "checked"; ?>
                        >
                    </span>

                    <span id="pg3_gambar">
                    <?php if($soal[0]['pg3_gambar'] != ""){ ?>
                        <a class="label label-info" data-toggle="modal" href='#modal-pg3'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg3">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg3_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
                </p>

                <p style="margin:0px 20px 0px 0px;text-align:right;">
                    
                    <span id="pg4">
                        <?php echo $soal[0]['pg4']; ?>
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg4'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg4','1')"
                        <?php if($soal[0]['jawaban'] == "pg4")echo "checked"; ?>
                        >
                    </span>

                    <span id="pg4_gambar">
                    <?php if($soal[0]['pg4_gambar'] != ""){ ?>
                        <a class="label label-info" data-toggle="modal" href='#modal-pg4'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg4">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg4_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
                </p>

                <p style="margin:0px 20px 0px 0px;text-align:right;">
                    
                    <span id="pg5">
                        <?php echo $soal[0]['pg5']; ?>
                        <input type="radio" name="pilihan" value="<?php echo $soal[0]['pg5'] ?>" onclick="pilihan('<?php echo $data->id_ujian_siswa; ?>','<?php echo $soal[0]['id_pilihan_soal']; ?>','pg5','1')"
                        <?php if($soal[0]['jawaban'] == "pg5")echo "checked"; ?>
                        >
                    </span>

                    <span id="pg5_gambar">
                    <?php if($soal[0]['pg5_gambar'] != ""){ ?>
                        <a class="label label-info" data-toggle="modal" href='#modal-pg5'><i class="icon-picture"></i></a>
                        <div class="modal fade" id="modal-pg5">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <img src="<?php echo base_url().'file/soal/'.$soal[0]['pg5_gambar']; ?>" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </span>
                </p>

                

                <?php } ?>

                <br>
                <div id="buttonnya" style="text-align:center;">
                    <a class="btn btn-info" onclick="pindah('<?php echo $data->id_ujian_siswa; ?>','<?php echo $data->id_ujian; ?>','<?php echo $random; ?>','2','<?php echo $data->paragraph; ?>')">
                        <i class="icon-arrow-right"></i>
                    </a>
                </div>
                <br>





                </div>


                <style type="text/css">
                    .nomor:hover{
                        color:white;
                        cursor:pointer;
                    }
                </style>
                <div style="text-align:center;" >
                    <?php $no=0;foreach ($soal as $s) { $no++;?>
                        <span id="nomor<?php echo $no; ?>"  class="nomor" style="
                        <?php if($s['jawaban'] != '')echo 'color:red;text-decoration: underline;'; ?>"
                            onclick="pindah('<?php echo $data->id_ujian_siswa; ?>','<?php echo $data->id_ujian; ?>','<?php echo $random; ?>','<?php echo $no; ?>','<?php echo $data->paragraph; ?>')"
                            >
                            <?php echo $no; ?></span> | 
                    <?php } ?>
                </div>



                












            </div>

        </div>
    </div>
</div>



<script type="text/javascript">



    function pilihan(id_ujian_siswa,id_pilihan_soal,pilihan,nomor)
    {
        var lenkotime = $("#ms_timer").html().trim();
        lenkotime = lenkotime.length;
        if(lenkotime == 5){
            var time = Number($("#ms_timer").html().substr(0,2));
            var param = Number($("#ms_timer").html().substr(3,2));
            if(param >= 30){
                time = time + 1;
            }
        }
        else if(lenkotime == 6)
        {
            var time = Number($("#ms_timer").html().substr(0,3));
            var param = Number($("#ms_timer").html().substr(4,2));
            if(param >= 30){
                time = time + 1;
            }
        }
        else if(lenkotime == 7)
        {
            var time = Number($("#ms_timer").html().substr(0,4));
            var param = Number($("#ms_timer").html().substr(5,2));
            if(param >= 30){
                time = time + 1;
            }
        }
        
        
        $.post( "<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','ajax_memilih'); ?>", { xid_ujian_siswa: id_ujian_siswa, xid_pilihan_soal: id_pilihan_soal, xpilihan: pilihan, xnomor:nomor ,xtime : time }).done(function( data ) {
            var dd = data.trim();
            var ww = "#nomor" + dd.replace(" ","");
            $(ww).css({'color':'red','text-decoration':'underline'});
        });
    }

    function pindah(id_ujian_siswa,id_ujian,random,nomor,paragraph)
    {
        var lenkotime = $("#ms_timer").html().trim();
        lenkotime = lenkotime.length;
        if(lenkotime == 5){
            var time = Number($("#ms_timer").html().substr(0,2));
            var param = Number($("#ms_timer").html().substr(3,2));
            if(param >= 30){
                time = time + 1;
            }
        }
        else if(lenkotime == 6)
        {
            var time = Number($("#ms_timer").html().substr(0,3));
            var param = Number($("#ms_timer").html().substr(4,2));
            if(param >= 30){
                time = time + 1;
            }
        }
        else if(lenkotime == 7)
        {
            var time = Number($("#ms_timer").html().substr(0,4));
            var param = Number($("#ms_timer").html().substr(5,2));
            if(param >= 30){
                time = time + 1;
            }
        }


        $.post( "<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','ajax_pindah'); ?>", { xid_ujian_siswa: id_ujian_siswa, xid_ujian: id_ujian, xrandom: random, xnomor:nomor ,xtime : time, xnomor : nomor }).done(function( data ) {
            var list = JSON.parse(data);
            
            if(paragraph == "kanan")
            {
                var page;
                var base = '<?php echo base_url(); ?>';
                var nomor_asli = nomor;
                id_ujian_siswa = "'" + id_ujian_siswa + "'";
                id_ujian = "'" + id_ujian + "'";
                list['id_pilihan_soal'] =  "'" + list['id_pilihan_soal'] + "'";
                list['pertanyaan_gambar'] = list['pertanyaan_gambar'].trim(); 
                nomor = "'" + nomor + "'";
                random = "'" + random + "'";
                paragraph = "'" + paragraph + "'";

                var page = "'pg1'";
                if(list['jawaban'] == "pg1"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg1 = list['pg1'] + '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >';
                $("#pg1").html(pg1);



                var page = "'pg2'";
                if(list['jawaban'] == "pg2"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg2 = list['pg2'] + '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >';
                $("#pg2").html(pg2);



                var page = "'pg3'";
                if(list['jawaban'] == "pg3"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg3 = list['pg3'] + '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >';
                $("#pg3").html(pg3);



                var page = "'pg4'";
                if(list['jawaban'] == "pg4"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg4 = list['pg4'] + '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >';
                $("#pg4").html(pg4);



                var page = "'pg5'";
                if(list['jawaban'] == "pg5"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg5 = list['pg5'] + '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >';
                $("#pg5").html(pg5);



                //////////////////////////////////////////////pg gambar
                
                if(list['pg1_gambar'] != ""){
                    var pg1_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg1"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg1"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg1_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg1_gambar").html(pg1_gambar);
                }else{
                    $("#pg1_gambar").html("");
                }

                if(list['pg2_gambar'] != ""){
                    var pg2_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg2"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg2"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg2_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg2_gambar").html(pg2_gambar);
                }else{
                    $("#pg2_gambar").html("");
                }

                if(list['pg3_gambar'] != ""){
                    var pg3_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg3"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg3"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg3_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg3_gambar").html(pg3_gambar);
                }else{
                    $("#pg3_gambar").html("");
                }


                if(list['pg4_gambar'] != ""){
                    var pg4_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg4"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg4"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg4_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg4_gambar").html(pg4_gambar);
                }else{
                    $("#pg4_gambar").html("");
                }

                if(list['pg5_gambar'] != ""){
                    var pg5_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg5"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg5"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg5_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg5_gambar").html(pg5_gambar);
                }else{
                    $("#pg5_gambar").html("");
                }


                //////////////////////PERTANYAAN
                
                var pertanyaannya = 'Pertanyaan No.' + nomor_asli + ' <br><br>' + list['pertanyaan'];
                $("#pertanyaan").html(pertanyaannya);

                

                if(list['pertanyaan_gambar'] == "")
                {
                    var pertanyaan_gambarnya = '';
                }else{
                    var pertanyaan_gambarnya = '<a class="pull-left" data-toggle="modal" href="#modal-pertanyaan"><img src="'+ base +'file/soal/' + list['pertanyaan_gambar'] + '" style="width:80px"></a><div class="modal fade" id="modal-pertanyaan"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="'+ base +'file/soal/' + list['pertanyaan_gambar'] + '" style="width:100%"></div></div></div></div>';
                }
                $("#pertanyaan_gambar").html(pertanyaan_gambarnya);



                ///////////////////PANAH
                var total = "<?php echo count($soal); ?>";
                var nomor_tambah = Number(nomor_asli) + 1;
                var nomor_kurang = Number(nomor_asli) - 1;
                nomor_tambah = "'" + nomor_tambah + "'";
                nomor_kurang = "'" + nomor_kurang + "'";
                total = Number(total);

                if(nomor_asli == total){
                    // alert("kurang");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_kurang +',' + paragraph + ')"><i class="icon-arrow-left"></i></a>  <a data-toggle="modal" href="#modal-akhiri" class="btn btn-info">Selesai</a>';
                }else if(nomor_asli == "1"){
                    // alert("tambah");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_tambah +',' + paragraph + ')"><i class="icon-arrow-right"></i></a>';
                }else{
                    // alert("dua");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_kurang +',' + paragraph + ')"><i class="icon-arrow-left"></i></a><a style ="margin-left:10px;" class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_tambah +',' + paragraph + ')"><i class="icon-arrow-right"></i></a>';
                }
                $("#buttonnya").html(btn);
                
                
            }
            else
            {
                var page;
                var base = '<?php echo base_url(); ?>';
                var nomor_asli = nomor;
                id_ujian_siswa = "'" + id_ujian_siswa + "'";
                id_ujian = "'" + id_ujian + "'";
                list['id_pilihan_soal'] =  "'" + list['id_pilihan_soal'] + "'";
                list['pertanyaan_gambar'] = list['pertanyaan_gambar'].trim(); 
                nomor = "'" + nomor + "'";
                random = "'" + random + "'";
                paragraph = "'" + paragraph + "'";

                var page = "'pg1'";
                if(list['jawaban'] == "pg1"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg1 = '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >' + list['pg1'];
                $("#pg1").html(pg1);




                var page = "'pg2'";
                if(list['jawaban'] == "pg2"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg2 = '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >' + list['pg2'];
                $("#pg2").html(pg2);




                var page = "'pg3'";
                if(list['jawaban'] == "pg3"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg3 = '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >' + list['pg3'];
                $("#pg3").html(pg3);




                var page = "'pg4'";
                if(list['jawaban'] == "pg4"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg4 = '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >' + list['pg4'];
                $("#pg4").html(pg4);




                var page = "'pg5'";
                if(list['jawaban'] == "pg5"){
                    check = "checked";
                }else{
                    check = "";
                }
                var pg5 = '<input type="radio" name="pilihan" value="" onclick="pilihan('+ id_ujian_siswa +',' + list['id_pilihan_soal'] + ',' + page + ',' + nomor +')" '+ check +' >' + list['pg5'];
                $("#pg5").html(pg5);


                //////////////////////////////////////////////pg gambar
                
                if(list['pg1_gambar'] != ""){
                    var pg1_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg1"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg1"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg1_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg1_gambar").html(pg1_gambar);
                }else{
                    $("#pg1_gambar").html("");
                }

                if(list['pg2_gambar'] != ""){
                    var pg2_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg2"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg2"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg2_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg2_gambar").html(pg2_gambar);
                }else{
                    $("#pg2_gambar").html("");
                }

                if(list['pg3_gambar'] != ""){
                    var pg3_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg3"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg3"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg3_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg3_gambar").html(pg3_gambar);
                }else{
                    $("#pg3_gambar").html("");
                }


                if(list['pg4_gambar'] != ""){
                    var pg4_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg4"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg4"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg4_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg4_gambar").html(pg4_gambar);
                }else{
                    $("#pg4_gambar").html("");
                }

                if(list['pg5_gambar'] != ""){
                    var pg5_gambar = '<a class="label label-info" data-toggle="modal" href="#modal-pg5"><i class="icon-picture"></i></a><div class="modal fade" id="modal-pg5"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="' + base +'file/soal/' + list['pg5_gambar'] + '" style="width:100%"></div></div></div></div>';
                    $("#pg5_gambar").html(pg5_gambar);
                }else{
                    $("#pg5_gambar").html("");
                }


                //////////////////////PERTANYAAN
                
                var pertanyaannya = 'Pertanyaan No.' + nomor_asli + ' <br><br>' + nomor_asli +") " +list['pertanyaan'];
                $("#pertanyaan").html(pertanyaannya);

                

                if(list['pertanyaan_gambar'] == "")
                {
                    var pertanyaan_gambarnya = '';
                }else{
                    var pertanyaan_gambarnya = '<a class="pull-right" data-toggle="modal" href="#modal-pertanyaan"><img src="'+ base +'file/soal/' + list['pertanyaan_gambar'] + '" style="width:80px"></a><div class="modal fade" id="modal-pertanyaan"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><img src="'+ base +'file/soal/' + list['pertanyaan_gambar'] + '" style="width:100%"></div></div></div></div>';
                }
                $("#pertanyaan_gambar").html(pertanyaan_gambarnya);


                ///////////////////PANAH
                var total = "<?php echo count($soal); ?>";
                var nomor_tambah = Number(nomor_asli) + 1;
                var nomor_kurang = Number(nomor_asli) - 1;
                nomor_tambah = "'" + nomor_tambah + "'";
                nomor_kurang = "'" + nomor_kurang + "'";
                total = Number(total);

                if(nomor_asli == total){
                    // alert("kurang");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_kurang +',' + paragraph + ')"><i class="icon-arrow-left"></i></a>  <a data-toggle="modal" href="#modal-akhiri" class="btn btn-info">Selesai</a>';
                }else if(nomor_asli == "1"){
                    // alert("tambah");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_tambah +',' + paragraph + ')"><i class="icon-arrow-right"></i></a>';
                }else{
                    // alert("dua");
                    var btn = '<a class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_kurang +',' + paragraph + ')"><i class="icon-arrow-left"></i></a><a style ="margin-left:10px;" class="btn btn-info" onclick="pindah('+ id_ujian_siswa +','+ id_ujian +',' + random +',' + nomor_tambah +',' + paragraph + ')"><i class="icon-arrow-right"></i></a>';
                }
                $("#buttonnya").html(btn);

            }
             


        });
    }

</script>














