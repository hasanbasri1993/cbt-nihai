<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
        	<i class="icon-book"></i>
            <h3>
            	Edit Mata Pelajaran <?php echo $list->nama_mapel; ?>
			</h3>
        </div>
        <div class="widget-content">
        	<?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>


            <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_mapel','edit_action',array('id' => $cf_semester->id,'id2' => $list->id_mapel)); ?>">

		        <table class="table table-responsive">
		            <tr>
		                <td>Nama Mata Pelajaran</td>
		                <td><input class="form-control" name="nama_mapel" value="<?php echo $list->nama_mapel; ?>" type="text" required></td>
		            </tr>
		            <tr>
		                <td>Bobot</td>
		                <td><input class="form-control" name="bobot" value="<?php echo $list->bobot; ?>" type="number" required></td>
		            </tr>
		            <tr>
		                <td>Jenis</td>
		                <td>
		                    <select name="rapot" class="form-control" required>
		                        <option value="pesantren" <?php if($list->rapot == "pesantren")echo "selected"; ?>>Rapot Pesantren</option>
		                        <option value="sekolah" <?php if($list->rapot == "sekolah")echo "selected"; ?>>Rapot Sekolah</option>
		                        <option value="pesantren dan sekolah" <?php if($list->rapot == "pesantren dan sekolah")echo "selected"; ?>>Rapot Pesantren dan Sekolah</option>
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td>Tulisan Bahasa</td>
		                <td>
		                    <select name="tulisan" class="form-control" required>
		                        <option value="latin" <?php if($list->tulisan == "latin")echo "selected"; ?>>Bahasa Latin</option>
		                        <option value="arab" <?php if($list->tulisan == "arab")echo "selected"; ?>>Bahasa Arab</option>
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td>Paragraph</td>
		                <td>
		                    <select name="paragraph" class="form-control" required>
		                        <option value="kiri" <?php if($list->paragraph == "kiri")echo "selected"; ?>>Dari Kiri</option>
		                        <option value="kanan" <?php if($list->paragraph == "kanan")echo "selected"; ?>>Dari Kanan</option>
		                    </select>
		                </td>
		            </tr>
		            <tr>
		                <td>Tipe Penilaian</td>
		                <td>
		                    <input type="checkbox" name="menulis" <?php 
		                    foreach ($tipe as $t) {
		                    	if($t->tipe == "menulis")echo "checked";
		                    }
		                     ?> > Menulis
		                    <input type="checkbox" name="membaca" <?php 
		                    foreach ($tipe as $t) {
		                    	if($t->tipe == "membaca")echo "checked";
		                    }
		                     ?>> Membaca
		                    <input type="checkbox" name="praktek" <?php 
		                    foreach ($tipe as $t) {
		                    	if($t->tipe == "praktek")echo "checked";
		                    }
		                     ?>> Praktek
		                </td>
		            </tr>
		            <tr>
		                <td>
		                    
		                </td>
		                <td style="text-align:left;">
		                    <button type="submit" class="btn btn-primary">Simpan</button>
		                </td>
		            </tr>
		        </table>
		    </form>
        </div>