<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-book"></i>
            <h3>
            	Daftar Mata Pelajaran 
			</h3>
			<a data-toggle="modal" href='#modal-id' class="pull-right btn-primary" style="margin:0px;">
            	<h3 style=""><i class="icon-plus"></i> Klik disini untuk tambah</h3>
			</a>
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
        	 <table class="table table-stripped table-hover table-responsive">
                <thead>
                    <tr class="bg-primary">
                        <td style="text-align:center;">No</td>
                        <td style="text-align:center;">Mata Pelajaran</td>
                        <td style="text-align:center;">Tulisan</td>
                        <td style="text-align:center;">Bobot</td>
                        <td style="text-align:center;">Jenis</td>
                        <td style="text-align:center;"> Action </td>
                    </tr>
                </thead>   
                <tbody>
                <?php $no=0;foreach ($list as $l) {$no++; ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no; ?></td>
                        <td style="text-align:center;"><?php echo $l->nama_mapel; ?></td>
                        <td style="text-align:center;"><?php echo $l->tulisan; ?></td>
                        <td style="text-align:center;"><?php echo $l->bobot; ?></td>
                        <td style="text-align:center;"><?php echo "Rapot ".$l->rapot; ?></td>
                        <td style="text-align:center;">
                            <a class="label-info label label-sm" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_mapel','edit',array('id' => $cf_semester->id, 'id2' => $l->id_mapel)); ?>">
                                <i class="icon-edit"></i> Edit
                            </a>
                            <a class="label-warning label label-sm" style="margin-left:5px;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_mapel','hapus',array('id' => $cf_semester->id, 'id2' => $l->id_mapel)); ?>" onclick="return confirm('hapus mata pelajaran ini ??')">
                                <i class="icon-remove"></i> Hapus
                            </a>
                            
                        </td>
                    </tr>

                <?php } ?>
                </tbody>
             </table>
        </div>
    </div>
</div>


<div id="modal-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Penambahan Mata Pelajaran</h3>
  </div>
  <div class="modal-body">
    <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_mapel','tambah',array('id' => $cf_semester->id)); ?>">

        <table class="table table-responsive">
            <tr>
                <td>Nama Mata Pelajaran</td>
                <td><input class="form-control" name="nama_mapel" type="text" required></td>
            </tr>
            <tr>
                <td>Bobot</td>
                <td><input class="form-control" name="bobot" type="number" required></td>
            </tr>
            <tr>
                <td>Jenis</td>
                <td>
                    <select name="rapot" class="form-control" required>
                        <option value="pesantren">Rapot Pesantren</option>
                        <option value="sekolah">Rapot Sekolah</option>
                        <option value="pesantren dan sekolah">Rapot Pesantren dan Sekolah</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tulisan Bahasa</td>
                <td>
                    <select name="tulisan" class="form-control" required>
                        <option value="latin">Bahasa Latin</option>
                        <option value="arab">Bahasa Arab</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Paragraph</td>
                <td>
                    <select name="paragraph" class="form-control" required>
                        <option value="kiri">Dari Kiri</option>
                        <option value="kanan">Dari Kanan</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tipe Penilaian</td>
                <td>
                    <input type="checkbox" name="menulis"> Menulis
                    <input type="checkbox" name="membaca"> Membaca
                    <input type="checkbox" name="praktek"> Praktek
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
                <td style="text-align:left;">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </td>
            </tr>
        </table>
    </form>
  </div>
</div>