<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Soal Ujian <?php echo ucfirst($kategori); ?>
			</h3>
            <?php if($_SESSION['akses'] == "guru"){ ?>
            <a data-toggle="modal" href='#modal-id' class="pull-right btn-primary" style="margin:0px;">
                <h3 style=""><i class="icon-plus"></i> Klik disini untuk tambah</h3>
            </a>
            <?php } ?>
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
        	 <table class="table table-stripped table-hover table-responsive">
                <thead>
                    <tr class="bg-primary">
                        <td style="text-align:center;">No</td>
                        <td style="text-align:center;">Nama Soal</td>
                        <td style="text-align:center;">Mata Pelajaran</td>
                        <td style="text-align:center;">Nama Guru</td>
                        <td style="text-align:center;">Waktu</td>
                        <td style="text-align:center;">Jumlah Pertanyaan</td>
                        <td style="text-align:center;">Action</td>
                    </tr>
                </thead>   
                <tbody>
                <?php $no=0;foreach ($list as $l) {$no++; ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no; ?></td>
                        <td style="text-align:center;"><?php echo $l['nama_ujian']; ?></td>
                        <td style="text-align:center;"><?php echo $l['nama_mapel']; ?></td>
                        <td style="text-align:center;"><?php echo $l['nama']; ?></td>
                        <td style="text-align:center;"><?php echo $l['waktu']; ?> menit</td>
                        <td style="text-align:center;"><?php echo $l['soal']; ?></td>
                        <td style="text-align:center;">

                            <?php if($_SESSION['akses'] == "guru"){ ?>
                            <a class="label-info label label-sm" data-toggle="modal" href='#modal-edit<?php echo $l['id_ujian']; ?>'>
                                <i class="icon-edit"></i> Edit
                            </a>
                            <?php } ?>

                            <a class="label-warning label label-sm" style="margin-left:5px;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $cf_semester->id, 'id2' => $kategori,'id3' => $l['id_ujian'])); ?>">
                                <i class="icon-th-list"></i> Lihat Pertanyaan
                            </a>

                            <?php if($_SESSION['akses'] == "guru"){ ?>
                            <a class="label-danger label label-sm" style="margin-left:5px;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','hapus',array('id' => $cf_semester->id, 'id2' => $kategori,'id3' => $l['id_ujian'])); ?>" onclick="return confirm('Anda Yakin ingin menghapus soal ini ??')">
                                <i class="icon-remove"></i> Hapus
                            </a>
                            <?php } ?>
                            
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
             </table>
        </div>
    </div>
</div>


<?php foreach ($list as $l) { ?>
    <?php if($_SESSION['akses'] == "guru"){ ?>
                    
     <div id="modal-edit<?php echo $l['id_ujian']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-plus"></i> Edit Soal Ujian <?php echo ucfirst($kategori); ?></h3>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','editAksesGuru',array('id' => $cf_semester->id, 'id2' => $kategori,'id3' => $l['id_ujian'])); ?>">

            <table class="table table-responsive">
                <tr>
                    <td>Pilih Mata Pelajaran </td>
                    <td>
                        <select name="id_mapel" required>
                            <?php foreach ($mapel as $m) { ?>
                            <option value="<?php echo $m->id_mapel; ?>" <?php if($m->id_mapel == $l['id_mapel'])echo 'selected'; ?>>
                                <?php echo $m->nama_mapel; ?>
                            </option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Nama Soal</td>
                    <td><input class="form-control" name="nama_ujian" value="<?php echo $l['nama_ujian']; ?>" type="text" required></td>
                </tr>

                <tr>
                    <td>Tipe Soal</td>
                    <td>
                        <select class="form-control" name="tipe_ujian" required>
                            <option value="pilihan">Pilihan</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Waktu Pengerjaan (menit)</td>
                    <td>
                        <input class="form-control" name="waktu" value="<?php echo $l['waktu']; ?>" type="number" placeholder="per menit" required>
                    </td>
                </tr>

                <tr>
                    <td>
                        
                    </td>
                    <td style="text-align:left;">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </td>
                </tr>
            </table>
        </form>
      </div>
    </div>
    
    
    <?php } ?>
<?php } ?>



<?php if($_SESSION['akses'] == "guru"){ ?>
<div id="modal-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Penambahan Soal Ujian <?php echo ucfirst($kategori); ?></h3>
  </div>
  <div class="modal-body">
    <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','tambahAksesGuru',array('id' => $cf_semester->id, 'id2' => $kategori)); ?>">

        <table class="table table-responsive">
            <tr>
                <td>Pilih Mata Pelajaran </td>
                <td>
                    <select name="id_mapel" required>
                        <?php foreach ($mapel as $m) { ?>
                        <option value="<?php echo $m->id_mapel; ?>">
                            <?php echo $m->nama_mapel; ?>
                        </option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Nama Soal</td>
                <td><input class="form-control" name="nama_ujian" type="text" required></td>
            </tr>

            <tr>
                <td>Tipe Soal</td>
                <td>
                    <select class="form-control" name="tipe_ujian" required>
                        <option value="pilihan">Pilihan</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Waktu Pengerjaan (menit)</td>
                <td>
                    <input class="form-control" name="waktu" type="number" placeholder="per menit" required>
                </td>
            </tr>

            <tr>
                <td>
                    
                </td>
                <td style="text-align:left;">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </td>
            </tr>
        </table>
    </form>
  </div>
</div>

<?php } ?>

<?php if($_SESSION['akses'] == "admin"){ ?>

<?php } ?>