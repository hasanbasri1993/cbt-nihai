<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Pertanyaan Soal <?php echo $ujian->nama_ujian; ?> (<?php echo $ujian->nama_mapel; ?>)
			</h3>
            
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>

            <!-- Untuk data Pilihan GADA -->
            <?php if($ujian->tipe_ujian == "pilihan"){ ?>
                <div>
                    <a class="btn btn-warning" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id, 'id2' => $kategori)); ?>">
                        <i class="icon-arrow-left"></i> Kembali
                    </a> 

                    <?php if($_SESSION['akses'] == "guru"){ ?>
                    <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>
                        <i class="icon-plus"></i> Tambah Pertanyaan
                    </a>    
                    <?php } ?>
                </div>

                <!-- modal penambahan -->
                <div class="modal fade" id="modal-id">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="icon-plus"></i> Penambahan Pertanyaan </h4>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pilihan_pertanyaan_tambah',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $ujian->id_ujian)); ?>" method="POST" enctype="multipart/form-data">
                                
                                    <table class="table table-responsive table-stripped">
                                        <tr>
                                            <td>Pertanyaan</td>
                                            <td>
                                                <input type="text" name="pertanyaan" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <i>
                                                    sisipkan gambar pertanyaan <input name="pertanyaan_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pilihan Pertama</td>
                                            <td>
                                                <input type="text" name="pg1" class="form-control">
                                                <br>
                                                <i>
                                                    sisipkan gambar <input name="pg1_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pilihan Ke Dua</td>
                                            <td>
                                                <input type="text" name="pg2" class="form-control">
                                                <br>
                                                <i>
                                                    sisipkan gambar <input name="pg2_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pilihan Ke Tiga</td>
                                            <td>
                                                <input type="text" name="pg3" class="form-control">
                                                <br>
                                                <i>
                                                    sisipkan gambar <input name="pg3_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pilihan Ke Empat</td>
                                            <td>
                                                <input type="text" name="pg4" class="form-control">
                                                <br>
                                                <i>
                                                    sisipkan gambar <input name="pg4_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pilihan Ke Lima</td>
                                            <td>
                                                <input type="text" name="pg5" class="form-control">
                                                <br>
                                                <i>
                                                    sisipkan gambar <input name="pg5_gambar" type="file">
                                                </i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Jawaban</td>
                                            <td>
                                                <select name="jawaban" class="form-control">
                                                    <option value="pg1">Pilihan Pertama</option>
                                                    <option value="pg2">Pilihan Ke Dua</option>
                                                    <option value="pg3">Pilihan Ke Tiga</option>
                                                    <option value="pg4">Pilihan Ke Empat</option>
                                                    <option value="pg5">Pilihan Ke Lima</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="icon-save"></i> Simpan
                                                </button>
                                            </td>
                                        </tr>
                                    </table>  
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end-modal -->
                <style type="text/css">
                    .edit-kiki:hover{
                        color:black;
                        text-decoration: underline;
                    }

                </style>
                <br>
                <div class="accordion" id="accordion2">
                  <?php $no=0;foreach ($soal as $s) {$no++; ?>
                  <div class="accordion-group">
                    <div class="accordion-heading bg-primary">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#pertanyaan<?php echo $s->id_pilihan_soal; ?>" style="color:white;">
                       Pertanyaan <?php echo $no; ?> | <?php echo $s->pertanyaan; ?>
                    
                        <?php if($_SESSION['akses'] == "guru"){ ?>
                        <span class="pull-right edit-kiki" onclick='document.getElementById("hapusnya<?php echo $s->id_pilihan_soal; ?>").click()'>
                           Hapus
                        </span>
                        <span class="pull-right">
                           | 
                        </span>
                        <span class="pull-right edit-kiki" onclick='document.getElementById("editnya<?php echo $s->id_pilihan_soal; ?>").click()'>
                           Edit
                        </span>
                        <?php } ?>
                       
                      </a>

                      <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','pilihan_pertanyaan_edit',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $id_ujian, 'id4' => $s->id_pilihan_soal)); ?>" class="hidden" id="editnya<?php echo $s->id_pilihan_soal; ?>">
                          Edit 
                      </a>

                      <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('soal','pilihan_pertanyaan_hapus',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $id_ujian, 'id4' => $s->id_pilihan_soal)); ?>" class="hidden" id="hapusnya<?php echo $s->id_pilihan_soal; ?>" onclick="return confirm('Hapus pertanyaan ini ??')">
                          Hapus 2
                      </a>
                    </div>
                    <div id="pertanyaan<?php echo $s->id_pilihan_soal; ?>" class="accordion-body collapse <?php if($no==1)echo 'in'; ?>">
                      <div class="accordion-inner">
                        <table class="table table-stripped table-responsive">
                            <tr>
                                <td style="vertical-align: middle;">Pertanyaan </td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pertanyaan; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pertanyaan_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pertanyaan_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Pilihan 1 </td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pg1; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pg1_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pg1_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Pilihan 2 </td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pg2; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pg2_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pg2_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Pilihan 3</td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pg3; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pg3_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pg3_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Pilihan 4</td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pg4; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pg4_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pg4_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;">Pilihan 5</td>
                                <td style="border-left:1px solid silver;vertical-align: middle;"><?php echo $s->pg5; ?></td>
                                <td style="vertical-align: middle;text-align:center;">
                                    <?php if($s->pg5_gambar != ""){ ?>
                                        <img src="<?php echo base_url().'file/soal/'.$s->pg5_gambar; ?>" style="width:80px;">
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td>Jawaban</td>
                                <td colspan="2" style="border-left:1px solid silver;"><?php echo str_replace("pg", "pilihan ", $s->jawaban); ?></td>
                            </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
            <?php } ?>
            <!-- End Pilihan GADA -->

        </div>
    </div>
</div>