<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Edit Pertanyaan
			</h3>
            
        </div>
        <div class="widget-content">
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>

            <form action="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pilihan_pertanyaan_edit_action',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $id_ujian,'id4' => $pertanyaan->id_pilihan_soal)); ?>" method="POST" enctype="multipart/form-data">
                                
                <table class="table table-responsive table-stripped">
                    <tr>
                        <td>Pertanyaan</td>
                        <td>
                            <input type="text" name="pertanyaan" value="<?php echo $pertanyaan->pertanyaan; ?>" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="vertical-align: middle;">
                            <?php if($pertanyaan->pertanyaan_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pertanyaan_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar pertanyaan <input name="pertanyaan_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilihan Pertama</td>
                        <td>
                            <input type="text" name="pg1" value="<?php echo $pertanyaan->pg1; ?>" class="form-control">
                            <br>
                            <?php if($pertanyaan->pg1_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pg1_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar <input name="pg1_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilihan Ke Dua</td>
                        <td>
                            <input type="text" name="pg2" value="<?php echo $pertanyaan->pg2; ?>" class="form-control">
                            <br>
                            <?php if($pertanyaan->pg2_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pg2_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar <input name="pg2_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilihan Ke Tiga</td>
                        <td>
                            <input type="text" name="pg3" value="<?php echo $pertanyaan->pg3; ?>" class="form-control">
                            <br>
                            <?php if($pertanyaan->pg3_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pg3_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar <input name="pg3_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilihan Ke Empat</td>
                        <td>
                            <input type="text" name="pg4" value="<?php echo $pertanyaan->pg4; ?>" class="form-control">
                            <br>
                            <?php if($pertanyaan->pg4_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pg4_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar <input name="pg4_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilihan Ke Lima</td>
                        <td>
                            <input type="text" name="pg5" value="<?php echo $pertanyaan->pg5; ?>" class="form-control">
                            <br>
                            <?php if($pertanyaan->pg5_gambar != ""){ ?>
                            <img src="<?php echo base_url().'file/soal/'.$pertanyaan->pg5_gambar; ?>" style="max-width: 100px;max-height: 130px;">
                            <?php } ?>
                            <i>
                                ganti gambar <input name="pg5_gambar" type="file">
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>Jawaban</td>
                        <td>
                            <select name="jawaban" class="form-control">
                                <option value="pg1" <?php if($pertanyaan->jawaban == "pg1")echo "selected"; ?>>Pilihan Pertama</option>
                                <option value="pg2" <?php if($pertanyaan->jawaban == "pg2")echo "selected"; ?>>Pilihan Ke Dua</option>
                                <option value="pg3" <?php if($pertanyaan->jawaban == "pg3")echo "selected"; ?>>Pilihan Ke Tiga</option>
                                <option value="pg4" <?php if($pertanyaan->jawaban == "pg4")echo "selected"; ?>>Pilihan Ke Empat</option>
                                <option value="pg5" <?php if($pertanyaan->jawaban == "pg5")echo "selected"; ?>>Pilihan Ke Lima</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <a class="btn btn-warning" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','pertanyaan',array('id' => $cf_semester->id, 'id2' => $kategori, 'id3' => $id_ujian)); ?>">
                                <i class="icon-arrow-left"></i> Kembali
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="icon-save"></i> Simpan
                            </button>
                        </td>
                    </tr>
                </table>  
            </form>

        </div>
    </div>
</div>