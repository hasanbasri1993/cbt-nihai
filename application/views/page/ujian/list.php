<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-list"></i>
            <h3>
            	Penambahan Ujian <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">
            <div style="text-align:right;">

                <?php if ($list_new) {
    ?>
                    <a class="btn-primary btn  btn-sm" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'upload_ujian', array('id' => $cf_semester->id,'id2' => $kategori)); ?>">
                                <i class="icon-upload" aria-hidden="true"></i> Upload Ujian
                    </a>
                <?php
}?>
                &nbsp
                <?php if ($list_new) {
        ?>
                    <a class="btn-primary btn  btn-sm" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_statusujian', array('id' => $cf_semester->id,'id2' => $kategori)); ?>">
                                <i class="icon-eye-open"></i> Monitor Ujian
                    </a>
                <?php
    }?>
                &nbsp
                <!-- Button SYCN modal -->
                <button  type="button" class="btn-primary btn  btn-sm" data-toggle="modal" data-target="#rombelModal">
                <i class="icon-file"></i> Reset Peserta
                </button>
                 &nbsp
                <button id="spin_sycn" type="button" class="btn-primary btn  btn-sm">
                    <i class="icon-spin"></i>Wait ...
                </button>
                <button id="belum_sycn"style="display: none" type="button" class="btn-primary btn  btn-sm" data-toggle="modal" data-target="#syncModal">
                     <i class="icon-file"></i> Sycn Now
                </button>
                <a id="udah_sycn" style="display: none" class="btn-primary btn btn-sm" disabled href="#">
                    <i class="icon-file"></i> Sycn Selesai
                </a>
                <!-- Button  modal -->
            </div>

            <?php if ($this->session->flashdata('gagal') != "") {
        ?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php
    } ?>
            <?php if ($this->session->flashdata('berhasil') != "") {
        ?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php
    } ?>
            <style type="text/css">
                table.table td{
                    text-align:center;
                }
            </style>
            <p>Daftar Kelas yang akan mengikuti ujian !</p>
            <table class="table table-responsive table-striped text-center">
                <thead>
                    <tr class="bg-primary">
                        <td>No</td>
                        <td>Kelas</td>
                        <td>Soal Ujian</td>
                        <td>Mata Pelajaran</td>
                        <td>Dari</td>
                        <td>Sampai</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=0; foreach ($list_new as $l) {
        $no++; ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $l->sekolah.' ('.$l->kelas.'-'.$l->rombel.')'; ?></td>
                        <td><?php echo $l->nama_ujian; ?></td>
                        <!-- <td><?php echo $l->tipe_ujian; ?></td> -->
                        <td><?php echo $l->nama_mapel; ?></td>
                        <!-- <td><?php echo $l->nama; ?></td> -->
                        <td><?php echo date('d-M-Y H:i:s', strtotime($l->waktu_dari)); ?></td>
                        <td><?php echo date('d-M-Y H:i:s', strtotime($l->waktu_sampai)); ?></td>
                    </tr>
                    <?php
    } ?>
                </tbody>
            </table>


        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="rombelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rombel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('rekap', 'list_siswa_allpelajaran'); ?>">
            <table class="table">
                <input name="id_semester" type="hidden" value="<?=$cf_semester->id;?>"/>
                <tr>
                    <td>Pilih Rombel</td>
                    <td>
                        <select  onchange="this.form.submit()"  id="pilihrombel" name="id_rombel">
                            <option>--</option>
                        <?php foreach ($rombel_list as $row) {
        ?>
                            <option value="<?=$row['id_rombel']; ?>"><?=$row['kelas']; ?></option>";
                        <?php
    }?>
                        </select>
                    </td>
                </tr>
            </table>
         </form>
      </div>
      <div class="modal-footer">
        
        
    </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="syncModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Setting Sycn</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'sycn', array('id' => $cf_semester->id,'id2' => $kategori)); ?>">
            <div class="checkbox">
                <label>
                <input type="checkbox" name="datasantri"> Data Santri
                </label>
            </div>
            <div class="checkbox">
                <label>
                <input type="checkbox" name="paketsoal"> Data Soal
                </label>
            </div>
            
       
      </div>
      <div class="modal-footer">
            
      <button type="submit" class="btn-primary btn  btn-sm">
                <i class="icon-file"></i> Sycn Sekarang
            </button>
            </form>
      </div>
    </div>
  </div>
</div>


<script>
    $(document).ready(function() {

        //var status = "1";
        $.post("<?php echo base_url().'index.php/ujian/cek_statussycn'?>", function(data) {
            if(data == "1"){
                $("#spin_sycn").hide();
                $("#udah_sycn").show();
            }else{
                $("#spin_sycn").hide();
                $("#belum_sycn").show();
            }
        });

    });
</script>