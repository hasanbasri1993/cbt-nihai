<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Penambahan Ujian <?php echo ucfirst($kategori); ?>
			</h3>
        </div>
        <div class="widget-content">
            <div style="text-align:right;">
                <a class="btn-primary btn  btn-sm" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','tambah_guru',array('id' => $cf_semester->id,'id2' => $kategori)); ?>">
                    <i class="icon-plus-sign"></i> Buat Ujian
                </a>
            </div>

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
            <style type="text/css">
                table.table td{
                    text-align:center;
                }
            </style>
            <p>Daftar Kelas yang akan mengikuti ujian !</p>
            <table class="table table-responsive table-striped text-center">
                <thead>
                    <tr class="bg-primary">
                        <td>No</td>
                        <td>Kelas</td>
                        <td>Soal Ujian</td>
                        <!-- <td>Tipe Ujian</td> -->
                        <td>Mata Pelajaran</td>
                        <td>Dari</td>
                        <td>Sampai</td>
                        <td>Random Pertanyaan</td>
                        <!-- <td>Guru Pembimbing</td> -->
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=0;foreach ($list_new as $l) {$no++; ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $l->sekolah.' ('.$l->kelas.'-'.$l->rombel.')'; ?></td>
                        <td><?php echo $l->nama_ujian; ?></td>
                        <!-- <td><?php echo $l->tipe_ujian; ?></td> -->
                        <td><?php echo $l->nama_mapel; ?></td>
                        <!-- <td><?php echo $l->nama; ?></td> -->
                        <td><?php echo date('d-M-Y H:i:s',strtotime($l->waktu_dari)); ?></td>
                        <td><?php echo date('d-M-Y H:i:s',strtotime($l->waktu_sampai)); ?></td>
                        <td><?php 
                            if($l->status_random == "Y"){
                                echo "<span class='label label-success'> yes </span>";
                            }
                            else
                            {
                                echo "<span class='label label-danger'> no </span>";
                            } ?>
                            
                        </td>
                        <td>
                            <a class="label label-danger" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','batalkan',array('id' => $cf_semester->id,'id2' => $kategori,'id3' => $l->id_ujian_launch)); ?>" onclick="return confirm('Apakah anda yakin ingin membatalkan ini ??')">
                                <i class="icon-remove"></i> Batalkan
                            </a>    
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>


        </div>
    </div>
</div>