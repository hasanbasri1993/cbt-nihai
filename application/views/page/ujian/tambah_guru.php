<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-th-list"></i>
            <h3>
            	Daftar Ujian <?php echo ucfirst($kategori); ?>
			</h3>
            
        </div>
        <div class="widget-content">

            <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>

            <form action="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','tambah_action_guru',array('id_semester' => $cf_semester->id, 'id2' => $kategori)); ?>" method="POST" class="form-horizontal">
                <div>
                    <a class="btn-warning btn  btn-sm" href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_guru',array('id' => $cf_semester->id,'id2' => $kategori)); ?>">
                        <i class="icon-arrow-left"></i> Kembali
                    </a>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="icon-save"></i> Simpan</button>
                </div>
                <br>
                <table class="table">
                    <tr>
                        <td>Pilih Mata Pelajaran</td>
                        <td>
                            <select id="mapel" class="form-control" required>
                            <?php foreach ($mapel as $m) { ?>
                                <option value="<?php echo $m->id_mapel; ?>" >
                                    <?php echo $m->nama_mapel; ?>
                                </option>
                            <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Pilih Soal</td>
                        <td id="soal">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>Pilih Kelas</td>
                        <td id="kelas">
                            <select id="kelass" class="form-control" name="id_rombel" required>
                            <?php foreach ($rombel as $k) { ?>
                                <option value="<?php echo $k->id_rombel; ?>" >
                                    <?php echo $k->sekolah." (".$k->kelas."-".$k->rombel.")"; ?>
                                </option>
                            <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Waktu Ujian Dari</td>
                        <td>
                            <input type="text" name="waktu_dari" id="datetimepicker" class="form-control" required> 
                        </td>
                    </tr>
                    <tr>
                        <td>Waktu Ujian Sampai</td>
                        <td>
                            <input type="text" name="waktu_sampai" id="datetimepicker2" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Random Pertanyaan</td>
                        <td>
                            <select name="status_random" class="form-control" required>
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
                        </td>
                    </tr>

                </table>
                
            </form>
            
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
   
//	$().ajaxmapel();

//	(function( $ ){
//           $.fn.ajaxmapel = function() {
//               var id = $("#mapel").val();
//        $.post( "<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','ajax_findsoal_guru'); ?>", { id_semester: "<?php echo $cf_semester->id; ?>", id_mapel: id, kategori_ujian: "<?php echo $kategori; ?>" }).done(function( data ) {
//            // alert("sdsd");
//            var list = JSON.parse(data);
//            var isi = "<select name='id_ujian' id='guruf' class='form-control' required>";
//            for(x=0;x<list.length;x++)
//            {
//                isi = isi + "<option value='" + list[x]['id_ujian'] + "' >" + list[x]['nama_ujian'] + "</option>";
//            }
//            isis = isi + "</select>";
//
//            $("#soal").html(isi);
//        });


//              return this;
//           }; 
//        })(jQuery);

    $("#mapel").change(function(){
        var id = $("#mapel").val();
        $.post( "<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','ajax_findsoal_guru'); ?>", { id_semester: "<?php echo $cf_semester->id; ?>", id_mapel: id, kategori_ujian: "<?php echo $kategori; ?>" }).done(function( data ) {
            // alert("sdsd");
            var list = JSON.parse(data);
            var isi = "<select name='id_ujian' id='guruf' class='form-control' required>";
            for(x=0;x<list.length;x++)
            {
                isi = isi + "<option value='" + list[x]['id_ujian'] + "' >" + list[x]['nama_ujian'] + "</option>";
            }
            isis = isi + "</select>";
            
            $("#soal").html(isi);
        });
    });

    $("#mapel").click(function(){
        var id = $("#mapel").val();
        $.post( "<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','ajax_findsoal_guru'); ?>", { id_semester: "<?php echo $cf_semester->id; ?>", id_mapel: id, kategori_ujian: "<?php echo $kategori; ?>" }).done(function( data ) {
            // alert("sdsd");
            var list = JSON.parse(data);
            var isi = "<select name='id_ujian' id='guruf' class='form-control' required>";
            for(x=0;x<list.length;x++)
            {
                isi = isi + "<option value='" + list[x]['id_ujian'] + "' >" + list[x]['nama_ujian'] + "</option>";
            }
            isis = isi + "</select>";
            
            $("#soal").html(isi);
        });
    });

});
</script>







    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/template/calendartime/jquery.datetimepicker.css"/>
    <script src="<?php echo base_url(); ?>asset/template/calendartime/jquery.js"></script>
    <script src="<?php echo base_url(); ?>asset/template/calendartime/build/jquery.datetimepicker.full.js"></script> 
    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        // disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        startDate:  '<?php echo date('Y-m-d'); ?>'
        });
        $('#datetimepicker').datetimepicker({value:'<?php echo date('Y-m-d h:i:s'); ?>',step:10});


        $('#datetimepicker2').datetimepicker({
        dayOfWeekStart : 2,
        lang:'en',
        // disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        startDate:  '<?php echo date('Y-m-d'); ?>'
        });
        $('#datetimepicker2').datetimepicker({value:'<?php echo date('Y-m-d h:i:s'); ?>',step:10});

    </script>
