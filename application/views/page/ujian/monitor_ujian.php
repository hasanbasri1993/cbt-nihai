<link rel="stylesheet" href="<?php echo base_url() ?>asset/sort/css/jq.css">
<link href="<?php echo base_url() ?>asset/sort/css/prettify.css" rel="stylesheet">
<script src="<?php echo base_url() ?>asset/sort/js/prettify.js"></script>


<!-- Tablesorter: required -->
<link rel="stylesheet" href="<?php echo base_url() ?>asset/sort/sort/css/theme.green.css">
<script src="<?php echo base_url() ?>asset/sort/sort/js/jquery.tablesorter.js"></script>
<script src="<?php echo base_url() ?>asset/sort/sort/js/jquery.tablesorter.widgets.js"></script>

<script src="<?php echo base_url() ?>asset/sort/sort/js/widgets/widget-columnSelector.js"></script>
<script src="<?php echo base_url() ?>asset/sort/sort/js/widgets/widget-print.js"></script>
<style>
.options th.narrow {
	width: 150px;
}
.button {
	position: relative;
	padding: 1px 6px;
	display: inline-block;
	cursor: pointer;
	border: #000 1px solid;
	border-radius: 5px;
}
.columnSelector, .hidden {
	display: none;
}
#colSelect1:checked + label {
	color: #307ac5;
}
#colSelect1:checked ~ #columnSelector {
	display: block;
}
.columnSelector {
	width: 120px;
	position: absolute;
	top: 30px;
	padding: 10px;
	background: #fff;
	border: #99bfe6 1px solid;
	border-radius: 5px;
}
.columnSelector label {
	display: block;
	text-align: left;
}
.columnSelector label:nth-child(1) {
	border-bottom: #99bfe6 solid 1px;
	margin-bottom: 5px;
}
.columnSelector input {
	margin-right: 5px;
}
.columnSelector .disabled {
	color: #ddd;
}

</style>
<script id="js">
$(function() {


	

	$.tablesorter.language.button_print = "Print";
	$.tablesorter.language.button_close = "Close";

	$(".tablesorter").tablesorter({
		theme: 'green',
		widgets: ["zebra", "filter", "print", "columnSelector"],
		widgetOptions : {
			filter_cssFilter   : '',
			filter_ignoreCase  : true,
			filter_reset : '.reset',
			filter_saveFilters : true,
			filter_searchDelay : 300,
			filter_functions : {
				4 : {
					"< 50"      : function(e, n, f, i, $r, c, data) { return e < 50; },
					"> 50"     : function(e, n, f, i, $r, c, data) { return e > 50; }
					},
				5 : {
					"selesai"      : function(e, n, f, i, $r, c, data) {  return /Selesai/.test(e); },
					"tertunda"      : function(e, n, f, i, $r, c, data) {  return /Tertunda/.test(e); },
					"belum"     : function(e, n, f, i, $r, c, data) { return /Belum/.test(e); }
					}
			},
			columnSelector_container : $('#columnSelector'),
			columnSelector_name : 'data-name',
			print_title      : '',          // this option > caption > table id > "table"
			print_dataAttrib : 'data-name', // header attrib containing modified header name
			print_rows       : 'f',         // (a)ll, (v)isible, (f)iltered, or custom css selector
			print_columns    : 's',         // (a)ll, (v)isible or (s)elected (columnSelector widget)
			print_extraCSS   : '',          // add any extra css definitions for the popup window here
			print_styleSheet : '<?php echo base_url() ?>asset/sort/sort/css/theme.blue.css', // add the url of your print stylesheet
			print_now        : false,        // Open the print dialog immediately if true
			// callback executed when processing completes - default setting is null
			print_callback   : function(config, $table, printStyle){
				// do something to the $table (jQuery object of table wrapped in a div)
				// or add to the printStyle string, then...
				// print the table using the following code
				$.tablesorter.printTable.printOutput( config, $table.html(), printStyle );
			}
		}
	});

	$.post("<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_statusujian_ajax', array('id' => $id_semester,'id2' => $kategori)); ?>", function(data) {
			$(".tablesorter tbody").html(data);
			var resort = true;
			$(".tablesorter").trigger("update", [resort]);
	});


	var myVar = setInterval(myTimer, 1000);
	function myTimer() {
		$.post("<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian', 'list_statusujian_ajax', array('id' => $id_semester,'id2' => $kategori)); ?>", function(data) {
			$(".tablesorter tbody").html(data);
			$(".tablesorter").trigger("update");
		});
	}

	$('.print').click(function(){
		$('.tablesorter').trigger('printTable');
	});
});
</script>



<div class="row all-icons" style="margin:0px 0px;">
	<div class="widget">
		<div class="widget-header">
			<i class="icon-th-list"></i>
			<h3>
				Daftar Nilai Ujian
			</h3>
		</div>
		<div class="widget-content">
			<div class="btn btn-sm btn-info btn-sm" >
				<a style="color:white;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('ujian', 'list_admin', array('id' => $id_semester,'id2' => $kategori)); ?>">
                <i class="icon-hand-left"></i> Kembali  
              </a>
			</div>
			<div class="print btn btn-sm btn-success"><i class="icon-print"></i> print</div>
			<div class="columnSelectorWrapper button">
				<input id="colSelect1" type="checkbox" class="hidden">
				<label class="columnSelectorButton btn btn-sm" for="colSelect1">
					<i class="icon-search"></i> filter kolom</label>
				<div id="columnSelector" class="columnSelector">
				</div>
			</div>
			<br><br>
			<table class="tablesorter">
				<thead>
					<tr>
						<th> No </th>
						<th> Nama Siswa </th>
						<th> Kelas </th>
						<th class="pelajaran filter-select" data-placeholder="Pilih Pelajaran"> Pelajaran </th>
						<th class="nilai filter-select" data-placeholder="Pilih Nilai"> Nilai </th>
						<th class="status filter-select" data-placeholder="Pilih Status"> Status </th>
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>
</div>