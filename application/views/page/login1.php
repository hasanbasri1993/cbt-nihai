<?php 
    $banner = "bannerMadipo.png";
    $logo   = "literasi.png";
    $warna  = "#0AF713";
    $skull =  "MA Daarul Uluum Lido";
?>
<!DOCTYPE html>
<html  lang="en">
<head>
    <title><?php echo $skull;?> | UJIAN ONLINE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <link href="<?php echo base_url(); ?>asset/template/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>asset/template/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>asset/template/css/components.min.css" rel="stylesheet" type="text/css" id="style_components"/>
    <link href="<?php echo base_url(); ?>asset/template/css/pages/login-4.min.css" rel="stylesheet" type="text/css" /> 
    <link href='images/icon.png' rel='icon' type='image/png'/>
    <style>
    </style>
</head>
		
 <body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
      
            <br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('login','action'); ?>" method="post" data-toggle="validator" id="form1">
            <h3 class="form-title">Login</h3>
            <?php if($this->session->flashdata('gagal') != ""){?>
                <div class="account-container " style="background-color:#ba3434;margin-top:10px;">
                        <div class="alert alert-danger" style="margin-left:5px;margin-bottom:0px;margin-right:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                        </div>
                </div>
            <?php } ?>

            <div class="form-group">
                <label class="control-label" for="UserName">Username</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input data-toggle="UserName" class="form-control placeholder-no-fix" data-val="true" data-val-required="User name wajib diisi"  type="text" autocomplete="off" id="inputUsername" name="username"  value=""  required />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="Password">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control" data-val="true" data-val-required="Password wajib diisi" type="password" autocomplete="off" id="inputPassword" name="password"  value=""  required />
                </div>
            </div>	
            <div class="form-actions">
            <button type="submit" id="submit" class="btn green pull-right" > Login </button>
            </div>
        </form>
        </div>
    </div>		
</body>	
</html>	

<script src="<?php echo base_url(); ?>asset/template/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>asset/template/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>asset/template/js/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/template/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("form1").validate();

    $.backstretch([
        "<?php echo base_url(); ?>asset/media/bg/8.jpg",
        "<?php echo base_url(); ?>asset/media/bg/9.jpg"
        ], {
            fade: 750,
            duration: 4000
        });
    });
</script>