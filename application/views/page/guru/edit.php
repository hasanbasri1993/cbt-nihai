<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
        	<i class="icon-group"></i>
            <h3>
            	Edit Data Guru
			</h3>
        </div>
        <div class="widget-content">
        	<?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>


            <form method="POST" action="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_guru','edit_action',array('id' => $cf_semester->id,'id2' => $guru->id)); ?>">

		        <table class="table table-responsive">
		            <tr>
		                <td>Pilih Pegawai</td>
		                <td>
		                	<?php echo $guru->nama; ?>
		                </td>
		            </tr>
		            
		            <tr>
		                <td>Pilih Mata Pelajaran</td>
		                <td>
		                <?php foreach ($mapel as $m) { ?>
		                    <input type="checkbox" class="form-control" name="mapel<?php echo $m->id_mapel; ?>" value="<?php echo $m->id_mapel; ?>"
		                    	<?php 
		                    		foreach ($el_mapel as $el) {
		                    			if($el->id_mapel == $m->id_mapel)echo "checked";
		                    		}
		                    	 ?>
		                    	
		                    	> 

		                    	<?php echo $m->nama_mapel; ?><br>
		                <?php } ?>
		                </td>
		            </tr>
		            <tr>
		            	<td colspan="2">
		            		<p style="border-bottom:1px solid black;"><b>Pilih Akses Kelas Guru</b></p>
		            	</td>
		            </tr>
		            <tr>
		                <td></td>
		                <td>
			                <?php for($x=0;$x<count($sekolah);$x++) { ?>
			                 	<p style="border-bottom:1px dotted black;"><b ><?php echo $sekolah[$x]['sekolah']; ?></b></p>
			                 	<br>  
			                 	<ul>
			                 		<?php for($i=0;$i<count($sekolah[$x]['kelasnya']);$i++) { ?>
			                 			<li>
			                 				<b>
			                 				<i>
			                 				<?php echo "Kelas ".$sekolah[$x]['kelasnya'][$i]['kelas']; ?>
			                 				</i>
			                 				</b>
			                 				<!-- <ul> -->
			                 				<br>
			                 					<?php foreach ($sekolah[$x]['kelasnya'][$i]['rombelnya'] as $r) { ?>
			                 					<!-- <li> -->
			                 					<span style="margin:0px 0px 0px 25px;">
			                 						<input type="checkbox" name="<?php echo $r['id']; ?>" value="<?php echo $r['id']; ?>" class="form-control"

			                 						<?php 
							                    		foreach ($el_rombel as $el) {
							                    			if($el->id_rombel == $r['id'])echo "checked";
							                    		}
							                    	 ?>

			                 						> 

			                 						<?php echo $r['rombel']; ?>
			                 					</span>
			                 					<!-- </li> -->

			                 					<?php } ?>
			                 				<!-- </ul> -->
			                 			</li>
			                 		<?php } ?>
			                 	</ul>
			                <?php } ?>
		                </td>
		            </tr>

		            <tr>
		                <td>
		                    
		                </td>
		                <td style="text-align:left;">
		                    <button type="submit" class="btn btn-primary">Simpan</button>
		                </td>
		            </tr>
		        </table>
		    </form>
        </div>