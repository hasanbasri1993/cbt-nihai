<div class="row all-icons" style="margin:0px 0px;">
    <div class="widget">
        <div class="widget-header">
            <i class="icon-group"></i>
            <h3>
            	Daftar Guru 
			</h3>
			<a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_guru','tambah',array('id' => $cf_semester->id)); ?>" class="pull-right btn-primary" style="margin:0px;">
            	<h3><i class="icon-plus"></i> Klik disini untuk tambah</h3>
			</a>
        </div>
        <div class="widget-content">
        	 <?php if($this->session->flashdata('gagal') != ""){?>
                <div style="background-color:red;border-radius:5px;">
                    <div class="alert alert-danger" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('gagal'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('berhasil') != ""){?>
                <div style="background-color:green;border-radius:5px;">
                    <div class="alert alert-success" style="margin-left:5px;">
                        <?php echo $this->session->flashdata('berhasil'); ?>
                    </div>
                </div>
            <?php } ?>
             <table class="table table-stripped table-hover table-responsive">
                <thead>
                    <tr class="bg-primary">
                        <td style="text-align:center;">No</td>
                        <td style="text-align:center;">Nama Guru</td>
                        <td style="text-align:center;">Username</td>
                        <td style="text-align:center;">Jumlah Mata Pelajaran</td>
                        <td style="text-align:center;"> Action </td>
                    </tr>
                </thead>   
                <tbody>
                <?php $no=0;foreach ($list as $l) {$no++; ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no; ?></td>
                        <td style="text-align:center;"><?php echo $l->nama; ?></td>
                        <td style="text-align:center;">guru_<?php echo $l->username; ?></td>
                        <td style="text-align:center;"><?php echo $l->jumlah; ?></td>
                        <td style="text-align:center;">
                            <a class="label-info label label-sm" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_guru','edit',array('id' => $cf_semester->id, 'id2' => $l->id)); ?>">
                                <i class="icon-edit"></i> Edit
                            </a>
                            <a class="label-warning label label-sm" style="margin-left:5px;" href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_guru','hapus',array('id' => $cf_semester->id, 'id2' => $l->id)); ?>" onclick="return confirm('hapus data guru ini ??')">
                                <i class="icon-remove"></i> Hapus
                            </a>
                            
                        </td>
                    </tr>

                <?php } ?>
                </tbody>
             </table>
        </div>
    </div>
</div>