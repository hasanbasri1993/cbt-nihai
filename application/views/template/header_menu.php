
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">E-Learning Tahun <?php echo $cf_tahun_semester->tahun; ?> (<?php echo $cf_semester->semester; ?>)</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li>
            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="icon icon-signout"></i> Logout</a>
          </li>
        </ul>
        <!-- <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form> -->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->


<!-- /subnavbar
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        
        <li class="<?php if($this->uri->segment(1) == 'data_admin')echo 'active'; ?>">
          <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_admin','index',array('id' => $cf_semester->id));?>">
            <i class="icon-user"></i><span>Data Admin</span> 
          </a> 
        </li>
        
      
        <li class="<?php if($this->uri->segment(1) == 'data_mapel')echo 'active'; ?>">
          <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_mapel','index',array('id' => $cf_semester->id));?>">
            <i class="icon-book"></i><span>Mata Pelajaran</span> 
          </a> 
        </li>


        <li class="<?php if($this->uri->segment(1) == 'data_guru')echo 'active'; ?>">
          <a href="<?php echo base_url(); ?>index.php/<?php echo $this->mycrypt->enkripsi('data_guru','index',array('id' => $cf_semester->id));?>">
            <i class="icon-user"></i><span>Data Guru</span> 
          </a> 
        </li>


        

        <li class="dropdown" ><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>Daftar Soal</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'soal')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "nihai")); ?>">Soal Ujian Nihai</a>
            </li>
	    <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'soal')
            {
              if($kategori == 'harian')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "harian")); ?>">Soal Ujian Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'soal')
            {
              if($kategori == 'uts')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "uts")); ?>">Soal Ujian UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'soal')
            {
              if($kategori == 'uas')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "uas")); ?>">Soal Ujian UAS</a>
            </li>
          </ul>
        </li>
        
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>Daftar Ujian</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'ujian')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_admin',array('id' => $cf_semester->id,'id2' => 'nihai')); ?>">Ujian Nihai</a>
            </li>
	    <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'ujian')
            {
              if($kategori == 'harian')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_admin',array('id' => $cf_semester->id,'id2' => 'harian')); ?>">Ujian Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'ujian')
            {
              if($kategori == 'uts')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_admin',array('id' => $cf_semester->id,'id2' => 'uts')); ?>">Ujian UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'ujian')
            {
              if($kategori == 'uas')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_admin',array('id' => $cf_semester->id,'id2' => 'uas')); ?>">Ujian UAS</a>
            </li>

          </ul>
        </li>
	
	<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-list-alt"></i><span>Rekap Hasil</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
	    <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'nihai')); ?>">rekap Nihai</a>
            </li>        
 		<li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'nihai')echo '';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index1',array('id' => $cf_semester->id,'id2' => 'nihai')); ?>">Rekap Nihai Per Semester</a>
            </li> 
						
	    <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'harian')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'harian')); ?>">rekap Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'uts')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'uts')); ?>">rekap UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'uas')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'uas')); ?>">rekap UAS</a>
            </li>

          </ul>
        </li>

        
      </ul>
    </div>
  </div>
</div>
<!-- /subnavbar -->

<br>
<div class="main">
  <div class="main-inner">
    <div class="container" style="min-height:400px;">
