
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">E-Learning Tahun <?php echo $cf_tahun_semester->tahun; ?> (<?php echo $cf_semester->semester; ?>)</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li>
            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="icon icon-signout"></i> Logout</a>
          </li>
          <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> EGrappler.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li> -->
        </ul>
        <!-- <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form> -->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->



<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        

        <li class="<?php if($this->uri->segment(1) == 'dashboard')echo 'active'; ?>">
          <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('dashboard','guru',array('id' => $cf_semester->id)); ?>">
            <i class="icon-dashboard"></i><span>Dashboard</span> 
          </a> 
        </li>

        

        <li class="dropdown" ><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>Daftar Soal</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'soal')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "nihai")); ?>">Soal Ujian Nihai</a>
            </li>
	    <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'harian')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "harian")); ?>">Soal Ujian Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'uts')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "uts")); ?>">Soal Ujian UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'uas')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('soal','index',array('id' => $cf_semester->id,'id2' => "uas")); ?>">Soal Ujian UAS</a>
            </li>
          </ul>
        </li>
        
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>Daftar Ujian</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
           
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'ujian')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_guru',array('id' => $cf_semester->id,'id2' => 'nihai')); ?>">Ujian Nihai</a>
            </li>

	   <li>
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ujian','list_guru',array('id' => $cf_semester->id,'id2' => 'harian')); ?>">Ujian Harian</a>
            </li>
          </ul>
        </li>

        <!-- <li>
          <a href="#">
            <i class="icon-list-alt"></i><span> Rekap Hasil</span> 
          </a> 
        </li> -->

	<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-list-alt"></i><span>Rekap Hasil</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'nihai')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'nihai')); ?>">rekap Nihai</a>
            </li>
	    <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'harian')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'harian')); ?>">rekap Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'uts')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'uts')); ?>">rekap UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori) and $this->uri->segment(1) == 'rekap')
            {
              if($kategori == 'uas')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('rekap','index',array('id' => $cf_semester->id,'id2' => 'uas')); ?>">rekap UAS</a>
            </li>

          </ul>
        </li>

        
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container" style="min-height:400px;">
