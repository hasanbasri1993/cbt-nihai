<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <title>E-Learning (Pesantren Daarululum)</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="" />
  <meta name="description" content="Aplikasi E-Learning Berbasis web dinamis, bertujuan untuk mempermudah aktivitas antara guru dan siswa.">
  <meta name="author" content="muhammadrejeki29@gmail.com">
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <meta name="apple-mobile-web-app-capable" content="yes"> 


  <link href="<?php echo base_url(); ?>asset/template/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
          rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/font-awesome.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>asset/template/css/pages/dashboard.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>asset/template/js/jquery-1.7.2.min.js"></script> 
  <style type="text/css">
    .bg-primary{
      background-color: #00ba8b;
      color:white;
    }
    .text-center{
      text-align:center;
    }
  </style>

</head>
<body>
