
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">E-Learning Tahun <?php echo $cf_tahun_semester->tahun; ?> (<?php echo $cf_semester->semester; ?>)</a>
      <div class="nav-collapse">
        
        <ul class="nav pull-right">
          <li>
            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="icon icon-signout"></i> Logout</a>
          </li>
        </ul>

      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->



<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        

        <li class="<?php if($this->uri->segment(1) == 'dashboard')echo 'active'; ?>">
          <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('dashboard','siswa',array('id' => $cf_semester->id)); ?>">
            <i class="icon-dashboard"></i><span>Dashboard</span> 
          </a> 
        </li>
        

        <li class="dropdown" ><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file"></i><span>UJIAN</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'nihai' and $this->uri->segment(1) == 'ulangan')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','index',array('id' => $cf_semester->id,'id2' => "nihai")); ?>">Ujian Nihai</a>
            </li>

	    <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'harian' and $this->uri->segment(1) == 'ulangan')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','index',array('id' => $cf_semester->id,'id2' => "harian")); ?>">Ujian Harian</a>
            </li>
            <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'uts' and $this->uri->segment(1) == 'ulangan')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','index',array('id' => $cf_semester->id,'id2' => "uts")); ?>">Ujian UTS</a>
            </li>
            <li class="<?php 
            if(isset($kategori))
            {
              if($kategori == 'uas' and $this->uri->segment(1) == 'ulangan')echo 'active';
            }
            ?>">
              <a href="<?php echo base_url().'index.php/'.$this->mycrypt->enkripsi('ulangan','index',array('id' => $cf_semester->id,'id2' => "uas")); ?>">Ujian UAS</a>
            </li>
          </ul>
        </li>
        

        <!-- <li>
          <a href="#">
            <i class="icon-list-alt"></i><span> Rekap Hasil</span> 
          </a> 
        </li> -->

        
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container" style="min-height:400px;">
