<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('createCode'))
{
	function createCode($b) {
	    $awalnya = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","!");
	    $gantinya = array("!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}",";",":","?","1","2","3","4","5","`","~",">","a","b","c","d","e","f","g","/");
	    $hasilnya = str_replace($awalnya, $gantinya, $b);
	    return $hasilnya;
    }

}

// ------------------------------------------------------------------------

if ( ! function_exists('translateCode'))
{

    function translateCode($t) {
	    $kodenya = array("!","@","#","$","%","^","&","*","(",")","-","_","+","=","[","]","{","}",";",":","?","1","2","3","4","5","`","~",">","a","b","c","d","e","f","g","/");
	    $terjemahanya = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","!");
	    $hasilterjemahan = str_replace($kodenya, $terjemahanya, $t);
	    return $hasilterjemahan;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('randCode'))
{
	function randCode($panjang)
	{
		$karakter = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","!","@","#","$","%","&","(",")");
    	$hasil = '';
    	for($i=0;$i<$panjang;$i++){
			$no = rand(0,count($karakter)-1);
			$kata = $karakter[$no];
			$hasil = $hasil.$kata;
		}
		return $hasil;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('randCode'))
{
	function arrayKeObject($array)
	{
		$object = new stdClass();
		if (is_array($array))
		{
			foreach ($array as $kolom=>$isi)
			{
				$kolom = strtolower(trim($kolom));
				$object->$kolom = $isi;
			}
		}
		return $object;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('randCode'))
{
	function objectKeArray($object)
	  {
	    $array = array();
	    if (is_object($object)) {
	      	$array = get_object_vars($object);
	    }
	    	return $array;
	 	}
	}


// ------------------------------------------------------------------------
// End of Mine (Muhammad Rejeki)

?> 
